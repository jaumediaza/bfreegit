package com.android.internal.telephony;

/**
 * Created by Alberto on 17/05/2018.
 */

public interface ITelephony {
    boolean endCall();
    void answerRingingCall();
    void silenceRinger();
}
