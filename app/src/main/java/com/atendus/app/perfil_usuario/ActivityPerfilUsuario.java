package com.atendus.app.perfil_usuario;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.ResultadoPerfilUsuario;
import com.atendus.app.clases.Usuario;
import com.atendus.app.json.JSONEditarUsuario;
import com.atendus.app.json.JSONPerfilUsuario;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;


/**
 * Created by Alberto on 13/06/2018.
 */

public class ActivityPerfilUsuario extends AppCompatActivity {
    private int resultadoCambio=0;
    private Activity activity=null;
    private Aplicacion app=new Aplicacion();
    private ProgressBar progressBar;
    private LinearLayout lyDisabled;

    private EditText edtxt_nombre,edtxt_email,edtxt_pass,edtxt_pass2,edtxt_codigo_pin;
    private Thread tDatosUsuario;
    private Handler handlerDatosUsuario;

    private ResultadoPerfilUsuario resultadoPerfilUsuario=new ResultadoPerfilUsuario();
    private Thread tRegistro;

    private int resultadoEditarUsuario=0;
    private Handler handlerRegistro;

    Typeface Roboto_Medium=null;
    Typeface Roboto_Light=null;

    private TextView textView;

    private void webServiceDatosUsuario(){

        final JSONPerfilUsuario jsonPerfilUsuario=new JSONPerfilUsuario(this);



        tDatosUsuario = new Thread() {
            @Override
            public void run()
            {
                try {
                    resultadoPerfilUsuario =jsonPerfilUsuario.run(app.getUsuario().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerDatosUsuario.post(new Runnable() {
                    public void run() {

                        if(resultadoPerfilUsuario.getResultado()==1)
                        {
                            app.setUsuario(resultadoPerfilUsuario.getUsuario());
                            app.guardarEnPreferencias(activity);
                            actualizarDatosPantalla();
                        }
                    }
                });
            }
        };

        handlerDatosUsuario = new Handler();
        tDatosUsuario.start();
    }



    private void comprobarEditarDatosUsuario(){

        String name = edtxt_nombre.getText().toString();
        String email = edtxt_email.getText().toString();
        String pass = edtxt_pass.getText().toString();

        if (name.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
            dlg.setMessage(getString(R.string.error_no_nombre_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_nombre.requestFocus();
                    TresAndroides.showKeyboard(ActivityPerfilUsuario.this);
                }
            });
            dlg.show();

        }else if (email.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
            dlg.setMessage(getString(R.string.error_no_email_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(ActivityPerfilUsuario.this);
                }
            });
            dlg.show();

        }else if (!TresAndroides.validateEmail(email)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
            dlg.setMessage(getString(R.string.error_no_email_valido));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(ActivityPerfilUsuario.this);
                }
            });
            dlg.show();
        }else if(pass.length()>0 || edtxt_pass2.getText().toString().length()>0)
        {
            if (pass.length() < 4){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
                dlg.setMessage(  getString(R.string.error_no_pass_intro));
                dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        edtxt_pass.requestFocus();
                        TresAndroides.showKeyboard(ActivityPerfilUsuario.this);
                    }
                });
                dlg.show();
            }else if (!edtxt_pass2.getText().toString().equals(pass)){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
                dlg.setMessage(  getString(R.string.error_no_pass_match));
                dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        edtxt_pass.requestFocus();
                        TresAndroides.showKeyboard(ActivityPerfilUsuario.this);
                    }
                });
                dlg.show();
            }
            else if (!TresAndroides.isOnline(ActivityPerfilUsuario.this)){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
                dlg.setMessage(getString(R.string.error_no_internet));
                dlg.setPositiveButton(getString(R.string.aceptar), null).show();
            }else{
                webServiceEditarUsuario();
            }
        }else if (!TresAndroides.isOnline(ActivityPerfilUsuario.this)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityPerfilUsuario.this);
            dlg.setMessage(getString(R.string.error_no_internet));
            dlg.setPositiveButton(getString(R.string.aceptar), null).show();
        }else{
            webServiceEditarUsuario();
        }
    }

    private void webServiceEditarUsuario(){

        final JSONEditarUsuario jsonEditarUsuario=new JSONEditarUsuario(this);
        final Usuario usuario=new Usuario();

        //Campos obligatorios
        usuario.setId(app.getUsuario().getId());
        usuario.setEmail(edtxt_email.getText().toString());
        usuario.setNombre(edtxt_nombre.getText().toString());
        usuario.setCodigo_pin(edtxt_codigo_pin.getText().toString());

        if(edtxt_pass.getText().toString().length()>0)
        {
            usuario.setPassword(edtxt_pass.getText().toString());
        }

        progressBar.setVisibility(View.VISIBLE);
        lyDisabled.setVisibility(View.VISIBLE);

        tRegistro = new Thread() {
            @Override
            public void run()
            {
                try {
                    resultadoEditarUsuario =jsonEditarUsuario.run(usuario);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerRegistro.post(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        lyDisabled.setVisibility(View.GONE);

                        if(resultadoEditarUsuario==0)
                        {
                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage("No ha sido posible editar tus datos de usuario.\n\nPor favor, revisa los datos y vuelve a intentarlo.");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();

                        }
                        else
                        {
                            app.setUsuario(usuario);
                            app.guardarEnPreferencias(activity);

                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage("Tus datos han sido modificados correctamente.");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();
                        }
                    }
                });
            }
        };

        handlerRegistro = new Handler();
        tRegistro.start();
    }

    private void actualizarDatosPantalla()
    {
        edtxt_nombre.setText(app.getUsuario().getNombre());
        edtxt_email.setText(app.getUsuario().getEmail());
        edtxt_codigo_pin.setText(app.getUsuario().getCodigo_pin());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_usuario);

        activity = this;
        app.cargarAplicacionDePreferencias(this);

        edtxt_nombre= this.findViewById(R.id.edtxt_nombre);
        edtxt_email= this.findViewById(R.id.edtxt_email);
        edtxt_pass= this.findViewById(R.id.edtxt_pass);
        edtxt_pass2= this.findViewById(R.id.edtxt_pass2);
        edtxt_codigo_pin= this.findViewById(R.id.edtxt_codigo_pin);
        textView= this.findViewById(R.id.txt_titulo);

        Roboto_Medium=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        Roboto_Light=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");

        edtxt_nombre.setTypeface(Roboto_Light);
        edtxt_email.setTypeface(Roboto_Light);
        edtxt_pass.setTypeface(Roboto_Light);
        edtxt_pass2.setTypeface(Roboto_Light);
        edtxt_codigo_pin.setTypeface(Roboto_Light);
        textView.setTypeface(Roboto_Medium);


        actualizarDatosPantalla();

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        lyDisabled = findViewById(R.id.ly_disabled);
        lyDisabled.setVisibility(View.GONE);

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarEditarDatosUsuario();
            }
        });

        findViewById(R.id.done_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarEditarDatosUsuario();
            }
        });

        if (!TresAndroides.isOnline(ActivityPerfilUsuario.this))
        {
            webServiceDatosUsuario();
        }
