package com.bfree.app;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.clases.DispositivosControlados;
import com.bfree.app.clases.ResultadoPerfilUsuario;
import com.bfree.app.clases.Usuario;
import com.bfree.app.json.JSONBorrarVinculacionDisposiivo;
import com.bfree.app.json.JSONComprobarVinculacionesDisponibles;
import com.bfree.app.json.JSONDispositivosControlados;
import com.bfree.app.json.JSONEditarUsuario;
import com.bfree.app.json.JSONPerfilUsuario;
import com.bfree.app.json.JSONVincularDispositivosUsuario;
import com.bfree.app.remoto.ActivityPrincipalRemoto;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import spencerstudios.com.bungeelib.Bungee;

import static android.view.View.GONE;


public class FragmentEditarPerfil extends Fragment{

    private View view;
    private Context context;

    private ProgressBar progressBar;
    private LinearLayout lyDisabled;

    private EditText edtxt_nombre,edtxt_email,edtxt_pass,edtxt_pass2,edtxt_codigo_pin;
    private Thread tDatosUsuario;
    private Handler handlerDatosUsuario;
    private TextView textView;

    private ResultadoPerfilUsuario resultadoPerfilUsuario=new ResultadoPerfilUsuario();
    private Thread tRegistro;

    private int resultadoEditarUsuario=0;
    private Handler handlerRegistro;

    private Aplicacion app;

    private ScrollView scrollView;

    public FragmentEditarPerfil() {}


    public static FragmentEditarPerfil newInstance() {
        FragmentEditarPerfil fragmentFirst = new FragmentEditarPerfil();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kEditarPerfil);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_perfilusuario, container, false);

        init();
        return view;
    }


    private void init(){
        app = new Aplicacion();
        app.cargarUsuarioDePreferencias(context);

        edtxt_nombre = view.findViewById(R.id.edtxt_nombre);
        edtxt_email = view.findViewById(R.id.edtxt_email);
        edtxt_pass = view.findViewById(R.id.edtxt_pass);
        edtxt_pass2 = view.findViewById(R.id.edtxt_pass2);
        edtxt_codigo_pin = view.findViewById(R.id.edtxt_codigo_pin);
        textView = view.findViewById(R.id.txt_titulo);

        scrollView = view.findViewById(R.id.scrollView);

        actualizarDatosPantalla();

        progressBar = view.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        lyDisabled = view.findViewById(R.id.ly_disabled);
        lyDisabled.setVisibility(View.GONE);

        view.findViewById(R.id.done_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarEditarDatosUsuario();
            }
        });

        view.findViewById(R.id.done_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarEditarDatosUsuario();
            }
        });

        if (!TresAndroides.isOnline(context))
        {
            webServiceDatosUsuario();
        }

        edtxt_nombre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(v.getScrollX(),v.getScrollY() + 300 );
            }
        });


        edtxt_pass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(v.getScrollX(),v.getScrollY() + 500 );
            }
        });

        edtxt_pass2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(v.getScrollX(),v.getScrollY() + 500 );
            }
        });

        edtxt_codigo_pin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                scrollView.scrollTo(v.getScrollX(),v.getScrollY() + 500 );
            }
        });

        edtxt_nombre.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollView.scrollTo(view.getScrollX(),view.getScrollY() + 300 );
            }
        });

        edtxt_pass.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollView.scrollTo(view.getScrollX(),view.getScrollY() + 500 );
            }
        });

        edtxt_pass2.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollView.scrollTo(view.getScrollX(),view.getScrollY() + 500 );
            }
        });

        edtxt_codigo_pin.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                scrollView.scrollTo(view.getScrollX(),view.getScrollY() + 700 );
            }
        });


        actualizarDispositivosVinculados(app.getDispositivosControlados());
        final Handler handler = new Handler();
        new Thread(){
            @Override
            public void run() {
                super.run();
                final Aplicacion app = new Aplicacion();
                List<DispositivosControlados> dispo = new ArrayList<>();
                app.cargarAplicacionDePreferencias(context);
                JSONDispositivosControlados js = new JSONDispositivosControlados(context);
                try {
                    dispo = js.run(app.getUsuario().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final List<DispositivosControlados>  dispositivosControlados =  dispo;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        app.setDispositivosControlados(dispositivosControlados);
                        app.guardarEnPreferencias(context);
                        actualizarDispositivosVinculados(dispositivosControlados);

                    }
                });
            }
        }.start();

        view.findViewById(R.id.lyAddDispo).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.ly_disabled).setVisibility(View.VISIBLE);
                final Handler handler = new Handler();

                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        JSONComprobarVinculacionesDisponibles jsonComprobarVinculacionesDisponibles
                                = new JSONComprobarVinculacionesDisponibles(context);
                        boolean b = false;
                        try {
                            b = jsonComprobarVinculacionesDisponibles.run(app.getUsuario().getId());
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        final boolean resultado = b;

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (resultado){
                                    Intent i = new Intent(context, ActivityAddDispositivo.class);
                                    Bungee.fade(context);
                                    startActivity(i);
                                }else {
                                    new android.app.AlertDialog.Builder(context)
                                            .setTitle("Has alcanzado el máximo de vinculaciones")
                                            .setMessage("Aumenta tu plan de Bfree o desvincula algún dispositivo")
                                            .setPositiveButton("Aceptar", null).show();
                                    view.findViewById(R.id.pgBar).setVisibility(View.GONE);
                                    view.findViewById(R.id.lyDis).setVisibility(View.GONE);
                                }
                            }
                        });
                    }
                }.start();


            }
        });

    }

    private void webServiceDatosUsuario(){

        final JSONPerfilUsuario jsonPerfilUsuario=new JSONPerfilUsuario(context);



        tDatosUsuario = new Thread() {
            @Override
            public void run()
            {
                try {
                    resultadoPerfilUsuario =jsonPerfilUsuario.run(app.getUsuario().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerDatosUsuario.post(new Runnable() {
                    public void run() {

                        if(resultadoPerfilUsuario.getResultado()==1)
                        {
                            app.setUsuario(resultadoPerfilUsuario.getUsuario());
                            app.guardarEnPreferencias(context);
                            actualizarDatosPantalla();
                        }
                    }
                });
            }
        };

        handlerDatosUsuario = new Handler();
        tDatosUsuario.start();
    }

    private void comprobarEditarDatosUsuario(){

        String name = edtxt_nombre.getText().toString();
        String email = edtxt_email.getText().toString();
        String pass = edtxt_pass.getText().toString();

        if (name.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
            dlg.setMessage(context.getString(R.string.error_no_nombre_intro));
            dlg.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_nombre.requestFocus();
                    TresAndroides.showKeyboard(context, view);
                }
            });
            dlg.show();

        }else if (email.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
            dlg.setMessage(context.getString(R.string.error_no_email_intro));
            dlg.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(context, view);
                }
            });
            dlg.show();

        }else if (!TresAndroides.validateEmail(email)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
            dlg.setMessage(context.getString(R.string.error_no_email_valido));
            dlg.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(context, view);
                }
            });
            dlg.show();
        }else if(pass.length()>0 || edtxt_pass2.getText().toString().length()>0)
        {
            if (pass.length() < 4){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                dlg.setMessage(  context.getString(R.string.error_no_pass_intro));
                dlg.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        edtxt_pass.requestFocus();
                        TresAndroides.showKeyboard(context, view);
                    }
                });
                dlg.show();
            }else if (!edtxt_pass2.getText().toString().equals(pass)){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                dlg.setMessage(  context.getString(R.string.error_no_pass_match));
                dlg.setPositiveButton(context.getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        edtxt_pass.requestFocus();
                        TresAndroides.showKeyboard(context, view);
                    }
                });
                dlg.show();
            }
            else if (!TresAndroides.isOnline(context)){
                AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                dlg.setMessage(context.getString(R.string.error_no_internet));
                dlg.setPositiveButton(context.getString(R.string.aceptar), null).show();
            }else{
                webServiceEditarUsuario();
            }
        }else if (!TresAndroides.isOnline(context)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
            dlg.setMessage(context.getString(R.string.error_no_internet));
            dlg.setPositiveButton(context.getString(R.string.aceptar), null).show();
        }else{
            webServiceEditarUsuario();
        }
    }

    private void actualizarDatosPantalla()
    {
        edtxt_nombre.setText(app.getUsuario().getNombre());
        edtxt_email.setText(app.getUsuario().getEmail());
        edtxt_codigo_pin.setText(app.getUsuario().getCodigo_pin());
    }

    private void webServiceEditarUsuario(){

        final JSONEditarUsuario jsonEditarUsuario=new JSONEditarUsuario(context);
        final Usuario usuario=new Usuario();

        //Campos obligatorios
        usuario.setId(app.getUsuario().getId());
        usuario.setEmail(edtxt_email.getText().toString());
        usuario.setNombre(edtxt_nombre.getText().toString());
        usuario.setCodigo_pin(edtxt_codigo_pin.getText().toString());

        if(edtxt_pass.getText().toString().length()>0)
        {
            usuario.setPassword(edtxt_pass.getText().toString());
        }

        progressBar.setVisibility(View.VISIBLE);
        lyDisabled.setVisibility(View.VISIBLE);

        tRegistro = new Thread() {
            @Override
            public void run()
            {
                try {
                    resultadoEditarUsuario =jsonEditarUsuario.run(usuario);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerRegistro.post(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        lyDisabled.setVisibility(View.GONE);

                        if(resultadoEditarUsuario==0)
                        {
                            AlertDialog.Builder dlg = new AlertDialog.Builder(context);
                            dlg.setMessage("No ha sido posible editar tus datos de usuario.\n\nPor favor, revisa los datos y vuelve a intentarlo.");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();

                        }
                        else
                        {
                            app.setUsuario(usuario);
                            app.guardarEnPreferencias(context);

                            AlertDialog.Builder dlg = new AlertDialog.Builder(context);
                            dlg.setMessage("Tus datos han sido modificados correctamente.");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();
                        }
                    }
                });
            }
        };

        handlerRegistro = new Handler();
        tRegistro.start();
    }

    private void actualizarDispositivosVinculados(List<DispositivosControlados> dispositivosControlados){

        LayoutInflater inflater = LayoutInflater.from(context);

        LinearLayout ly = view.findViewById(R.id.lyDispositivos);
        ly.removeAllViews();
        if (dispositivosControlados!=null)
            for (int i = 0; i < dispositivosControlados.size(); ++i){
                View v = inflater.inflate(R.layout.dispositivos_controlados_blanco, null);

                TextView etiqueta = v.findViewById(R.id.etiqueta);
                TextView nombre = v.findViewById(R.id.usuario);

                if (i != 0)
                    v.findViewById(R.id.lyTop).setVisibility(GONE);

                etiqueta.setText(dispositivosControlados.get(i).getEtiqueta());
              //  nombre.setText(dispositivosControlados.get(i).getNombrehijo());

                final int idusu = dispositivosControlados.get(i).getId_usuario();
                final int iddisp = dispositivosControlados.get(i).getId_dispositivo();
                v.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Aplicacion app = new Aplicacion();
                        app.cargarAplicacionDePreferencias(context);
                        cargarBfreeRemoto(idusu, iddisp);
                    }
                });

                final DispositivosControlados disp = dispositivosControlados.get(i);
                v.setOnLongClickListener(new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(final View view) {

                        new android.app.AlertDialog.Builder(context).
                                setTitle("¿Deseas desvincular este dispositivo?")
                                .setPositiveButton("Devincular", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        view.findViewById(R.id.ly_disabled).setVisibility(View.VISIBLE);
                                        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                                        final Handler handler = new Handler();
                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                final Aplicacion app = new Aplicacion();
                                                app.cargarAplicacionDePreferencias(context);
                                                JSONBorrarVinculacionDisposiivo jsonBorrarVinculacionDisposiivo =
                                                        new JSONBorrarVinculacionDisposiivo(context);
                                                try {
                                                    jsonBorrarVinculacionDisposiivo.run(disp, app.getUsuario().getId());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }

                                                List<DispositivosControlados> dispo = new ArrayList<>();
                                                app.cargarAplicacionDePreferencias(context);
                                                JSONDispositivosControlados js = new JSONDispositivosControlados(context);
                                                try {
                                                    dispo = js.run(app.getUsuario().getId());
                                                } catch (JSONException e) {
                                                    e.printStackTrace();
                                                }
                                                final List<DispositivosControlados>  dispositivosControlados =  dispo;

                                                handler.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        app.setDispositivosControlados(dispositivosControlados);
                                                        app.guardarEnPreferencias(context);
                                                        actualizarDispositivosVinculados(dispositivosControlados);
                                                        view.findViewById(R.id.ly_disabled).setVisibility(View.GONE);
                                                        view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                                    }
                                                });
                                            }
                                        }.start();
                                    }
                                }).setNegativeButton("Cancelar", null).show();


                        return true;
                    }
                });

                ly.addView(v);
            }
        /*if (dispositivosControlados!=null) {
            if (dispositivosControlados.size() == 0) {
                view.findViewById(R.id.txtNoDispo).setVisibility(View.VISIBLE);
            }else{
                view.findViewById(R.id.txtNoDispo).setVisibility(View.GONE);
            }
        }else
            view.findViewById(R.id.txtNoDispo).setVisibility(View.VISIBLE);
        */
        ly.invalidate();
    }

    private void cargarBfreeRemoto(final int idUsuario, final int idDisp){
        view.findViewById(R.id.ly_disabled).setVisibility(View.VISIBLE);
        view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);

        final Handler handler = new Handler();

        new Thread(){
            @Override
            public void run() {
                super.run();
                final Aplicacion app = new Aplicacion();
                final List<DispositivosControlados> dispositivosControlados = new ArrayList<>();
                app.cargarAplicacionDePreferencias(context);
                JSONDispositivosControlados js = new JSONDispositivosControlados(context);
                try {
                    while (app.getUsuario() == null){
                        app.cargarAplicacionDePreferencias(context);
                    }
                    dispositivosControlados.addAll(js.run(app.getUsuario().getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        app.setDispositivosControlados(dispositivosControlados);
                        app.guardarEnPreferencias(context);
                        actualizarDispositivosVinculados(dispositivosControlados);

                        int pos = -1;

                        for (int i = 0; i < dispositivosControlados.size(); ++i ){
                            if (dispositivosControlados.get(i).getId_usuario() == idUsuario
                                    && dispositivosControlados.get(i).getId_dispositivo() == idDisp)
                                pos = i;
                        }

                        if (pos == -1){
                            new android.app.AlertDialog.Builder(context).
                                    setTitle("¡Ha ocurrido un error")
                                    .setMessage("Ha ocurrido un error con el dispositivo, intentalo más tarde")
                                    .setPositiveButton("Aceptar", null).show();
                        }else{
                            Intent intent = new Intent(context, ActivityPrincipalRemoto.class);
                            final DispositivosControlados dispositivo = dispositivosControlados.get(pos);

                            if (dispositivo.getEstado() != 1){
                                new android.app.AlertDialog.Builder(context)
                                        .setTitle("Dispositivo no vinculado")
                                        .setMessage("El usuario ha rechazado o aun no ha aceptado la vinculación")
                                        .setPositiveButton("Aceptar", null).
                                        setNegativeButton("Volver a enviar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                view.findViewById(R.id.ly_disabled).setVisibility(View.VISIBLE);
                                                view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                                                final Handler handler = new Handler();
                                                new Thread(){
                                                    @Override
                                                    public void run() {
                                                        super.run();
                                                        JSONVincularDispositivosUsuario
                                                                jsonVincularDispositivosUsuario =
                                                                new JSONVincularDispositivosUsuario(context);
                                                        try {
                                                            jsonVincularDispositivosUsuario.run(
                                                                    app.getUsuario().getId(),
                                                                    dispositivo.getId_usuario(),
                                                                    String.valueOf(dispositivo.getId_dispositivo()));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        handler.post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                new android.app.AlertDialog.Builder(context)
                                                                        .setTitle("Vinculación enviada")
                                                                        .setPositiveButton("Aceptar", null)
                                                                        .show();
                                                                view.findViewById(R.id.ly_disabled).setVisibility(View.GONE);
                                                                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                                            }
                                                        });
                                                    }
                                                }.start();
                                            }
                                        }).show();
                                view.findViewById(R.id.ly_disabled).setVisibility(View.GONE);
                                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                            }else {
                                intent.putExtra("accesoRemoto", true);
                                intent.putExtra("idUsuario", dispositivo.getId_usuario());
                                intent.putExtra("idDispositivo", dispositivo.getId_dispositivo());
                                intent.putExtra("horas", dispositivo.getUltima_desconexion_hh());
                                intent.putExtra("minutos", dispositivo.getUltima_desconexion_mm());
                                intent.putExtra("notificaciones", dispositivo.isUltima_desconexion_bloquear_apps());
                                intent.putExtra("llamadas", dispositivo.isUltima_desconexion_bloquear_llamadas());
                                intent.putExtra("listaTfnos", dispositivo.getUltima_desconexion_lista_contactos());
                                intent.putExtra("listaApps", dispositivo.getUltima_desconexion_lista_apps());
//                                intent.putExtra("nombre", dispositivo.getNombrehijo());
                                intent.putExtra("etiqueta", dispositivo.getEtiqueta());
                                intent.putExtra("estadoDesconexion", dispositivo.getEstadoDesconexion());

                                view.findViewById(R.id.ly_disabled).setVisibility(View.GONE);
                                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                Bungee.fade(context);
                                startActivity(intent);
                            }

                        }
                    }
                });
            }
        }.start();
    }



}
