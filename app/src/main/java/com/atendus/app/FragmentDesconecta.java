package com.bfree.app;

import android.Manifest;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.TimePicker;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.clases.ConfiguracionDesconexion;
import com.bfree.app.clases.DispositivosControlados;
import com.bfree.app.json.JSONEnviarDesconexionRemota;
import com.bfree.app.json.JSONEnviarEstadoDispositivo;
import com.bfree.app.json.JSONPararDesconexionRemota;
import com.bfree.app.remoto.ActivityPrincipalRemoto;
import com.bfree.app.remoto.ActivityTelefonosDesconectaRemoto;
import com.bfree.app.services.NotificationMonitor;
import com.bfree.app.services.ServicioDesconexion;
import com.bfree.app.utilidades.CustomViewPager;
import com.bfree.app.utilidades.PhoneStateManager;
import com.bfree.app.utilidades.ViewPagerManager;

import org.json.JSONException;

import java.util.Calendar;

import spencerstudios.com.bungeelib.Bungee;


public class FragmentDesconecta extends Fragment{

    private Context context;
    private View view;
    public ImageView llamadas;
    public ImageView notificaciones;

    private SeekBar seekBarTiempo;
    TextView txtTiempo;

    private  ImageView desconectar;



    private CountDownTimer contadorDesconexion;
    private ServiceConnection connection;
    ActivityPrincipal activityPrincipal;

    private boolean fromTimer = false;

    private int seconds = 0;

    private Intent servicioDesconexion;
    private Intent notificationMonitor = null;

    private static CustomViewPager mViewPager;

    private static DrawerLayout drawer;

    private static Intent intent;
    private ConfiguracionDesconexion desconexionProgramada = null;

    private static boolean dispositivoRemoto = false;
    private static DispositivosControlados dispositivo;

    public FragmentDesconecta(){}


    public static FragmentDesconecta newInstance(CustomViewPager pager, DrawerLayout d, Intent intnt) {
        FragmentDesconecta fragmentFirst = new FragmentDesconecta();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kDesconecta);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        mViewPager = pager;
        drawer = d;
        intent = intnt;
        dispositivoRemoto = false;
        return fragmentFirst;
    }

    public static FragmentDesconecta newInstance(Intent intnt, DispositivosControlados d) {
        FragmentDesconecta fragmentFirst = new FragmentDesconecta();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kDesconecta);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        dispositivoRemoto = true;
        dispositivo = d;
        intent = intnt;
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    // Inflate the view for the fragment based on layout XML
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_desconecta, container, false);

        if (!dispositivoRemoto)
            init();
        else
            initRemoto();
        return view;
    }


    private void mostrarDialogoTiempo()
    {
        final Calendar myCalender = Calendar.getInstance();
        int hour = PhoneStateManager.getInstance().getDesconexion().getHorasDesconexion();
        int minute = PhoneStateManager.getInstance().getDesconexion().getMinutosDesconexion();

        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    myCalender.set(Calendar.MINUTE, minute);

                    //int miliseconds = hourOfDay * 3600 + minute *60;
                   // seekBarTiempo.setProgress(miliseconds);
                    modificarTiempoDesconexion2(hourOfDay, minute, 0);


                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle(context.getString(R.string.modificar_tiempo_desconexion));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }
    private void mostrarDialogoTiempoRemoto()
    {
        final Calendar myCalender = Calendar.getInstance();
        int hour = dispositivo.getUltima_desconexion_hh();
        int minute = dispositivo.getUltima_desconexion_mm();

        TimePickerDialog.OnTimeSetListener myTimeListener = new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                if (view.isShown()) {
                    myCalender.set(Calendar.HOUR_OF_DAY, hourOfDay);
                    myCalender.set(Calendar.MINUTE, minute);

                    //int miliseconds = hourOfDay * 3600 + minute *60;
                    // seekBarTiempo.setProgress(miliseconds);
                    modificarTiempoDesconexion3(hourOfDay, minute, 0);


                }
            }
        };
        TimePickerDialog timePickerDialog = new TimePickerDialog(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar, myTimeListener, hour, minute, true);
        timePickerDialog.setTitle(context.getString(R.string.modificar_tiempo_desconexion));
        timePickerDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        timePickerDialog.show();
    }

    public void mostrarDialogoSeleccionarContactosPermitidos()
    {
        Intent i = new Intent(context, ActivityTelefonosDesconecta.class);
        Bungee.fade(context);
        context.startActivity(i);
    }


    private void init(){

        llamadas = view.findViewById(R.id.llamadas);
        notificaciones = view.findViewById(R.id.notificaciones);

        seekBarTiempo = view.findViewById(R.id.seekBarTiempo);
        txtTiempo = view.findViewById(R.id.txtTiempo);

        desconectar = view.findViewById(R.id.desconectar);
        PhoneStateManager.getInstance().cargarDatosDeAplicacion(context);
        cargarLayoutDatosPreferencias();

        //Botones de bloqueo de llamadas y notificaciones


        llamadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!PhoneStateManager.getInstance().isDesconectado())
                {
                    if (PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()){
                        llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_off));
                        PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(false);

                    }else {
                        llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
                        PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(true);


                    }
                }
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);

            }
        });

        notificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!PhoneStateManager.getInstance().isDesconectado()) {

                    if (PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
                        notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));
                        PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(false);
                      }else {
                        notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
                        PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(true);
                    }
                    PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);

                }

            }
        });

        notificaciones.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

               if(!PhoneStateManager.getInstance().isDesconectado()) {
                   mViewPager.setCurrentItem(ActivityPrincipal.kAplicaciones);
                   return true;
               }
               return false;
            }
        });

        llamadas.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if(!PhoneStateManager.getInstance().isDesconectado()) {
                        mostrarDialogoSeleccionarContactosPermitidos();
                        return true;
                }
                return false;
            }
        });

        //Seekbar y texto de tiempo



        seekBarTiempo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                double time = (double) progress / 3600;
                double floorTime = Math.floor(time);
                int horas = (int)( floorTime);
                int minutos = (int)((time - floorTime)*60);
                String tiempo;
                if (horas >= 0 && horas <=9){
                    tiempo = "0" + horas;
                }else{
                    tiempo = String.valueOf(horas);
                }

                if (!fromTimer) {
                    double minaux = (double) (minutos / 5);
                    minaux = Math.floor(minaux);
                    minutos = (int) (minaux * 5);
                }

                if (minutos >= 0 && minutos <= 9){
                    tiempo = tiempo.concat(":0" + minutos);
                }else{
                    tiempo = tiempo.concat(":" + minutos);
                }

                int seg = 0;




                progress = horas * 3600 + minutos * 60 + seg;

                txtTiempo.setText(tiempo);
                PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexion(progress, horas, minutos, seg);
                PhoneStateManager.getInstance().setMillisUntilFinished(progress*1000);
                fromTimer = false;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        txtTiempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!PhoneStateManager.getInstance().isDesconectado())
                {
                    mostrarDialogoTiempo();
                }

            }
        });

        //Desconexión



        desconectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PhoneStateManager.getInstance().isEditable() || !PhoneStateManager.getInstance().isDesconectado())
                    desconectar();


            }

        });

        if (PhoneStateManager.getInstance().isDesconectado()){
            reanudarModoDesconexion();
        }else{
            if (contadorDesconexion != null)
                contadorDesconexion.cancel();
            seekBarTiempo.setProgress(PhoneStateManager.getInstance().getDesconexion().getTiempoDesconexion());
            /*PhoneStateManager.getInstance().getDesconexion()
                    .setTiempoDesconexionActual(seekBarTiempo.getProgress());*/
            PhoneStateManager.getInstance().getDesconexion().restaurarTiempoOriginal();
            modificarTiempoDesconexion2(
                    PhoneStateManager.getInstance().getDesconexion().getHorasDesconexion(),
                    PhoneStateManager.getInstance().getDesconexion().getMinutosDesconexion(),
                    PhoneStateManager.getInstance().getDesconexion().getSegundosDesconexion());
            PhoneStateManager.getInstance().setDesconectado(false);
            quitarLayoutDesconexion();

            PhoneStateManager.getInstance().getDesconexion().restoreAudioStatus(context);

            PhoneStateManager.getInstance().guardarDatosDesconexion(context);

            if(servicioDesconexion != null){
                context.stopService(servicioDesconexion);
            }
            if(notificationMonitor != null)
                context.stopService(notificationMonitor);

            PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
            view.findViewById(R.id.ly_disable).setVisibility(View.GONE);
            if (mViewPager != null)
            mViewPager.setPagingEnabled(true);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }

    }
    private void initRemoto(){
        llamadas = view.findViewById(R.id.llamadas);
        notificaciones = view.findViewById(R.id.notificaciones);

        seekBarTiempo = view.findViewById(R.id.seekBarTiempo);
        txtTiempo = view.findViewById(R.id.txtTiempo);

        desconectar = view.findViewById(R.id.desconectar);

        if (dispositivo.isUltima_desconexion_bloquear_llamadas()){
            llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
        }else
            llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_off));

        if (dispositivo.isUltima_desconexion_bloquear_apps())
            notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
        else
            notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));

        int tiempoDesconexion = dispositivo.getUltima_desconexion_hh()*3600 + dispositivo.getUltima_desconexion_mm()*60;

        modificarTiempoDesconexion(tiempoDesconexion, false);
        seekBarTiempo.setProgress(tiempoDesconexion);

        llamadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dispositivo.isUltima_desconexion_bloquear_llamadas()){
                    llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_off));
                    dispositivo.setUltima_desconexion_bloquear_llamadas(false);
                }else {
                    llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
                    dispositivo.setUltima_desconexion_bloquear_llamadas(true);
                }
            }
        });

        notificaciones.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dispositivo.isUltima_desconexion_bloquear_apps()){
                    notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));
                    dispositivo.setUltima_desconexion_bloquear_apps(false);
                }else {
                    notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
                    dispositivo.setUltima_desconexion_bloquear_apps(true);
                }

            }
        });

        notificaciones.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                mViewPager.setCurrentItem(ActivityPrincipal.kAplicaciones);
                return true;
            }
        });

        llamadas.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                Intent i = new Intent(context, ActivityTelefonosDesconectaRemoto.class);
                Bungee.fade(context);
                context.startActivity(i);
                return true;
            }
        });


        seekBarTiempo.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                double time = (double) progress / 3600;
                double floorTime = Math.floor(time);
                int horas = (int)( floorTime);
                int minutos = (int)((time - floorTime)*60);
                String tiempo;
                if (horas >= 0 && horas <=9){
                    tiempo = "0" + horas;
                }else{
                    tiempo = String.valueOf(horas);
                }

                if (!fromTimer) {
                    double minaux = (double) (minutos / 5);
                    minaux = Math.floor(minaux);
                    minutos = (int) (minaux * 5);
                }

                if (minutos >= 0 && minutos <= 9){
                    tiempo = tiempo.concat(":0" + minutos);
                }else{
                    tiempo = tiempo.concat(":" + minutos);
                }

                int seg = 0;




                progress = horas * 3600 + minutos * 60 + seg;

                txtTiempo.setText(tiempo);
                dispositivo.setUltima_desconexion_hh(horas);
                dispositivo.setUltima_desconexion_mm(minutos);
                fromTimer = false;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        txtTiempo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!PhoneStateManager.getInstance().isDesconectado())
                {
                    mostrarDialogoTiempoRemoto();
                }

            }
        });


        desconectar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!dispositivo.isUltima_desconexion_bloquear_apps() &&
                        !dispositivo.isUltima_desconexion_bloquear_llamadas()){
                    AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                    dlg.setMessage(  context.getString(R.string.error_seleccionar_tipo_desconexion));
                    dlg.setPositiveButton("Aceptar",null);
                    dlg.show();
                    return;
                }
                if (dispositivo.getUltima_desconexion_hh() == 0 &&
                        dispositivo.getUltima_desconexion_mm() == 0){
                    AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                    dlg.setMessage(  context.getString(R.string.error_no_tiempo_desconexion));
                    dlg.setPositiveButton("Establecer tiempo", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mostrarDialogoTiempo();
                        }
                    });
                    dlg.setNegativeButton("Cancelar",null);
                    dlg.show();
                    return;
                }


                view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.ly_disable).setVisibility(View.VISIBLE);
                final Handler handler = new Handler();
                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        Aplicacion app = new Aplicacion();
                        app.cargarAplicacionDePreferencias(context);
                        JSONEnviarDesconexionRemota jsonEnviarDesconexionRemota =
                                new JSONEnviarDesconexionRemota(context);
                        boolean b = false;
                        try {
                            b = jsonEnviarDesconexionRemota.run(dispositivo,
                                    app.getUsuario().getId(),
                                    ActivityPrincipalRemoto.appsPermitidas,
                                    ActivityPrincipalRemoto.tfnosPermitidos);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        final boolean resultado = b;
                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (resultado) {
                                    new AlertDialog.Builder(context).
                                            setTitle("Desconexión enviada con éxito").
                                            setPositiveButton("Aceptar", null).show();

                                 //   view.findViewById(R.id.imgPause).setVisibility(View.VISIBLE);

                                    ActivityPrincipalRemoto.setBAckgroundDesconectado(true, context);
                                }

                                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                view.findViewById(R.id.ly_disable).setVisibility(View.GONE);
                            }
                        });
                    }
                }.start();

            }

        });

        if (dispositivo.getEstadoDesconexion() == 1) {
          //  view.findViewById(R.id.imgPause).setVisibility(View.VISIBLE);
        }else{
          //  view.findViewById(R.id.imgPause).setVisibility(View.GONE);
        }

       /* view.findViewById(R.id.imgPause).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                view.findViewById(R.id.ly_disable).setVisibility(View.VISIBLE);
                final Handler handler = new Handler();
                new Thread(){
                    @Override
                    public void run() {
                        boolean b = false;
                        super.run();
                        JSONPararDesconexionRemota jsonPararDesconexionRemota =
                                new JSONPararDesconexionRemota(context);
                        try {
                            b = jsonPararDesconexionRemota.run(dispositivo);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        final boolean resultado = b;

                        handler.post(new Runnable() {
                            @Override
                            public void run() {
                                if (resultado){
                                    new AlertDialog.Builder(context).
                                            setTitle("LA desconexión no se ha podido parar").
                                            setMessage("Por favor intentelo más tarde").
                                            setPositiveButton("Aceptar", null).show();
                                }else{
                                    new AlertDialog.Builder(context).
                                            setTitle("Desconexión parada con éxito").
                                            setPositiveButton("Aceptar", null).show();
                                    view.findViewById(R.id.imgPause).setVisibility(View.GONE);

                                    Window window = PhoneStateManager.getInstance().getCurrentActivity().getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(context.getColor(R.color.backgroundStart));
                                    ActivityPrincipalRemoto.setBAckgroundDesconectado(false, context);
                                }

                                view.findViewById(R.id.progressBar).setVisibility(View.GONE);
                                view.findViewById(R.id.ly_disable).setVisibility(View.GONE);
                            }
                        });


                    }
                }.start();
            }
        });*/
    }


    public void setDrawer(DrawerLayout d){
        drawer = d;
    }

    private void modificarTiempoDesconexion2(int hh, int mm, int ss){
        int progress = hh * 3600 + mm * 60 + ss;
        seekBarTiempo.setProgress(progress);


        String tiempo;

        if (hh >= 0 && hh <=9){
            tiempo = "0" + hh;
        }else{
            tiempo = String.valueOf(hh);
        }

        if (mm >= 0 && mm <= 9){
            tiempo = tiempo.concat(":0" + mm);
        }else{
            tiempo = tiempo.concat(":" + mm);
        }


        txtTiempo.setText(tiempo);
        PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexion(progress, hh, mm);
        PhoneStateManager.getInstance().setMillisUntilFinished(progress*1000);
    }

    private void modificarTiempoDesconexion3(int hh, int mm, int ss){
        int progress = hh * 3600 + mm * 60 + ss;
        seekBarTiempo.setProgress(progress);


        String tiempo;

        if (hh >= 0 && hh <=9){
            tiempo = "0" + hh;
        }else{
            tiempo = String.valueOf(hh);
        }

        if (mm >= 0 && mm <= 9){
            tiempo = tiempo.concat(":0" + mm);
        }else{
            tiempo = tiempo.concat(":" + mm);
        }


        txtTiempo.setText(tiempo);
        dispositivo.setUltima_desconexion_hh(hh);
        dispositivo.setUltima_desconexion_mm(mm);
    }
    private void modificarTiempoDesconexion(int progress, boolean fromTimer) {
        double time = (double) progress / 3600;
        double floorTime = Math.floor(time);
        int horas = (int)( floorTime);
        int minutos = (int)((time - floorTime)*60);
        String tiempo;
        if (horas >= 0 && horas <=9){
            tiempo = "0" + horas;
        }else{
            tiempo = String.valueOf(horas);
        }

        if (minutos >= 0 && minutos <= 9){
            tiempo = tiempo.concat(":0" + minutos);
        }else{
            tiempo = tiempo.concat(":" + minutos);
        }

        if (fromTimer) {
            double time2 = (double) progress / 60;
            double floorTime2 = Math.floor(time2);
            int seg = (int) ((time2 - floorTime2) * 60);

            if (seg <= 9) {
                tiempo = tiempo.concat(":0" + seg);
            } else {
                tiempo = tiempo.concat(":" + seg);
            }
        }

        txtTiempo.setText(tiempo);

        //PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexionActual(progress);
    }

    private void ponerLayoutDesconexion()
    {
        PhoneStateManager.getInstance().getLy_main().setBackground(context.getDrawable(R.drawable.gradient_background_off));

        Window window = PhoneStateManager.getInstance().getCurrentActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(context.getColor(R.color.backgroundStart_off));
    }

    private void quitarLayoutDesconexion()
    {
        PhoneStateManager.getInstance().getLy_main().setBackground(context.getDrawable(R.drawable.gradient_background));
        Window window = PhoneStateManager.getInstance().getCurrentActivity().getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(context.getColor(R.color.backgroundStart));
        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
        //PhoneStateManager.getInstance().setDesconectado(false);
        PhoneStateManager.getInstance().setFromProgramarDesconexion(false);
    }

    //Función comienzo de la desconexión
    public void desconectar(){
        if(!PhoneStateManager.getInstance().isDesconectado())
        {

            if(!PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas() &&
                    !PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones())
            {
                AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                dlg.setMessage(  context.getString(R.string.error_seleccionar_tipo_desconexion));
                dlg.setPositiveButton("Aceptar",null);
                dlg.show();
                return;
            }

            if(seekBarTiempo.getProgress()<=0)
            {
                AlertDialog.Builder dlg  = new AlertDialog.Builder(context);
                dlg.setMessage(  context.getString(R.string.error_no_tiempo_desconexion));
                dlg.setPositiveButton("Establecer tiempo", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mostrarDialogoTiempo();
                    }
                });
                dlg.setNegativeButton("Cancelar",null);
                dlg.show();
                return;
            }

            ponerLayoutDesconexion();
            //limpiarSegundos();

            //PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexionActual(seekBarTiempo.getProgress());
            PhoneStateManager.getInstance().getDesconexion().guardarTiempoOriginal();
            //PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexion(seekBarTiempo.getProgress());
            PhoneStateManager.getInstance().setDesconectado(true);



            servicioDesconexion = new Intent(context, ServicioDesconexion.class);
            notificationMonitor = new Intent(context, NotificationMonitor.class);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(servicioDesconexion);
                context.startForegroundService(notificationMonitor);
            }else{
                context.startService(servicioDesconexion);
                context.startService(notificationMonitor);
            }

            PhoneStateManager.getInstance().getNotificacioines().getList().clear();

            PhoneStateManager.getInstance().getDesconexion().setlDateStart();

            view.findViewById(R.id.ly_disable).setVisibility(View.VISIBLE);

            PhoneStateManager.getInstance().getDesconexion().setAudioStatus(context);

            if (mViewPager != null)
                mViewPager.setPagingEnabled(false);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);



            seconds = 59;
            contadorDesconexion=null;

        }
        else
        {
            if (contadorDesconexion != null)
                contadorDesconexion.cancel();
            seekBarTiempo.setProgress(PhoneStateManager.getInstance().getDesconexion().getTiempoDesconexion());
            /*PhoneStateManager.getInstance().getDesconexion()
                    .setTiempoDesconexionActual(seekBarTiempo.getProgress());*/
            PhoneStateManager.getInstance().getDesconexion().restaurarTiempoOriginal();
            modificarTiempoDesconexion2(
                    PhoneStateManager.getInstance().getDesconexion().getHorasDesconexion(),
                    PhoneStateManager.getInstance().getDesconexion().getMinutosDesconexion(),
                    PhoneStateManager.getInstance().getDesconexion().getSegundosDesconexion());
            PhoneStateManager.getInstance().setDesconectado(false);
            PhoneStateManager.getInstance().setEditable(true);
            quitarLayoutDesconexion();

            PhoneStateManager.getInstance().getDesconexion().restoreAudioStatus(context);

            PhoneStateManager.getInstance().guardarDatosDesconexion(context);

            if(servicioDesconexion != null){
                context.stopService(servicioDesconexion);
            }
            if(notificationMonitor != null)
                context.stopService(notificationMonitor);

            PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
            view.findViewById(R.id.ly_disable).setVisibility(View.GONE);
            if (mViewPager != null)
                mViewPager.setPagingEnabled(true);
            drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }



        new Thread(){
            @Override
            public void run() {
                super.run();
                if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
                final String iddisp = telephonyManager.getDeviceId();

                JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                        new JSONEnviarEstadoDispositivo(context);
                int estdao = 0;
                if (PhoneStateManager.getInstance().isDesconectado())
                    estdao = 1;
                try {
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(context);
                    jsonEnviarEstadoDispositivo.run(app.getUsuario().getId(), iddisp, estdao);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }.start();

    }


    private void cargarLayoutDatosPreferencias()
    {
        PhoneStateManager.getInstance().cargarDatosDeAplicacion(context);
        if (PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()){
            llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
        }else {
            llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_off));
        }

        if (PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
            notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
        }else {
            notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));
        }

        modificarTiempoDesconexion(PhoneStateManager.getInstance().getDesconexion()
                .getTiempoDesconexion(), false);
        seekBarTiempo.setProgress(PhoneStateManager.getInstance().getDesconexion()
                .getTiempoDesconexion());

    }


    @Override
    protected void finalize() {

    }

    private void reanudarModoDesconexion(){
        ponerLayoutDesconexion();
        if (mViewPager != null)
            mViewPager.setPagingEnabled(false);
        view.findViewById(R.id.ly_disable).setVisibility(View.VISIBLE);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        Log.d("BFree ", "Modo desconexión reanudado" );

    }

    public void setmViewPager(CustomViewPager pager){
        mViewPager = pager;
    }

    public void tickDesconectar(long millisUntilFinished){
        int segundos = (int)(millisUntilFinished / 1000);
        fromTimer = true;
        seekBarTiempo.setProgress(segundos);
        String tiempo = txtTiempo.getText().toString();

        double ss = (double) segundos / 60;
        ss = ss - Math.floor(ss);
        ss *=60;
        ss = Math.round(ss);
        int secs = (int)ss;

        if (secs <= 9) {
            tiempo = tiempo.concat(":0" + secs);
        } else {
            tiempo = tiempo.concat(":" + secs);
        }
        /*seconds--;
        if (seconds < 0){
            seconds = 59;
        }*/
        txtTiempo.setText(tiempo);

        if (llamadas != null){
            if (PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()){
                llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_on));
            }else {
                llamadas.setImageDrawable(context.getDrawable(R.drawable.llamadas_off));
            }
        }

        if (notificaciones != null){
            if (PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()){
                notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_on));
            }else {
                notificaciones.setImageDrawable(context.getDrawable(R.drawable.notificaciones_off));
            }
        }
        ponerLayoutDesconexion();
        if (mViewPager!=null)
        mViewPager.setPagingEnabled(false);

        Log.d("BFree desconexion", tiempo);
        ponerLayoutDesconexion();

    }

    public void finishFromTimer(){
        quitarLayoutDesconexion();
        //PhoneStateManager.getInstance().setDesconectado(false);

        if(servicioDesconexion != null){
            context.stopService(servicioDesconexion);
        }
        if(notificationMonitor != null)
            context.stopService(notificationMonitor);


        PhoneStateManager.getInstance().getDesconexion().restaurarTiempoOriginal();
        modificarTiempoDesconexion2(
                PhoneStateManager.getInstance().getDesconexion().getHorasDesconexion(),
                PhoneStateManager.getInstance().getDesconexion().getMinutosDesconexion(),
                PhoneStateManager.getInstance().getDesconexion().getSegundosDesconexion());

        PhoneStateManager.getInstance().getDesconexion().restoreAudioStatus(context);
        PhoneStateManager.getInstance().setDesconectado(false);
        PhoneStateManager.getInstance().guardarDatosEnAplicacion(context);
        view.findViewById(R.id.ly_disable).setVisibility(View.GONE);
        if (mViewPager!=null)
        mViewPager.setPagingEnabled(true);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    public void desconectarOnUiThread(){
        view.post(new Runnable() {
            @Override
            public void run() {
                desconectar();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();


    }
}
