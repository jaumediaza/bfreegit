package com.bfree.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.clases.Usuario;
import com.bfree.app.json.JSONChild;
import com.bfree.app.json.JSONPremiumStripe;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ActivityStripe extends AppCompatActivity {

    private Aplicacion app;
    private static Activity activity;
    private Context contexto;
    int premium;
    TextView txtSaludo;
    Usuario usuario;
    String scrKey;
    Handler handlerStripe;

/*


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stripe);
        activity = this;
        contexto = this;



       txtSaludo = (TextView)findViewById(R.id.titleProduct);



        app = new Aplicacion();

        app.cargarAplicacionDePreferencias(this);
        usuario = app.getUsuario();

        String saludo = "Se te cargará en tu tarjeta de crédito un importe de ";
        if(getIntent()!=null){
            if(getIntent().getExtras()!=null){
                String precio = getIntent().getExtras().getString("precio");
                saludo = saludo.concat(precio);
                premium = getIntent().getExtras().getInt("premium");
            }
        }
        saludo = saludo.concat(". Por favor, añade los datos de tu tarjeta para realizar el pago seguro.");
        txtSaludo.setText(saludo);

/*
        Button accept = (Button) findViewById(R.id.btn_comprar);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                card = cardInput.getCard();
                if (card == null || !card.validateCard()) {
                    Toast toast =
                            Toast.makeText(getApplicationContext(),
                                    "La tarjeta introducida no es una tarjeta válida.", Toast.LENGTH_SHORT);


                    toast.show();
                }
                else{
                    try {
                        stripe = new Stripe(
                                contexto,
                                "pk_test_7eQbBXro347VnOwlTbql9JQv00T6yraYHL"
                        );
                        handlerStripe = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                super.run();


                                JSONPremiumStripe jsonChild = new JSONPremiumStripe(ActivityStripe.this);
                                scrKey = "";
                                try {
                                    scrKey = jsonChild.run(premium);
                                    handlerStripe.post(new Runnable() {
                                        public void run() {
                                            if (!scrKey.equals("")) {
                                                confirmPayment(createConfirmPaymentIntentParams(card));
                                            }
                                        }
                                    });
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        }.start();
                    }
                    catch(Exception e)

                    {

                    }
                }

            }
        });

        Button cancel = (Button) findViewById(R.id.btn_cancelar);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();

            }
        });
    }

    /*
    @NonNull
    private ConfirmPaymentIntentParams createConfirmPaymentIntentParams(Card card) {
        final PaymentMethodCreateParams.Card paymentMethodParamsCard =
                card.toPaymentMethodParamsCard();
        final PaymentMethodCreateParams paymentMethodCreateParams =
                PaymentMethodCreateParams.create(paymentMethodParamsCard,
                        null);
        final Map<String, Object> extraParams = new HashMap();
        extraParams.put("receipt_email",usuario.getEmail() );
        return ConfirmPaymentIntentParams.createWithPaymentMethodCreateParams(
                paymentMethodCreateParams, "sk_test_3CtonrB9eFuvQVPWAfh0zS2m000PtUQl7N",
                null,false,extraParams);
    }

    private void confirmPayment(@NonNull ConfirmPaymentIntentParams params) {
        // Optional: customize the payment authentication experience
        final PaymentAuthConfig.Stripe3ds2UiCustomization uiCustomization =
                new PaymentAuthConfig.Stripe3ds2UiCustomization.Builder()
                        .build();
        PaymentAuthConfig.init(new PaymentAuthConfig.Builder()
                .set3ds2Config(new PaymentAuthConfig.Stripe3ds2Config.Builder()
                        // set a 5 minute timeout for challenge flow
                        .setTimeout(5)
                        // customize the UI of the challenge flow
                        .setUiCustomization(uiCustomization)
                        .build())
                .build());

        stripe.confirmPayment(this, params);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        stripe.onPaymentResult(requestCode, data,
                new ApiResultCallback<PaymentIntentResult>() {
                    @Override
                    public void onSuccess(@NonNull PaymentIntentResult result) {
                        // If authentication succeeded, the PaymentIntent will
                        // have user actions resolved; otherwise, handle the
                        // PaymentIntent status as appropriate (e.g. the
                        // customer may need to choose a new payment method)

                        final PaymentIntent paymentIntent = result.getIntent();
                        final PaymentIntent.Status status =
                                paymentIntent.getStatus();
                        if (status == PaymentIntent.Status.Succeeded) {
                            // show success UI
                            Log.e("wat","wat");
                        } else if (PaymentIntent.Status.RequiresPaymentMethod
                                == status) {
                            Log.e("watb","watb");
                            // attempt authentication again or
                            // ask for a new Payment Method
                        }
                    }

                    @Override
                    public void onError(@NonNull Exception e) {
                        // handle error
                    }
                });
    }*/
/*
    @Override
    protected void onPostResume() {
        super.onPostResume();


        app.cargarAplicacionDePreferencias(this);

    }


    @Override
    protected void onResume() {
        super.onResume();


    }*/
}
