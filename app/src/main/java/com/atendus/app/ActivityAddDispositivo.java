package com.bfree.app;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.clases.DispositivoBasic;
import com.bfree.app.json.JSONBuscarDispositivosUsuario;
import com.bfree.app.json.JSONVincularDispositivosUsuario;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class ActivityAddDispositivo extends AppCompatActivity {

    List<Integer> ids = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_dispositivo);

        final TextView txt_titulo = findViewById(R.id.txt_titulo);
        final EditText edtxt_search = findViewById(R.id.edtxt_search);
        ImageView search = findViewById(R.id.search);

        findViewById(R.id.progressBar).setVisibility(View.GONE);


        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxt_search.getVisibility() == View.GONE){
                    txt_titulo.setVisibility(View.INVISIBLE);
                    edtxt_search.setVisibility(View.VISIBLE);
                    edtxt_search.requestFocus();
                    TresAndroides.showKeyboard(ActivityAddDispositivo.this);
                }else{
                   buscarDispositivo(edtxt_search.getText().toString());
                }
            }
        });

        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TresAndroides.showKeyboard(this);

    }

    private void buscarDispositivo(String correo){
        final String fCorreo = correo.trim();
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        final int idusu = app.getUsuario().getId();
        if (fCorreo.length() == 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityAddDispositivo.this);
            dlg.setMessage(getString(R.string.error_no_email_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    TresAndroides.showKeyboard(ActivityAddDispositivo.this);
                }
            });
            dlg.show();
        }else if (!TresAndroides.validateEmail(fCorreo)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityAddDispositivo.this);
            dlg.setMessage(getString(R.string.error_no_email_valido));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    TresAndroides.showKeyboard(ActivityAddDispositivo.this);
                }
            });
            dlg.show();
        }else{
            findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            findViewById(R.id.lyDis).setVisibility(View.VISIBLE);

            final Handler handler = new Handler();
            new Thread(){
                @Override
                public void run() {
                    super.run();
                    JSONBuscarDispositivosUsuario jsonBuscarDispositivosUsuario =
                            new JSONBuscarDispositivosUsuario(ActivityAddDispositivo.this);
                    List <DispositivoBasic> d = new ArrayList<>();
                    try {
                        d = jsonBuscarDispositivosUsuario.run(fCorreo, idusu);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    final List<DispositivoBasic> dispositivos = d;

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            cargarBusqueda(dispositivos);
                            findViewById(R.id.progressBar).setVisibility(View.GONE);
                            findViewById(R.id.lyDis).setVisibility(View.GONE);
                        }
                    });
                }
            }.start();
        }
    }

    private void cargarBusqueda(List<DispositivoBasic> disps){
        LinearLayout lyDispositivos = findViewById(R.id.lyDispositivos);
        LayoutInflater inflater = LayoutInflater.from(this);

        final Button btnEmparejar = findViewById(R.id.btnEmparejar);
        lyDispositivos.removeAllViews();

        for (int i = 0; i < disps.size(); ++i){
            View v = inflater.inflate(R.layout.ly_add_dispositivo, null);
            CheckBox chBx = v.findViewById(R.id.chBx);
            final int id = disps.get(i).getId();
            chBx.setText(disps.get(i).getNombre());
            chBx.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b)
                        ids.add(id);
                    else
                        ids.remove(id);
                }
            });
            lyDispositivos.addView(v);
        }
        lyDispositivos.addView(btnEmparejar);
        if (disps.size() == 0){
            findViewById(R.id.noCoincidencias).setVisibility(View.VISIBLE);
            btnEmparejar.setVisibility(View.GONE);
        }else{
            findViewById(R.id.noCoincidencias).setVisibility(View.GONE);
            btnEmparejar.setVisibility(View.VISIBLE);
        }
        lyDispositivos.invalidate();
        TresAndroides.hideKeyboard(this);

        btnEmparejar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ids.size() == 0){
                    new AlertDialog.Builder(ActivityAddDispositivo.this)
                            .setTitle("Debes seleccionar al menos un dispositivo")
                            .setPositiveButton("Aceptar", null).show();
                }else{
                    findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);

                    final Handler handler = new Handler();
                    new Thread(){
                        @Override
                        public void run() {
                            super.run();
                            Aplicacion app = new Aplicacion();
                            app.cargarAplicacionDePreferencias(ActivityAddDispositivo.this);
                            String iddisp = "";
                            for (int i : ids){
                                iddisp = iddisp.concat(String.valueOf(i) +",");
                            }
                            iddisp = iddisp.substring(0, iddisp.length()-1);
                            JSONVincularDispositivosUsuario jsonVincularDispositivosUsuario
                                    = new JSONVincularDispositivosUsuario(ActivityAddDispositivo.this);
                            try {
                                jsonVincularDispositivosUsuario.run(app.getUsuario().getId(),
                                        app.getUsuario().getId(), iddisp);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
                                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                                    finish();
                                }
                            });
                        }
                    }.start();
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TresAndroides.hideKeyboard(this);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        TresAndroides.hideKeyboard(this);
    }
}
