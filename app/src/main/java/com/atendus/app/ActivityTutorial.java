package com.bfree.app;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.app.Dialog;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.clases.AppUsage;
import com.bfree.app.clases.ConfiguracionDesconexion;
import com.bfree.app.clases.DispositivosControlados;
import com.bfree.app.json.AESCrypt;
import com.bfree.app.json.JSONBorrarVinculacionDisposiivo;
import com.bfree.app.json.JSONComprobarVinculacionesDisponibles;
import com.bfree.app.json.JSONDispositivosControlados;
import com.bfree.app.json.JSONEnviarEstadisticas;
import com.bfree.app.json.JSONEnviarTokenPush;
import com.bfree.app.json.JSONVincularDispositivosUsuario;
import com.bfree.app.remoto.ActivityPrincipalRemoto;
import com.bfree.app.services.AdminReceiver;
import com.bfree.app.services.ServicioDatos;
import com.bfree.app.splash_inicio.ActivityInicio;
import com.bfree.app.utilidades.CustomViewPager;
import com.bfree.app.utilidades.DatosUsoManager;
import com.bfree.app.utilidades.PhoneStateManager;
import com.bfree.app.utilidades.TresAndroides;
import com.bfree.app.utilidades.ViewPagerManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelAdapter;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import spencerstudios.com.bungeelib.Bungee;

import static android.view.View.GONE;

public class ActivityTutorial extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    //private static Aplicacion app=new Aplicacion();
    private static DrawerLayout drawer;
    private TextView txt_nombre;
    private TextView txt_correo;

    private boolean primeraVez = true;


    private static CustomViewPager mViewPager;
    private FragmentPagerAdapter adapterViewPager;
    List<Fragment> fragmentsList;

    private static Activity activity;

    WheelView wheelView;

    public static final int kTotal = 8; // Total de pantallas
    public static final int kNotificaciones = 6;
    public static final int kAplicaciones = 7;
    public static final int kDesconecta = 0;
    public static final int kGeolocalizacion = 3;
    public static final int kAjustes = 4;
    public static final int kDatosUso = 1;
    public static final int kProgramarDesconexion = 2;
    public static final int kEditarPerfil = 5;
    Dialog alertDialog;



    private ImageView imgDesconecta, imgAPPs;


    private ServicioDatos servicioDatos;
    private Intent servicioDatosIntent;

    private static Intent intent;


    int diapotoshow;

    @Override
    protected void onResume() {
        super.onResume();



        final WheelView wheelView = (WheelView)findViewById(R.id.wheelview);
        wheelView.setAdapter(new WheelAdapter() {
            @Override
            public Drawable getDrawable(int position) {
                switch(position){
                    case 0:
                        return getResources().getDrawable(R.drawable.iconos_01);
                    case 1:
                        return getResources().getDrawable(R.drawable.iconos_07);
                    case 2:
                        return getResources().getDrawable(R.drawable.iconos_06);
                    case 3:
                        return getResources().getDrawable(R.drawable.iconos_08);
                    case 4:
                        return getResources().getDrawable(R.drawable.iconos_05);
                    case 5:
                        return getResources().getDrawable(R.drawable.iconos_04);
                    case 6:
                        return getResources().getDrawable(R.drawable.iconos_03);
                    case 7:
                        return getResources().getDrawable(R.drawable.iconos_02);
                    default:
                        return getResources().getDrawable(R.drawable.iconos_01);
                }

                //return drawable here - the position can be seen in the gifs above
            }

            @Override
            public int getCount() {
                return 8;
            }
        });

        TextView txtUso = findViewById(R.id.txtTiempoUso);
        TextView txtDesbloqueos = findViewById(R.id.txtDesbloqueos);
        long millis = DatosUsoManager.getInstance().getAjustes().getUsageTime();
        String uso = TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.MILLISECONDS.toHours(millis) * 60) + "m";
        txtUso.setText(uso);

        txtDesbloqueos.setText(String.valueOf(DatosUsoManager.getInstance().getAjustes().getDesbloqueos()));

        ViewPagerManager.getInstance().pager = mViewPager;
        ViewPagerManager.getInstance().drawer = drawer;


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mCurrentPosition;
            private int mScrollState;
            private int mPreviousPosition;
            private void setNextItemIfNeeded() {
                if (!isScrollStateSettling()) {
                    handleSetNextItem();
                }
            }
            private boolean isScrollStateSettling() {
                return mScrollState == ViewPager.SCROLL_STATE_SETTLING; //indicated page is settling to it's final position
            }

            private void handleSetNextItem() {
                final int lastPosition = mViewPager.getAdapter().getCount() - 1;
                if (mCurrentPosition == 0) {
                    mViewPager.setCurrentItem(lastPosition,false);
                    wheelView.setSelected(lastPosition);
                } else if (mCurrentPosition == lastPosition) {
                    mViewPager.setCurrentItem(0, false);
                    wheelView.setSelected(0);
                }
            }
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    setNextItemIfNeeded();
                }
                mScrollState = state;
            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                mCurrentPosition = position;
                mPreviousPosition = position-1;

                wheelView.setSelected(position);

                /*if (position == kDesconecta) {
                    mViewPager.setCurrentItem(kAplicaciones, true);
                }

                // skip fake page (last), go to first page
                if (position == kAplicaciones) {
                    mViewPager.setCurrentItem(kDesconecta, false);
                }*/
                //wheelView.setSelected(position);
            }
        });







    }


    private void actualizarCabeceraUsuario() {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(ActivityTutorial.this);
        if (!app.isUsuarioConectado()) {
            txt_correo.setText("");
            txt_nombre.setText(getString(R.string.no_registrado));
            findViewById(R.id.ly_cerrarsesion).setVisibility(GONE);
        } else {
            txt_nombre.setText(app.getUsuario().getNombre());
            txt_correo.setText(app.getUsuario().getEmail());
        }

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);




        Button tv = (Button)header.findViewById(R.id.button5);
                if(app.isDeviceAdmin()) {
                    tv.setText(getString(R.string.eliminar_admin));
                }
                else{
                    tv.setText(getString(R.string.permiso_admin_title));
                }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        ActivityAppNoPermitida.fromService = false;
        activity = this;

        diapotoshow = 0;



        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        PhoneStateManager.getInstance().cargarDatosDeAplicacion(this);



        intent = getIntent();

            try {
                PackageManager packageManager = this.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.getPackageName(), 0);
                AppOpsManager appOpsManager = (AppOpsManager) this.getSystemService(Context.APP_OPS_SERVICE);
                int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            PhoneStateManager.getInstance().setCurrentActivity(this);
            PhoneStateManager.getInstance().setLy_main(
                    (ConstraintLayout) findViewById(R.id.ly_main));

            PhoneStateManager.getInstance().cargarAplicacionesInstaladas();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                if(getSharedPreferences("Preferencias",Context.MODE_PRIVATE).getInt("padre",0)==0) {
                    PhoneStateManager.getInstance().cargarListinTelefonicoThread(getContentResolver(), this);
                }
            }


            //cargarLayoutDatosPreferencias();


            final NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            drawer = findViewById(R.id.drawer_layout);


            if (app.getDesconexionActual() == null) {
                PhoneStateManager.getInstance().setDesconexion(new ConfiguracionDesconexion());
            } else {
                PhoneStateManager.getInstance().setDesconexion(app.getDesconexionActual());
            }


            findViewById(R.id.hamburguer).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PhoneStateManager.getInstance().isDesconectado())
                        drawer.openDrawer(Gravity.START);
                }
            });

            View header = navigationView.getHeaderView(0);

            header.findViewById(R.id.hamburguer2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(Gravity.START);
                }
            });

            header.findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(ActivityTutorial.this);
                    if (!app.isUsuarioConectado()) {
                        TresAndroides.changeActivity(ActivityTutorial.this,
                                ActivityInicio.class, true);
                    } else {
                    /*TresAndroides.changeActivity(ActivityPrincipal.this,
                            ActivityPerfilUsuario.class, false);*/
                        mViewPager.setCurrentItem(kEditarPerfil);
                        wheelView.setSelected(kEditarPerfil);
                        drawer.closeDrawer(Gravity.START);
                    }
                }
            });


        header.findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(app.isDeviceAdmin()) {

                    ComponentName mDeviceAdminSample;
                    mDeviceAdminSample = new ComponentName(ActivityTutorial.this, AdminReceiver.class);

                    DevicePolicyManager mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                    mDPM.removeActiveAdmin(mDeviceAdminSample);

                    app.setDeviceAdmin(false);
                    if(!app.isDeviceAdmin()) {
                        ((Button) v).setText(getString(R.string.permiso_admin_title));
                    }
                }
                else{
                    Intent intent = new Intent(ActivityTutorial.this, ActivityPermisos.class);


                    startActivity(intent);
                    finish();
                }






            }
        });


        txt_nombre = header.findViewById(R.id.txt_nombre);
            txt_correo = header.findViewById(R.id.txt_correo);

            actualizarCabeceraUsuario();

            findViewById(R.id.ly_cerrarsesion).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(ActivityTutorial.this);
                    app.cerrarSesion(activity);
                    app.guardarEnPreferencias(activity);

                    Intent i = new Intent(ActivityTutorial.this, ActivityInicio.class);
                    startActivity(i);
                    finish();
                }
            });


            //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);
            adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(), this);
            // Set up the ViewPager with the sections adapter.
            mViewPager = findViewById(R.id.container);

        fragmentsList = new ArrayList<>();
        fragmentsList.add(FragmentDesconecta.newInstance(mViewPager, drawer, intent));
        fragmentsList.add(FragmentDatosDeUso.newInstance());
        fragmentsList.add(FragmentProgramarDesconexion.newInstance());
        fragmentsList.add(FragmentGeolocalizacion.newInstance());
        fragmentsList.add(FragmentAjustes.newInstance(mViewPager));
        fragmentsList.add(FragmentEditarPerfil.newInstance());
        fragmentsList.add(FragmentNotificaciones.newInstance());
        fragmentsList.add(FragmentAplicaciones.newInstance());

            mViewPager.setAdapter(adapterViewPager);
            //mViewPager.setAdapter(mSectionsPagerAdapter);


            imgAPPs = this.findViewById(R.id.imgAPPs);
            imgDesconecta = this.findViewById(R.id.imgDesconecta);

        wheelView = this.findViewById(R.id.wheelview);
        wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectListener() {
            @Override
            public void onWheelItemSelected(WheelView parent, Drawable itemDrawable, int position) {
               // mViewPager.setCurrentItem(position);
            }
        });
        wheelView.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
            @Override
            public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
                parent.setSelected(position);
                mViewPager.setCurrentItem(position);
            }
        });



            mViewPager.setCurrentItem(kDesconecta);
            wheelView.setSelected(kDesconecta);


            servicioDatos = new ServicioDatos();
            servicioDatosIntent = new Intent(this, servicioDatos.getClass());

            if (!isMyServiceRunning(servicioDatos.getClass())) {
                DatosUsoManager.getInstance().loadFromPrefs(this);
                DatosUsoManager.getInstance().getAjustes().unlocked();
                DatosUsoManager.getInstance().saveInPrefs(this);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(servicioDatosIntent);

                } else {
                    startService(servicioDatosIntent);
                }
            }




            comprobarToken();

            if (!app.isUsuarioConectado()) {
                findViewById(R.id.lySinc).setVisibility(GONE);
            }


            RelativeLayout v = (RelativeLayout)findViewById(R.id.bgtutorial);
            v.setVisibility(View.VISIBLE);



        alertDialog = new Dialog(ActivityTutorial.this);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        showNewDiapo();




    }

    void showNewDiapo(){
        if(diapotoshow==1 || diapotoshow==4 || diapotoshow==5 || diapotoshow==6 || diapotoshow==7) {
            alertDialog.setContentView(R.layout.dialog_tutorialb);
        }
        else if(diapotoshow==0){
            alertDialog.setContentView(R.layout.dialog_tutorial);
        }
        else{
            alertDialog.setContentView(R.layout.dialog_tutorialc);
        }
        alertDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = alertDialog.getWindow();
        WindowManager.LayoutParams wlp = window.getAttributes();

      /*  View view;

        switch(diapotoshow){
            case 1:
                view = findViewById(R.id.)
                break;
        }
        int[] location = new int[2];
        view.getLocationOnScreen(location);

        wlp.x = 50;   //x position
        wlp.y = 200;   //y position

        wlp.gravity = Gravity.TOP | Gravity. LEFT;

        window.setAttributes(wlp);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);*/
        wlp.x = 0;   //x position
        wlp.y = 0;
        TextView tv = (TextView)alertDialog.findViewById(R.id.maintext);
        TextView tvv = (TextView)alertDialog.findViewById(R.id.titlename);
        TextView tvvv = (TextView)alertDialog.findViewById(R.id.titlepage);
        if(diapotoshow==0){

          wlp.x = (int)TypedValue.applyDimension(
                  TypedValue.COMPLEX_UNIT_DIP,
                  getResources().getDimension(R.dimen._7sdp),
                  getResources().getDisplayMetrics());   //x position
          wlp.y = (int)TypedValue.applyDimension(
                  TypedValue.COMPLEX_UNIT_DIP,
                  getResources().getDimension(R.dimen._16sdp),
                  getResources().getDisplayMetrics()); ;   //y position

          wlp.gravity = Gravity.TOP | Gravity. LEFT;

          tvv.setText("En el menú desplegable,");
          tvvv.setText("1/6");
          tv.setText("podrás gestionar los dispositivos vinculados, así como crear nuevas vinculaciones o cancelar las existentes y cerrar sesión.");

      }
      else if(diapotoshow==1){

            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);

            tvv.setText("El botón central");
            tvvv.setText("2/6");
            tv.setText("te permite crear una desconexión.");
        }
        else if(diapotoshow==2){

            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);

            tvv.setText("El tiempo que hay");
            tvvv.setText("2/6");
            tv.setText("Es el tiempo que quieres crear la desconexión.");
        }
        else if(diapotoshow==3){

            window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);

            wlp.y = (int)TypedValue.applyDimension(
                    TypedValue.COMPLEX_UNIT_DIP,
                    -100,getResources().getDisplayMetrics());   //y position
        //    wlp.gravity = Gravity.TOP | Gravity. LEFT;

            tvv.setText("Los botones de apps y teléfono");
            tvvv.setText("2/6");
            tv.setText("te permiten elegir si desbloquear las apps y las llamadas.");
        }
      else if(diapotoshow==4){
            // wheelView.setSelected(kAplicaciones);
          mViewPager.setCurrentItem(kAplicaciones,false);
          window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
          window.setGravity(Gravity.CENTER);

            tvv.setText("En Apps");
            tvvv.setText("3/6");
            tv.setText("elegirás qué aplicaciones permites durante el bloqueo que crees a tu hijo.");
      }
      else if(diapotoshow==5){
            // wheelView.setSelected(kDatosUso);
          mViewPager.setCurrentItem(kDatosUso,false);
          window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
          window.setGravity(Gravity.CENTER);

            tvv.setText("En Estadísticas");
            tvvv.setText("4/6");
            tv.setText("podrás ver cómo utiliza tu hijo su dispositivo.");
      }
      else if(diapotoshow==6){
            //wheelView.setSelected(kProgramarDesconexion);
          mViewPager.setCurrentItem(kProgramarDesconexion,false);
          window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
          window.setGravity(Gravity.CENTER);

            tvv.setText("En Programar desconexión");
            tvvv.setText("5/6");
            tv.setText("podrás crear horarios predeterminados, para no estar pendiente las 24 horas de gestionar el dispositivo de tu hijo.");
      }
      else if(diapotoshow==7){
         // wheelView.setSelected(kNotificaciones);
          mViewPager.setCurrentItem(kNotificaciones,false);
          window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
          window.setGravity(Gravity.CENTER);

          Button button= (Button) alertDialog.findViewById(R.id.buttonnext);
          button.setVisibility(View.INVISIBLE);

            tvv.setText("En Notificaciones");
            tvvv.setText("6/6");
            tv.setText("podrás ver las notificaciones que le llegan a tu hijo durante las desconexiones.");
      }
      else {
          window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
          window.setGravity(Gravity.CENTER);
      }
        window.setAttributes(wlp);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        Button button= (Button) alertDialog.findViewById(R.id.buttonskip);
        SpannableString content = new SpannableString(getString(R.string.saltar));
        content.setSpan(new UnderlineSpan(), 0, content.length(), 0);
        button.setText(content);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });




        button= (Button) alertDialog.findViewById(R.id.buttonnext);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                diapotoshow++;
                showNewDiapo();
            }
        });

        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        stopService(servicioDatosIntent);
        super.onDestroy();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*}*/



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PhoneStateManager.getInstance().guardarDatosEnAplicacion(this);
    }


    /**
     ***************************************************************************
     *
     *                    FUNCIONES VIEWPAGER
     *
     ***************************************************************************
     */


    public class MyPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 1;

        public MyPagerAdapter(FragmentManager fragmentManager, Context c) {
            super(fragmentManager);
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(c);
            NUM_ITEMS = kTotal;
            if (!app.isUsuarioConectado()){
               // NUM_ITEMS--;
            }
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case kNotificaciones:{
                    return fragmentsList.get(kNotificaciones);
                }
                case kAplicaciones:{
                    return fragmentsList.get(kAplicaciones);
                }
                case kDesconecta: { // Fragment # 0 - This will show FirstFragment
                    if (PhoneStateManager.getInstance().isDesconectado() &&
                            ViewPagerManager.getInstance().fragmentDesconecta != null){
                        mViewPager.setPagingEnabled(false);
                        ViewPagerManager.getInstance().fragmentDesconecta.setmViewPager(mViewPager);
                        return ViewPagerManager.getInstance().fragmentDesconecta;
                    }
                    ViewPagerManager.getInstance().fragmentDesconecta = (FragmentDesconecta)fragmentsList.get(kDesconecta);

                    return ViewPagerManager.getInstance().fragmentDesconecta;
                }
                case kAjustes:{
                    ViewPagerManager.getInstance().fragmentAjustes = (FragmentAjustes)fragmentsList.get(kAjustes);
                    return ViewPagerManager.getInstance().fragmentAjustes;
                }case kDatosUso:{
                    return fragmentsList.get(kDatosUso);
                }
                case kEditarPerfil:{
                    return fragmentsList.get(kEditarPerfil);
                }
                case kGeolocalizacion:{
                    return fragmentsList.get(kGeolocalizacion);
                }
                case kProgramarDesconexion:{
                    ViewPagerManager.getInstance().fragmentProgramarDesconexion = (FragmentProgramarDesconexion)fragmentsList.get(kProgramarDesconexion);
                    return ViewPagerManager.getInstance().fragmentProgramarDesconexion;
                }
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    private void comprobarToken(){
        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        String token = FirebaseInstanceId.getInstance().getToken();
        if (!app.getUsuario().getToken().equals(token)){
            final String oldToken = app.getUsuario().getToken();
            app.getUsuario().setToken(token);
            app.guardarEnPreferencias(this);

            if (TresAndroides.isOnline(this)){
                Thread tToken = new Thread() {
                    @Override
                    public void run()
                    {
                        JSONEnviarTokenPush jsonEnviarToken=new JSONEnviarTokenPush(ActivityTutorial.this);
                        try {
                            if (ActivityCompat.checkSelfPermission(ActivityTutorial.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                            final String iddisp = telephonyManager.getDeviceId();
                            jsonEnviarToken.run(app.getUsuario().getId(),app.getUsuario().getToken(), oldToken, iddisp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                };

                tToken.start();
            }

        }
    }

}


