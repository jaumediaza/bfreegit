package com.bfree.app;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.json.JSONChild;
import com.bfree.app.json.JSONVincularDispositivo;
import com.bfree.app.services.AdminReceiver;
import com.bfree.app.services.NotificationMonitor;
import com.bfree.app.services.ServicioAccesibilidad;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.List;


public class ActivityPadre extends AppCompatActivity {

    private Aplicacion app;

    private static Activity activity;
    private Context contexto;

    Button buttonLeft;
    Button buttonOk;
    Button buttonRight;

    ToggleButton toggle;

    TextView padremadre;
    TextView control;
    TextView txtSaludo;
    Handler handlerChild;
    int resChild;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_padre);
        activity = this;
        contexto = this;


        buttonLeft = findViewById(R.id.buttonLeft);
        buttonRight = findViewById(R.id.buttonRight);
        buttonOk = findViewById(R.id.buttonOk);

        toggle = (ToggleButton)findViewById(R.id.chkState);

        padremadre = (TextView)findViewById(R.id.txtMadrePadre);
        control = (TextView)findViewById(R.id.txtControla);
        txtSaludo = (TextView)findViewById(R.id.txtSaludo);



        app = new Aplicacion();

        app.cargarAplicacionDePreferencias(this);

        String saludo = getString(R.string.mensaje_bienvenida_1) + " ";
        if (app.getUsuario()!=null){
            saludo = saludo.concat(app.getUsuario().getNombre().toUpperCase());
        }else{
            app.setUsuarioSinRegistro(true);
            app.guardarEnPreferencias(this);
        }
        saludo = saludo.concat(" !");
        txtSaludo.setText(saludo);


        buttonLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle.setChecked(false);
                padremadre.setText(R.string.padremadre);
                control.setText(R.string.controla);
            }
        });


        buttonRight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle.setChecked(true);
                padremadre.setText(R.string.ninanino);
                control.setText(R.string.proteger);
            }
        });

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle.isChecked()) {
                    SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("padre", 0);
                    editor.commit();
                    Intent intent = new Intent(ActivityPadre.this, ActivityRegistroHijo.class);
                    startActivity(intent);
                    finish();
                }
                else{
                    SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("padre", 1);
                    editor.commit();
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);



                    if (ActivityCompat.checkSelfPermission(contexto, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        String[] permissions = {Manifest.permission.READ_PHONE_STATE};
                        requestPermissions(permissions, 66);
                    }
                    else {
                        goMain();
                    }
                }
            }
        });

        toggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (toggle.isChecked()) {
                    padremadre.setText(R.string.ninanino);
                    control.setText(R.string.proteger);
                }
                else{
                    padremadre.setText(R.string.padremadre);
                    control.setText(R.string.controla);
                }
            }
        });


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 66 && grantResults[0] >= 0){
            goMain();
        }



    }

    public void goMain(){
        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        final String device_name = TresAndroides.getDeviceName();
        if (ActivityCompat.checkSelfPermission(contexto, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
            final String device_id = telephonyManager.getDeviceId();


            final Handler handler = new Handler();

            new Thread() {
                @Override
                public void run() {
                    super.run();
                    JSONVincularDispositivo jsonVincularDispositivo = new JSONVincularDispositivo(ActivityPadre.this);
                    int res = -1;
                    try {
                        res = jsonVincularDispositivo.run(app.getUsuario().getId(), device_id, device_name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final int fRes = res;

                }
            }.start();

            setPadre(1,null,-1,-1);


        }
        //setpadre
    }

    void setPadre(final int isPadre, String nombre, int sexo, int edad){
        handlerChild = new Handler();

        new Thread() {
            @Override
            public void run() {
                super.run();
                JSONChild jsonChild = new JSONChild(ActivityPadre.this);
                resChild = -1;
                try {
                    resChild = jsonChild.run(app.getUsuario().getId(), isPadre,null,-1,-1);
                    handlerChild.post(new Runnable() {
                        public void run() {
                            if(resChild==1) {
                                app.getUsuario().setIsPadre(isPadre);
                                app.guardarEnPreferencias(contexto);
                                if (isPadre == 1) {
                                    Intent intent = new Intent(ActivityPadre.this, ActivityPrincipal.class);
                                    startActivity(intent);
                                    finish();
                                }
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }


    @Override
    protected void onPostResume() {
        super.onPostResume();


        app.cargarAplicacionDePreferencias(this);

    }


    @Override
    protected void onResume() {
        super.onResume();


    }
}
