package com.atendus.app;

import android.Manifest;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.AppOpsManager;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AppUsage;
import com.atendus.app.clases.ConfiguracionDesconexion;
import com.atendus.app.clases.DispositivosControlados;
import com.atendus.app.json.AESCrypt;
import com.atendus.app.json.JSONBorrarVinculacionDisposiivo;
import com.atendus.app.json.JSONComprobarVinculacionesDisponibles;
import com.atendus.app.json.JSONDispositivosControlados;
import com.atendus.app.json.JSONEnviarEstadisticas;
import com.atendus.app.json.JSONEnviarTokenPush;
import com.atendus.app.json.JSONVincularDispositivosUsuario;
import com.atendus.app.remoto.ActivityPrincipalRemoto;
import com.atendus.app.services.AdminReceiver;
import com.atendus.app.services.ServicioDatos;
import com.atendus.app.splash_inicio.ActivityInicio;
import com.atendus.app.utilidades.CustomViewPager;
import com.atendus.app.utilidades.DatosUsoManager;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;
import com.atendus.app.utilidades.ViewPagerManager;
import com.google.firebase.iid.FirebaseInstanceId;
import com.lukedeighton.wheelview.WheelView;
import com.lukedeighton.wheelview.adapter.WheelAdapter;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import spencerstudios.com.bungeelib.Bungee;

import static android.view.View.GONE;

public class ActivityPrincipal extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {


    //private static Aplicacion app=new Aplicacion();
    private static DrawerLayout drawer;
    private TextView txt_nombre;
    private TextView txt_correo;

    private boolean primeraVez = true;


    private static CustomViewPager mViewPager;
    private FragmentPagerAdapter adapterViewPager;
    List<Fragment> fragmentsList;

    private static Activity activity;

    WheelView wheelView;

    public static final int kTotal = 8; // Total de pantallas
    public static final int kNotificaciones = 6;
    public static final int kAplicaciones = 7;
    public static final int kDesconecta = 0;
    public static final int kGeolocalizacion = 3;
    public static final int kAjustes = 4;
    public static final int kDatosUso = 1;
    public static final int kProgramarDesconexion = 2;
    public static final int kEditarPerfil = 5;




    private ImageView imgDesconecta, imgAPPs;


    private ServicioDatos servicioDatos;
    private Intent servicioDatosIntent;

    private static Intent intent;



    @Override
    protected void onResume() {
        super.onResume();

        final Handler handler = new Handler();
        new Thread(){
            @Override
            public void run() {
                super.run();
                final Aplicacion appi = new Aplicacion();
                List<DispositivosControlados> dispo = new ArrayList<>();
                appi.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                JSONDispositivosControlados js = new JSONDispositivosControlados(ActivityPrincipal.this);
                try {
                    dispo = js.run(appi.getUsuario().getId());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                final List<DispositivosControlados>  dispositivosControlados =  dispo;
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        appi.setDispositivosControlados(dispositivosControlados);
                        appi.guardarEnPreferencias(ActivityPrincipal.this);
                        actualizarDispositivosNavDrawer(dispositivosControlados);
                    }
                });
            }
        }.start();

        final com.lukedeighton.wheelview.WheelView wheelView = (com.lukedeighton.wheelview.WheelView)findViewById(R.id.wheelview);
        wheelView.setAdapter(new WheelAdapter() {
            @Override
            public Drawable getDrawable(int position) {
                switch(position){
                    case 0:
                        return getResources().getDrawable(R.drawable.iconos_01);
                    case 1:
                        return getResources().getDrawable(R.drawable.iconos_07);
                    case 2:
                        return getResources().getDrawable(R.drawable.iconos_06);
                    case 3:
                        return getResources().getDrawable(R.drawable.iconos_08);
                    case 4:
                        return getResources().getDrawable(R.drawable.iconos_05);
                    case 5:
                        return getResources().getDrawable(R.drawable.iconos_04);
                    case 6:
                        return getResources().getDrawable(R.drawable.iconos_03);
                    case 7:
                        return getResources().getDrawable(R.drawable.iconos_02);
                    default:
                        return getResources().getDrawable(R.drawable.iconos_01);
                }

                //return drawable here - the position can be seen in the gifs above
            }

            @Override
            public int getCount() {
                return 8;
            }
        });

        TextView txtUso = findViewById(R.id.txtTiempoUso);
        TextView txtDesbloqueos = findViewById(R.id.txtDesbloqueos);
        long millis = DatosUsoManager.getInstance().getAjustes().getUsageTime();
        String uso = TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.MILLISECONDS.toHours(millis) * 60) + "m";
        txtUso.setText(uso);

        txtDesbloqueos.setText(String.valueOf(DatosUsoManager.getInstance().getAjustes().getDesbloqueos()));

        ViewPagerManager.getInstance().pager = mViewPager;
        ViewPagerManager.getInstance().drawer = drawer;


        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            private int mCurrentPosition;
            private int mScrollState;
            private int mPreviousPosition;
            private void setNextItemIfNeeded() {
                if (!isScrollStateSettling()) {
                    handleSetNextItem();
                }
            }
            private boolean isScrollStateSettling() {
                return mScrollState == ViewPager.SCROLL_STATE_SETTLING; //indicated page is settling to it's final position
            }

            private void handleSetNextItem() {
                final int lastPosition = mViewPager.getAdapter().getCount() - 1;
                if (mCurrentPosition == 0) {
                    mViewPager.setCurrentItem(lastPosition,false);
                    wheelView.setSelected(lastPosition);
                } else if (mCurrentPosition == lastPosition) {
                    mViewPager.setCurrentItem(0, false);
                    wheelView.setSelected(0);
                }


            }
            public void onPageScrollStateChanged(int state) {
                if (state == ViewPager.SCROLL_STATE_IDLE) {
                    setNextItemIfNeeded();
                }
                mScrollState = state;
            }
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

            public void onPageSelected(int position) {
                mCurrentPosition = position;
                mPreviousPosition = position-1;

                if(position == kGeolocalizacion) {
                    FragmentGeolocalizacion f = (FragmentGeolocalizacion) fragmentsList.get(kGeolocalizacion);
                    if (f != null) {
                        f.populateListing();
                    }
                }
                wheelView.setSelected(position);

                /*if (position == kDesconecta) {
                    mViewPager.setCurrentItem(kAplicaciones, true);
                }

                // skip fake page (last), go to first page
                if (position == kAplicaciones) {
                    mViewPager.setCurrentItem(kDesconecta, false);
                }*/
                //wheelView.setSelected(position);
            }
        });



        if (PhoneStateManager.getInstance().isFromProgramarDesconexion()) {
            if (mViewPager.getCurrentItem() != kDesconecta)
                mViewPager.setCurrentItem(kDesconecta);
                wheelView.setSelected(kDesconecta);

            if (ViewPagerManager.getInstance().fragmentDesconecta != null)
                ViewPagerManager.getInstance().fragmentDesconecta.setDrawer(drawer);
                drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (primeraVez) {
            primeraVez = false;
        }

        actualizarCabeceraUsuario();



    }


    private void actualizarCabeceraUsuario() {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
        if (!app.isUsuarioConectado()) {
            txt_correo.setText("");
            txt_nombre.setText(getString(R.string.no_registrado));
            findViewById(R.id.ly_cerrarsesion).setVisibility(GONE);
        } else {
            txt_nombre.setText(app.getUsuario().getNombre());
            txt_correo.setText(app.getUsuario().getEmail());
        }

        NavigationView navigationView = findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);




        Button tv = (Button)header.findViewById(R.id.button5);
                if(app.isDeviceAdmin()) {
                    tv.setText(getString(R.string.eliminar_admin));
                }
                else{
                    tv.setText(getString(R.string.permiso_admin_title));
                }

    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
        ActivityAppNoPermitida.fromService = false;
        activity = this;





        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        PhoneStateManager.getInstance().cargarDatosDeAplicacion(this);



        intent = getIntent();

            try {
                PackageManager packageManager = this.getPackageManager();
                ApplicationInfo applicationInfo = packageManager.getApplicationInfo(this.getPackageName(), 0);
                AppOpsManager appOpsManager = (AppOpsManager) this.getSystemService(Context.APP_OPS_SERVICE);
                int mode = appOpsManager.checkOpNoThrow(AppOpsManager.OPSTR_GET_USAGE_STATS, applicationInfo.uid, applicationInfo.packageName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            PhoneStateManager.getInstance().setCurrentActivity(this);
            PhoneStateManager.getInstance().setLy_main(
                    (ConstraintLayout) findViewById(R.id.ly_main));

            PhoneStateManager.getInstance().cargarAplicacionesInstaladas();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) {
                if(getSharedPreferences("Preferencias",Context.MODE_PRIVATE).getInt("padre",0)==0) {
                    PhoneStateManager.getInstance().cargarListinTelefonicoThread(getContentResolver(), this);
                }
            }


            //cargarLayoutDatosPreferencias();


            final NavigationView navigationView = findViewById(R.id.nav_view);
            navigationView.setNavigationItemSelectedListener(this);
            drawer = findViewById(R.id.drawer_layout);


            if (app.getDesconexionActual() == null) {
                PhoneStateManager.getInstance().setDesconexion(new ConfiguracionDesconexion());
            } else {
                PhoneStateManager.getInstance().setDesconexion(app.getDesconexionActual());
            }


            findViewById(R.id.hamburguer).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PhoneStateManager.getInstance().isDesconectado())
                        drawer.openDrawer(Gravity.START);
                }
            });

            View header = navigationView.getHeaderView(0);

            header.findViewById(R.id.hamburguer2).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    drawer.closeDrawer(Gravity.START);
                }
            });

            header.findViewById(R.id.ly_perfil).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                    if (!app.isUsuarioConectado()) {
                        TresAndroides.changeActivity(ActivityPrincipal.this,
                                ActivityInicio.class, true);
                    } else {
                    /*TresAndroides.changeActivity(ActivityPrincipal.this,
                            ActivityPerfilUsuario.class, false);*/
                        mViewPager.setCurrentItem(kEditarPerfil);
                        wheelView.setSelected(kEditarPerfil);
                        drawer.closeDrawer(Gravity.START);
                    }
                }
            });


        SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        if(!preferences.getBoolean("shouldOpenTutorial",true)) {
            SharedPreferences.Editor editor = preferences.edit();
            editor.putBoolean("shouldOpenTutorial", false);
            editor.commit();
            Intent intent = new Intent(ActivityPrincipal.this, ActivityTutorial.class);
            startActivity(intent);
        }

        if(app.getUsuario().getIsPadre()==0){
            header.findViewById(R.id.button5).setVisibility(View.VISIBLE);
            if(header.findViewById(R.id.lyAddDispo)!=null)
            header.findViewById(R.id.lyAddDispo).setVisibility(View.INVISIBLE);
            app.startCheckingForLocation(ActivityPrincipal.this);
        }
        else{
            header.findViewById(R.id.button5).setVisibility(View.INVISIBLE);
            if(header.findViewById(R.id.lyAddDispo)!=null)
            header.findViewById(R.id.lyAddDispo).setVisibility(View.VISIBLE);
        }
        header.findViewById(R.id.button5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(app.isDeviceAdmin()) {

                    ComponentName mDeviceAdminSample;
                    mDeviceAdminSample = new ComponentName(ActivityPrincipal.this, AdminReceiver.class);

                    DevicePolicyManager mDPM = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
                    mDPM.removeActiveAdmin(mDeviceAdminSample);

                    app.setDeviceAdmin(false);
                    if(!app.isDeviceAdmin()) {
                        ((Button) v).setText(getString(R.string.permiso_admin_title));
                    }
                }
                else{
                    Intent intent = new Intent(ActivityPrincipal.this, ActivityPermisos.class);


                    startActivity(intent);
                    finish();
                }






            }
        });


        txt_nombre = header.findViewById(R.id.txt_nombre);
            txt_correo = header.findViewById(R.id.txt_correo);

            actualizarCabeceraUsuario();

            findViewById(R.id.ly_cerrarsesion).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                    app.cerrarSesion(activity);
                    app.guardarEnPreferencias(activity);

                    Intent i = new Intent(ActivityPrincipal.this, ActivityInicio.class);
                    startActivity(i);
                    finish();
                }
            });


            //mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);
            adapterViewPager = new MyPagerAdapter(getSupportFragmentManager(), this);
            // Set up the ViewPager with the sections adapter.
            mViewPager = findViewById(R.id.container);

        fragmentsList = new ArrayList<>();
        fragmentsList.add(FragmentDesconecta.newInstance(mViewPager, drawer, intent));
        fragmentsList.add(FragmentDatosDeUso.newInstance());
        fragmentsList.add(FragmentProgramarDesconexion.newInstance());
        fragmentsList.add(FragmentGeolocalizacion.newInstance());
        fragmentsList.add(FragmentAjustes.newInstance(mViewPager));
        fragmentsList.add(FragmentEditarPerfil.newInstance());
        fragmentsList.add(FragmentNotificaciones.newInstance());
        fragmentsList.add(FragmentAplicaciones.newInstance());

            mViewPager.setAdapter(adapterViewPager);
            //mViewPager.setAdapter(mSectionsPagerAdapter);


            imgAPPs = this.findViewById(R.id.imgAPPs);
            imgDesconecta = this.findViewById(R.id.imgDesconecta);

        wheelView = this.findViewById(R.id.wheelview);
        wheelView.setOnWheelItemSelectedListener(new WheelView.OnWheelItemSelectListener() {
            @Override
            public void onWheelItemSelected(WheelView parent, Drawable itemDrawable, int position) {
               // mViewPager.setCurrentItem(position);
            }
        });
        wheelView.setOnWheelItemClickListener(new WheelView.OnWheelItemClickListener() {
            @Override
            public void onWheelItemClick(WheelView parent, int position, boolean isSelected) {
                parent.setSelected(position);
                mViewPager.setCurrentItem(position);
                if(position == kGeolocalizacion) {
                    FragmentGeolocalizacion f = (FragmentGeolocalizacion) fragmentsList.get(kGeolocalizacion);
                    if (f != null) {
                        f.populateListing();
                    }
                }
            }
        });



/*            imgAPPs.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PhoneStateManager.getInstance().isDesconectado()) {
                        mViewPager.setCurrentItem(kAplicaciones);
                    }

                }
            });

            findViewById(R.id.imgNotif).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!PhoneStateManager.getInstance().isDesconectado()) {
                        mViewPager.setCurrentItem(kNotificaciones);
                    }

                }
            });
            findViewById(R.id.imgEstadisticas).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!PhoneStateManager.getInstance().isDesconectado()) {
                        mViewPager.setCurrentItem(kDatosUso);
                    }
                }
            });

            imgDesconecta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!PhoneStateManager.getInstance().isDesconectado()) {
                        mViewPager.setCurrentItem(kDesconecta);
                    }
                }
            });

            findViewById(R.id.imgProgDesc).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!PhoneStateManager.getInstance().isDesconectado()) {
                        mViewPager.setCurrentItem(kProgramarDesconexion);
                    }
                }
            });

        findViewById(R.id.imgGeoloc).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!PhoneStateManager.getInstance().isDesconectado()) {
                    mViewPager.setCurrentItem(kGeolocalizacion);
                }
            }
        });
*/
            mViewPager.setCurrentItem(kDesconecta);
            wheelView.setSelected(kDesconecta);


            servicioDatos = new ServicioDatos();
            servicioDatosIntent = new Intent(this, servicioDatos.getClass());

            if (!isMyServiceRunning(servicioDatos.getClass())) {
                DatosUsoManager.getInstance().loadFromPrefs(this);
                DatosUsoManager.getInstance().getAjustes().unlocked();
                DatosUsoManager.getInstance().saveInPrefs(this);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    startForegroundService(servicioDatosIntent);

                } else {
                    startService(servicioDatosIntent);
                }
            }


            findViewById(R.id.button3).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(ActivityPrincipal.this, ActivityVinculacionesPendientes.class);
                    startActivity(intent);
                    Bungee.fade(ActivityPrincipal.this);
                }
            });

            findViewById(R.id.button4).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new Thread() {
                        @Override
                        public void run() {
                            if (ActivityCompat.checkSelfPermission(ActivityPrincipal.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                            final int idusu = app.getUsuario().getId();
                            final String iddisp = telephonyManager.getDeviceId();
                            //Diarias
                            enviarEstadisticasDiarias(idusu, iddisp);
                            //Semanales
                            enviarEstadisticasSemanales(idusu, iddisp);
                            //mensuales
                            enviarEstadisticasMensuales(idusu, iddisp);

                        }
                    }.start();


                }
            });

            comprobarToken();

            if (!app.isUsuarioConectado()) {
                findViewById(R.id.lySinc).setVisibility(GONE);
            }


            findViewById(R.id.lyAddDispo).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                    final Handler handler = new Handler();

                    new Thread(){
                        @Override
                        public void run() {
                            super.run();
                            JSONComprobarVinculacionesDisponibles jsonComprobarVinculacionesDisponibles
                                    = new JSONComprobarVinculacionesDisponibles(ActivityPrincipal.this);
                            boolean b = false;
                            try {
                                b = jsonComprobarVinculacionesDisponibles.run(app.getUsuario().getId());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            final boolean resultado = b;

                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    if (resultado){
                                        Intent i = new Intent(ActivityPrincipal.this, ActivityAddDispositivo.class);
                                        Bungee.fade(ActivityPrincipal.this);
                                        startActivity(i);
                                    }else {
                                        new AlertDialog.Builder(ActivityPrincipal.this)
                                                .setTitle("Has alcanzado el máximo de vinculaciones")
                                                .setMessage("Aumenta tu plan de Bfree o desvincula algún dispositivo")
                                                .setPositiveButton("Aceptar", null).show();
                                    }
                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                }
                            });
                        }
                    }.start();


                }
            });


        findViewById(R.id.premium_ly).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(app.getUsuario().getPlan()<2) {
                    Intent i = new Intent(ActivityPrincipal.this, ActivityPremium.class);
                    Bungee.fade(ActivityPrincipal.this);
                    startActivity(i);
                }
                else{
                    Toast toast =
                            Toast.makeText(getApplicationContext(),
                                    "¡Ya eres Premium!", Toast.LENGTH_SHORT);


                    toast.show();
                }


            }
        });



        //animacionMenu();

    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    @Override
    protected void onDestroy() {
        stopService(servicioDatosIntent);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent startMain = new Intent(Intent.ACTION_MAIN);
            startMain.addCategory(Intent.CATEGORY_HOME);
            startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(startMain);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_principal, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    /*}*/



    private void enviarEstadisticasDiarias(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getDailyStatistics(ActivityPrincipal.this);
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(AESCrypt.encrypt(URLEncoder.encode(aux, "UTF-8")) + "/");
                } else {
                    name = name.concat(AESCrypt.encrypt(URLEncoder.encode(apusu.getAppName(), "UTF-8")) + "/");
                }

                pckg = pckg.concat(AESCrypt.encrypt(apusu.getPackage_name()) + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(ActivityPrincipal.this);

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg,1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasSemanales(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getWeeklyStatistics(ActivityPrincipal.this);
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(ActivityPrincipal.this);

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasMensuales(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getMonthlyStatistics(ActivityPrincipal.this);
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(ActivityPrincipal.this);

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, 3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        PhoneStateManager.getInstance().guardarDatosEnAplicacion(this);
    }


    /**
     ***************************************************************************
     *
     *                    FUNCIONES VIEWPAGER
     *
     ***************************************************************************
     */


    public class MyPagerAdapter extends FragmentPagerAdapter {
        private int NUM_ITEMS = 1;

        public MyPagerAdapter(FragmentManager fragmentManager, Context c) {
            super(fragmentManager);
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(c);
            NUM_ITEMS = kTotal;
            if (!app.isUsuarioConectado()){
               // NUM_ITEMS--;
            }
        }

        // Returns total number of pages
        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        // Returns the fragment to display for that page
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case kNotificaciones:{
                    return fragmentsList.get(kNotificaciones);
                }
                case kAplicaciones:{
                    return fragmentsList.get(kAplicaciones);
                }
                case kDesconecta: { // Fragment # 0 - This will show FirstFragment
                    if (PhoneStateManager.getInstance().isDesconectado() &&
                            ViewPagerManager.getInstance().fragmentDesconecta != null){
                        mViewPager.setPagingEnabled(false);
                        ViewPagerManager.getInstance().fragmentDesconecta.setmViewPager(mViewPager);
                        return ViewPagerManager.getInstance().fragmentDesconecta;
                    }
                    ViewPagerManager.getInstance().fragmentDesconecta = (FragmentDesconecta)fragmentsList.get(kDesconecta);

                    return ViewPagerManager.getInstance().fragmentDesconecta;
                }
                case kAjustes:{
                    ViewPagerManager.getInstance().fragmentAjustes = (FragmentAjustes)fragmentsList.get(kAjustes);
                    return ViewPagerManager.getInstance().fragmentAjustes;
                }case kDatosUso:{
                    return fragmentsList.get(kDatosUso);
                }
                case kEditarPerfil:{
                    return fragmentsList.get(kEditarPerfil);
                }
                case kGeolocalizacion:{
                    return fragmentsList.get(kGeolocalizacion);
                }
                case kProgramarDesconexion:{
                    ViewPagerManager.getInstance().fragmentProgramarDesconexion = (FragmentProgramarDesconexion)fragmentsList.get(kProgramarDesconexion);
                    return ViewPagerManager.getInstance().fragmentProgramarDesconexion;
                }
                default:
                    return null;
            }
        }

        // Returns the page title for the top indicator
        @Override
        public CharSequence getPageTitle(int position) {
            return "Page " + position;
        }

    }

    private void comprobarToken(){
        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        String token = FirebaseInstanceId.getInstance().getToken();
        if (!app.getUsuario().getToken().equals(token)){
            final String oldToken = app.getUsuario().getToken();
            app.getUsuario().setToken(token);
            app.guardarEnPreferencias(this);

            if (TresAndroides.isOnline(this)){
                Thread tToken = new Thread() {
                    @Override
                    public void run()
                    {
                        JSONEnviarTokenPush jsonEnviarToken=new JSONEnviarTokenPush(ActivityPrincipal.this);
                        try {
                            if (ActivityCompat.checkSelfPermission(ActivityPrincipal.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                return;
                            }
                            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                            final String iddisp = telephonyManager.getDeviceId();
                            jsonEnviarToken.run(app.getUsuario().getId(),app.getUsuario().getToken(), oldToken, iddisp);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


                    }
                };

                tToken.start();
            }

        }
    }

    private void actualizarDispositivosNavDrawer(List<DispositivosControlados> dispositivosControlados){

        LayoutInflater inflater = LayoutInflater.from(this);

        LinearLayout ly = findViewById(R.id.lyDispositivos);
        ly.removeAllViews();
        if (dispositivosControlados!=null)
        for (int i = 0; i < dispositivosControlados.size(); ++i){
            View v = inflater.inflate(R.layout.dispositivos_controlados, null);

            TextView etiqueta = v.findViewById(R.id.etiqueta);
            TextView nombre = v.findViewById(R.id.usuario);

            if (i != 0)
                v.findViewById(R.id.lyTop).setVisibility(GONE);

            etiqueta.setText(dispositivosControlados.get(i).getEtiqueta());
            //nombre.setText(dispositivosControlados.get(i).getNombrehijo());

            final int idusu = dispositivosControlados.get(i).getId_usuario();
            final int iddisp = dispositivosControlados.get(i).getId_dispositivo();
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    drawer.closeDrawer(Gravity.START);
                    cargarBfreeRemoto(idusu, iddisp);


                }
            });

            final DispositivosControlados disp = dispositivosControlados.get(i);
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {

                    new AlertDialog.Builder(ActivityPrincipal.this).
                            setTitle("¿Deseas desvincular este dispositivo?")
                            .setPositiveButton("Devincular", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                                    findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                                    final Handler handler = new Handler();
                                    new Thread(){
                                        @Override
                                        public void run() {
                                            super.run();
                                            final Aplicacion app = new Aplicacion();
                                            app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                                            JSONBorrarVinculacionDisposiivo jsonBorrarVinculacionDisposiivo =
                                                    new JSONBorrarVinculacionDisposiivo(ActivityPrincipal.this);
                                            try {
                                                jsonBorrarVinculacionDisposiivo.run(disp, app.getUsuario().getId());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }

                                            List<DispositivosControlados> dispo = new ArrayList<>();
                                            app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                                            JSONDispositivosControlados js = new JSONDispositivosControlados(ActivityPrincipal.this);
                                            try {
                                                dispo = js.run(app.getUsuario().getId());
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            final List<DispositivosControlados>  dispositivosControlados =  dispo;

                                            handler.post(new Runnable() {
                                                @Override
                                                public void run() {
                                                    app.setDispositivosControlados(dispositivosControlados);
                                                    app.guardarEnPreferencias(ActivityPrincipal.this);
                                                    actualizarDispositivosNavDrawer(dispositivosControlados);
                                                    findViewById(R.id.pgBar).setVisibility(View.GONE);
                                                    findViewById(R.id.lyDis).setVisibility(View.GONE);
                                                }
                                            });
                                        }
                                    }.start();
                                }
                            }).setNegativeButton("Cancelar", null).show();


                    return true;
                }
            });

            ly.addView(v);
        }
        if (dispositivosControlados!=null) {
            if (dispositivosControlados.size() == 0) {
                findViewById(R.id.txtNoDispo).setVisibility(View.VISIBLE);
            }else{
                findViewById(R.id.txtNoDispo).setVisibility(View.GONE);
            }
        }else
            findViewById(R.id.txtNoDispo).setVisibility(View.VISIBLE);

        ly.invalidate();
    }

    private void cargarBfreeRemoto(final int idUsuario, final int idDisp){
        findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
        findViewById(R.id.lyDis).setVisibility(View.VISIBLE);

        final Handler handler = new Handler();

        new Thread(){
            @Override
            public void run() {
                super.run();
                final Aplicacion app = new Aplicacion();
                final List<DispositivosControlados> dispositivosControlados = new ArrayList<>();
                app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                JSONDispositivosControlados js = new JSONDispositivosControlados(ActivityPrincipal.this);
                try {
                    while (app.getUsuario() == null){
                        app.cargarAplicacionDePreferencias(ActivityPrincipal.this);
                    }
                    dispositivosControlados.addAll(js.run(app.getUsuario().getId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        app.setDispositivosControlados(dispositivosControlados);
                        app.guardarEnPreferencias(ActivityPrincipal.this);
                        actualizarDispositivosNavDrawer(dispositivosControlados);

                        int pos = -1;

                        for (int i = 0; i < dispositivosControlados.size(); ++i ){
                            if (dispositivosControlados.get(i).getId_usuario() == idUsuario
                                    && dispositivosControlados.get(i).getId_dispositivo() == idDisp)
                                pos = i;
                        }

                        if (pos == -1){
                            new AlertDialog.Builder(ActivityPrincipal.this).
                                    setTitle("¡Ha ocurrido un error")
                                    .setMessage("Ha ocurrido un error con el dispositivo, intentalo más tarde")
                                    .setPositiveButton("Aceptar", null).show();
                        }else{
                            Intent intent = new Intent(ActivityPrincipal.this, ActivityPrincipalRemoto.class);
                            final DispositivosControlados dispositivo = dispositivosControlados.get(pos);

                            if (dispositivo.getEstado() != 1){
                                new AlertDialog.Builder(ActivityPrincipal.this)
                                        .setTitle("Dispositivo no vinculado")
                                        .setMessage("El usuario ha rechazado o aun no ha aceptado la vinculación")
                                        .setPositiveButton("Aceptar", null).
                                        setNegativeButton("Volver a enviar", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialogInterface, int i) {
                                                findViewById(R.id.pgBar).setVisibility(View.VISIBLE);
                                                findViewById(R.id.lyDis).setVisibility(View.VISIBLE);
                                                final Handler handler = new Handler();
                                                new Thread(){
                                                    @Override
                                                    public void run() {
                                                        super.run();
                                                        JSONVincularDispositivosUsuario
                                                                jsonVincularDispositivosUsuario =
                                                                new JSONVincularDispositivosUsuario(ActivityPrincipal.this);
                                                        try {
                                                            jsonVincularDispositivosUsuario.run(
                                                                    app.getUsuario().getId(),
                                                                    dispositivo.getId_usuario(),
                                                                    String.valueOf(dispositivo.getId_dispositivo()));
                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }

                                                        handler.post(new Runnable() {
                                                            @Override
                                                            public void run() {
                                                                new AlertDialog.Builder(ActivityPrincipal.this)
                                                                        .setTitle("Vinculación enviada")
                                                                        .setPositiveButton("Aceptar", null)
                                                                        .show();
                                                                findViewById(R.id.pgBar).setVisibility(View.GONE);
                                                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                                                            }
                                                        });
                                                    }
                                                }.start();
                                            }
                                        }).show();
                                findViewById(R.id.pgBar).setVisibility(View.GONE);
                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                            }else {
                                intent.putExtra("accesoRemoto", true);
                                intent.putExtra("idUsuario", dispositivo.getId_usuario());
                                intent.putExtra("idDispositivo", dispositivo.getId_dispositivo());
                                intent.putExtra("horas", dispositivo.getUltima_desconexion_hh());
                                intent.putExtra("minutos", dispositivo.getUltima_desconexion_mm());
                                intent.putExtra("notificaciones", dispositivo.isUltima_desconexion_bloquear_apps());
                                intent.putExtra("llamadas", dispositivo.isUltima_desconexion_bloquear_llamadas());
                                intent.putExtra("listaTfnos", dispositivo.getUltima_desconexion_lista_contactos());
                                intent.putExtra("listaApps", dispositivo.getUltima_desconexion_lista_apps());
//                                intent.putExtra("nombre", dispositivo.getNombrehijo());
                                intent.putExtra("etiqueta", dispositivo.getEtiqueta());
                                intent.putExtra("estadoDesconexion", dispositivo.getEstadoDesconexion());

                                findViewById(R.id.pgBar).setVisibility(View.GONE);
                                findViewById(R.id.lyDis).setVisibility(View.GONE);
                                Bungee.fade(ActivityPrincipal.this);
                                startActivity(intent);
                                finish();
                            }

                        }
                    }
                });
            }
        }.start();
    }




    /*private int startTouch;
    //private int startObjx;
    //private int startObjy;

    private class ObjetoMenu{
        public ImageView imagen;
        //public int x, y;
        public float startX, startY;
        public float w,h;
    }

    @SuppressLint("ClickableViewAccessibility")
    private void animacionMenu(){
        ConstraintLayout barra = findViewById(R.id.ly_main);

        final ObjetoMenu imgNotif = new ObjetoMenu();
        final ObjetoMenu imgApps = new ObjetoMenu();
        final ObjetoMenu imgDescf = new ObjetoMenu();
        final ObjetoMenu imgEst = new ObjetoMenu();
        final ObjetoMenu imgProgDesc = new ObjetoMenu();


        imgNotif.imagen = findViewById(R.id.imgNotif);
        imgApps.imagen = findViewById(R.id.imgAPPs);
        imgDescf.imagen = findViewById(R.id.imgDesconecta);
        imgEst.imagen = findViewById(R.id.imgEstadisticas);
        imgProgDesc.imagen = findViewById(R.id.imgProgDesc);


        imgDescf.imagen.post(new Runnable() {
            @Override
            public void run() {
                imgNotif.startX = imgNotif.imagen.getX();
                imgApps.startX = imgApps.imagen.getX();
                imgDescf.startX = imgDescf.imagen.getX();
                imgEst.startX = imgEst.imagen.getX();
                imgProgDesc.startX = imgProgDesc.imagen.getX();


                imgNotif.startY = imgNotif.imagen.getY();
                imgApps.startY = imgApps.imagen.getY();
                imgDescf.startY = imgDescf.imagen.getY();
                imgEst.startY = imgEst.imagen.getY();
                imgProgDesc.startY = imgProgDesc.imagen.getY();

                imgDescf.w = imgDescf.imagen.getWidth();
                imgDescf.h = imgDescf.imagen.getHeight();
            }
        });

        barra.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int x = (int) motionEvent.getX();
                int y = (int) motionEvent.getY();

                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        Log.i("TAGMenu", "touched down");
                        startTouch = x;

                        break;
                    case MotionEvent.ACTION_MOVE:
                        Log.i("TAGMenu", "moving: (" + x +")");
                        int dx = x - startTouch;

                        float xf = imgDescf.startX + dx;

                        /*if (imgDescf.startX == 0)
                            return true;
                        int yf = (xf * imgDescf.startY)/imgDescf.startX;

                        imgDescf.imagen.setX(xf);
                        imgDescf.imagen.setY(yf);

                        aplicarRotacion(imgDescf.startY, xf+imgDescf.w/2,   imgDescf.imagen , imgDescf.h, imgDescf.w);
                        break;
                    case MotionEvent.ACTION_UP:
                        Log.i("TAGMenu", "touched up");
                        break;
                }

                return true;

        }});

    }

    //Z=arcocoseno(((xf-xo)/yi))
    // yf = yo+yi*sen(z)

    // new_y = ((y_origin - y) * cos(angle)) - ((x - x_origin) * sin(angle)) + y_origin;
    private void aplicarRotacion( float yi, float xf, ImageView obj, float h, float w){
        LinearLayout lyCentro = findViewById(R.id.lyCentro);
        double yf;
        float xo = lyCentro.getX() - w/2;
        float yo = -lyCentro.getY();



        double z = Math.acos(((xf-xo)/yi)/ 2.7);



        yf = yo +yi*Math.sin(-z) - (yo-yi) + h + h/2;

        //Log.d("valoresBFREE", "pi ( --, " + yi +")");
        //Log.d("valoresBFREE", "po ( "+xo+", " + yo +")");
        //Log.d("valoresBFREE", "pf ( "+xf+", " + ((float)yf) +")");

        Log.d("valoresBFREE",
                "\nZ = " + z
                + "\nxf = " + xf
                + "\nyf = " + yf);
                obj.setX(xf);

        obj.setY((float) yf);
        obj.invalidate();
    }*/
}


                                                              