package com.bfree.app;

import android.Manifest;
import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.app.usage.UsageStatsManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telecom.TelecomManager;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.json.JSONVincularDispositivo;
import com.bfree.app.login.ActivityPassword;
import com.bfree.app.services.AdminReceiver;
import com.bfree.app.services.NotificationMonitor;
import com.bfree.app.services.ServicioAccesibilidad;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.List;



public class ActivityPermisos extends AppCompatActivity {

    public static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    public static boolean permisosConcedidos = false;

    public static boolean permiso_llamadas;
    public static boolean permiso_default;
    public static boolean permiso_contactos;
    public static boolean permiso_admin;
    public static boolean permiso_datouso;
    public static boolean permiso_notif;
    public static boolean permiso_accesibilidad;
    public static boolean permiso_geoloc;

    private static final int PERMISSION_REQUEST_READ_PHONE_STATE = 666;
    private static final int PERMISSION_ACCESS_FINE_LOCATION = 777;

    private Aplicacion app;

    private Button btn_permiso_llamadas;
    private Button btn_default_app;
    private Button btn_permiso_contactos;
    private Button btn_permiso_admin;
    private Button btn_permiso_datosuso;
    private Button btn_continuar;
    private Button btn_accesibilidad;
    private Button btn_permiso_notif;
    private Button btn_geoloc;

    private boolean primeravez = true;

    private static final String TAG = "SevenNLS";

    private static final String TAG_PRE = "[" + ActivityPrincipal.class.getSimpleName() + "] ";

    private static final int EVENT_SHOW_CREATE_NOS = 0;

    private static final int EVENT_LIST_CURRENT_NOS = 1;

    private static final String ENABLED_NOTIFICATION_LISTENERS = "enabled_notification_listeners";

    private static final String ACTION_NOTIFICATION_LISTENER_SETTINGS = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";

    private boolean isEnabledNLS = false;//Indica si esta o no activado el NotificationListener

    private static Activity activity;
    private Context contexto;


    private void pedirPermisosAdministrador() {
        ComponentName mDeviceAdminSample;
        mDeviceAdminSample = new ComponentName(ActivityPermisos.this, AdminReceiver.class);
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                mDeviceAdminSample);

        startActivity(intent);
    }

    //Función que controla si esta o no activado el notification listener
    //Función que controla si esta o no activado el notification listener

    private boolean isActivadoNotificationListener() {

        String pkgName = getPackageName();

        final String flat = Settings.Secure.getString(getContentResolver(),

                ENABLED_NOTIFICATION_LISTENERS);

        if (!TextUtils.isEmpty(flat)) {

            final String[] names = flat.split(":");

            for (int i = 0; i < names.length; i++) {

                final ComponentName cn = ComponentName.unflattenFromString(names[i]);

                if (cn != null) {

                    if (TextUtils.equals(pkgName, cn.getPackageName())) {

                        app.setPermisoNotificacionesAceptado(true);

                        app.guardarEnPreferencias(activity);

                        return true;

                    }

                }

            }

        }

        app.setPermisoNotificacionesAceptado(false);

        app.guardarEnPreferencias(activity);

        return false;

    }


    private boolean isPermisoDatosUsoAceptado() {
        UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();
        List stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);

        return stats != null && !stats.isEmpty();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(requestCode==111){
            if(resultCode==RESULT_OK){
                permiso_default = true;
                activarBoton(btn_default_app);
            }else if(resultCode==RESULT_CANCELED){
                permiso_default = false;
                desactivarBoton(btn_default_app);
            }else{
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permisos);
        activity = this;
        contexto = this;



        permiso_llamadas = false;
        permiso_default = false;
        permiso_contactos = false;
        permiso_admin = false;
        permiso_datouso = false;
        permiso_notif = false;
        permiso_accesibilidad = false;
        permiso_geoloc = false;

        btn_permiso_llamadas = findViewById(R.id.btn_permiso_llamadas);
        btn_default_app = findViewById(R.id.btn_default_app);
        btn_permiso_contactos = findViewById(R.id.btn_permiso_contactos);
        btn_permiso_datosuso = findViewById(R.id.btn_permiso_datosuso);
        btn_permiso_admin = findViewById(R.id.btn_permiso_admin);
        btn_continuar = findViewById(R.id.btn_continuar);
        btn_permiso_notif = findViewById(R.id.btn_permiso_notif);
        btn_accesibilidad = findViewById(R.id.btn_accesibilidad);
        btn_geoloc = findViewById(R.id.btn_permiso_geoloc);

        desactivarBoton(btn_permiso_datosuso);
        desactivarBoton(btn_permiso_notif);


        app = new Aplicacion();

        comprobarPermisos();
        btn_permiso_llamadas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pedimos los permisos necesarios para las llamadas
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
                        String[] permissions = {Manifest.permission.READ_PHONE_STATE, Manifest.permission.CALL_PHONE};
                        requestPermissions(permissions, PERMISSION_REQUEST_READ_PHONE_STATE);
                    } else {
                        activarBoton(btn_permiso_llamadas);
                        permiso_llamadas = true;


                    }
                }
            }
        });

        btn_geoloc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pedimos los permisos necesarios para las llamadas
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED) {
                        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION};
                        requestPermissions(permissions, PERMISSION_ACCESS_FINE_LOCATION);
                    } else {
                        activarBoton(btn_geoloc);
                        permiso_geoloc = true;


                    }
                }
            }
        });

        btn_accesibilidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pedimos los permisos necesarios para las llamadas
                if (permiso_accesibilidad == false) {
                    startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                } else {

                    activarBoton(btn_accesibilidad);
                    permiso_accesibilidad = true;

                }
            }
        });


        btn_default_app.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pedimos los permisos necesarios para las llamadas
                TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

                boolean isAlreadyDefaultDialer = getPackageName()
                        .equals(telecomManager.getDefaultDialerPackage());

                if (!isAlreadyDefaultDialer){
                    Intent ChangeDialer = new Intent(TelecomManager.ACTION_CHANGE_DEFAULT_DIALER);
                    ChangeDialer.putExtra(TelecomManager.EXTRA_CHANGE_DEFAULT_DIALER_PACKAGE_NAME, getPackageName());
                    if (ChangeDialer.resolveActivity(getPackageManager()) != null) {
                        startActivityForResult(ChangeDialer, 111);
                    } else {
                        Log.w(getLocalClassName(), "No Intent available to handle action");
                    }

                }
                else{
                    activarBoton(btn_default_app);
                }
            }
        });

        btn_permiso_contactos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Pedimos los permisos necesarios para los contactos

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{Manifest.permission.READ_CONTACTS}, PERMISSIONS_REQUEST_READ_CONTACTS);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                }
            }
        });


        btn_permiso_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!app.isDeviceAdmin()) {
                    pedirPermisosAdministrador();
                }
            }
        });

        btn_permiso_datosuso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Comprobamos el permiso de acceso a datos de uso
                if (Build.VERSION.SDK_INT >= 21 && !app.isPermisoAccesoDatosUsoAceptado()) {
                    UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
                    long time = System.currentTimeMillis();
                    List stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);

                    if (isPermisoDatosUsoAceptado() == false) {

                        /*new AlertDialog.Builder(ActivityPermisos.this).setMessage(getResources().getText(R.string.mensaje_permiso_uso))
                                .setTitle("BFree necesita permisos para acceder a los datos de uso")
                                .setCancelable(false)
                                .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                                        //startActivityForResult(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS),ACCION_PERMISO_NOTIFICACIONES);
                                    }}).show();*/
                        startActivity(new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS));
                    } else {
                        app.setPermisoAccesoDatosUsoAceptado(true);
                        app.guardarEnPreferencias(ActivityPermisos.this);
                        activarBoton(btn_permiso_datosuso);
                        permiso_datouso = true;

                    }
                }
            }
        });

        btn_permiso_notif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*new android.app.AlertDialog.Builder(ActivityPermisos.this)
                        .setTitle("BFree necesita permisos para acceder a tus notificaciones")
                        .setMessage(getResources().getText(R.string.mensaje_permiso_notificaciones))
                        .setIconAttribute(android.R.attr.alertDialogIcon)
                        .setCancelable(false)
                        .setPositiveButton("Aceptar",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
                                    }
                                })
                        .create().show();*/

                startActivity(new Intent(ACTION_NOTIFICATION_LISTENER_SETTINGS));
            }
        });

        /*btn_continuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (permiso_llamadas && permiso_contactos && permiso_admin && permiso_datouso){

                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    final String device_name = TresAndroides.getDeviceName();
                    if (ActivityCompat.checkSelfPermission(ActivityPermisos.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        Intent i = new Intent(ActivityPermisos.this, ActivityBienvenida.class);
                        i.putExtra("Registro", 1);
                        startActivity(i);
                        Bungee.fade(activity);
                        finish();
                        return;
                    }


                    Intent intent = new Intent(ActivityPermisos.this, ActivityPrincipal.class);
                    startActivity(intent);
                    finish();
                }else{
                    new AlertDialog.Builder(ActivityPermisos.this)
                            .setTitle("¡Faltan permisos!")
                            .setMessage("Bfree necesita que aceptes todos los permisos para su correcto funcionamiento")
                            .setPositiveButton("Aceptar", null)
                            .show();
                }
            }
        });*/


    }

    @Override
    protected void onPostResume() {
        super.onPostResume();


        app.cargarAplicacionDePreferencias(this);
        if (app.isDeviceAdmin()) {
            activarBoton(btn_permiso_admin);
            permiso_admin = true;

        }
        else{
            permiso_admin = false;
        }

        if (Build.VERSION.SDK_INT >= 21 && !app.isPermisoAccesoDatosUsoAceptado()) {
            UsageStatsManager mUsageStatsManager = (UsageStatsManager) getSystemService(Context.USAGE_STATS_SERVICE);
            long time = System.currentTimeMillis();
            List stats = mUsageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000 * 10, time);

            if (!(stats == null || stats.isEmpty())) {
                app.setPermisoAccesoDatosUsoAceptado(true);
                app.guardarEnPreferencias(ActivityPermisos.this);
                activarBoton(btn_permiso_datosuso);
                permiso_datouso = true;

                TresAndroides.getDailyStatistics(this);
            }

        } else {
            app.setPermisoAccesoDatosUsoAceptado(true);
            app.guardarEnPreferencias(ActivityPermisos.this);
            activarBoton(btn_permiso_datosuso);
            permiso_datouso = true;
        }

        ComponentName cn = new ComponentName(this, NotificationMonitor.class);
        String flat = Settings.Secure.getString(this.getContentResolver(), "enabled_notification_listeners");
        final boolean enabled = flat != null && flat.contains(cn.flattenToString());
        if (enabled) {
            activarBoton(btn_permiso_notif);
            permiso_notif = true;
        }


        if (permiso_llamadas && permiso_geoloc && permiso_default && permiso_contactos && permiso_admin && permiso_datouso && permiso_notif && permiso_accesibilidad && primeravez) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            final String device_name = TresAndroides.getDeviceName();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            final String device_id = telephonyManager.getDeviceId();



            final Handler handler = new Handler();

            new Thread(){
                @Override
                public void run() {
                    super.run();
                    JSONVincularDispositivo jsonVincularDispositivo = new JSONVincularDispositivo(ActivityPermisos.this);
                    int res = -1;
                    try {
                        res = jsonVincularDispositivo.run(app.getUsuario().getId(), device_id, device_name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final int fRes = res;



                }
            }.start();
            Intent intent = new Intent(ActivityPermisos.this, ActivityPrincipal.class);
            startActivity(intent);
            finish();
        }

        if (primeravez)
            primeravez = false;
    }

    private boolean comprobarPermisos() {
        boolean resultado=true;
        app.cargarAplicacionDePreferencias(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_DENIED || checkSelfPermission(Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED)) {
                activarBoton(btn_permiso_llamadas);
                permiso_llamadas = true;
            }
            else
            {
                desactivarBoton(btn_permiso_llamadas);
                resultado=false;
            }
        }else{
            activarBoton(btn_permiso_llamadas);
            permiso_llamadas = true;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!(checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_DENIED)) {
                activarBoton(btn_geoloc);
                permiso_geoloc = true;
            }
            else
            {
                desactivarBoton(btn_geoloc);
                resultado=false;
            }
        }else{
            activarBoton(btn_geoloc);
            permiso_geoloc = true;
        }

        TelecomManager telecomManager = (TelecomManager) getSystemService(TELECOM_SERVICE);

        boolean isAlreadyDefaultDialer = getPackageName()
                .equals(telecomManager.getDefaultDialerPackage());

        if (!isAlreadyDefaultDialer){
            desactivarBoton(btn_default_app);

        }
        else{
            activarBoton(btn_default_app);
            permiso_default = true;
        }

        boolean isAlreadyAccesibilidad = isAccessibilityServiceEnabled(contexto, ServicioAccesibilidad.class);

        if (!isAlreadyAccesibilidad){
            desactivarBoton(btn_accesibilidad);

        }
        else{
            activarBoton(btn_accesibilidad);
            permiso_accesibilidad = true;
        }


        if (!(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)) {
            activarBoton(btn_permiso_contactos);
            permiso_contactos = true;
        }
        else
        {
            desactivarBoton(btn_permiso_contactos);
            resultado=false;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (isPermisoDatosUsoAceptado()==true) {
                activarBoton(btn_permiso_datosuso);
                permiso_datouso = true;
            }
            else
            {
                desactivarBoton(btn_permiso_datosuso);
                permiso_datouso=false;
                resultado=false;
            }
        }else{
            activarBoton(btn_permiso_datosuso);
            permiso_datouso = true;
        }


        if (app.isDeviceAdmin()){
            activarBoton(btn_permiso_admin);
            permiso_admin = true;
        }
        else
        {
            desactivarBoton(btn_permiso_admin);
            permiso_admin = false;
            resultado=false;
        }


        if(isActivadoNotificationListener())
        {
            resultado=false;
        }

        return resultado;
    }

    private void activarBoton(Button btn){
        btn.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_checked), null, null, null);
    }

    private void desactivarBoton(Button btn){
        btn.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_cancel), null, null, null);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_REQUEST_READ_PHONE_STATE && grantResults[0] >= 0 && grantResults[1] >= 0){
            activarBoton(btn_permiso_llamadas);
            permiso_llamadas = true;

        }

        if (requestCode == PERMISSION_ACCESS_FINE_LOCATION && grantResults[0] >= 0){
            activarBoton(btn_geoloc);
            permiso_geoloc = true;

        }


        if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS && grantResults[0]>= 0){
            activarBoton(btn_permiso_contactos);
            permiso_contactos = true;
        }
    }

    public static boolean isAccessibilityServiceEnabled(Context context, Class<?> accessibilityService) {
        ComponentName expectedComponentName = new ComponentName(context, accessibilityService);

        String enabledServicesSetting = Settings.Secure.getString(context.getContentResolver(),  Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
        if (enabledServicesSetting == null)
            return false;

        TextUtils.SimpleStringSplitter colonSplitter = new TextUtils.SimpleStringSplitter(':');
        colonSplitter.setString(enabledServicesSetting);

        while (colonSplitter.hasNext()) {
            String componentNameString = colonSplitter.next();
            ComponentName enabledService = ComponentName.unflattenFromString(componentNameString);

            if (enabledService != null && enabledService.equals(expectedComponentName))
                return true;
        }

        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        comprobarPermisos();
        if(isActivadoNotificationListener()==true && permiso_default && permiso_accesibilidad && permiso_geoloc==true && permiso_datouso==true && permiso_admin==true && permiso_contactos==true && permiso_llamadas==true)
        {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            final String device_name = TresAndroides.getDeviceName();
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            final String device_id = telephonyManager.getDeviceId();



            final Handler handler = new Handler();

            new Thread(){
                @Override
                public void run() {
                    super.run();
                    JSONVincularDispositivo jsonVincularDispositivo = new JSONVincularDispositivo(ActivityPermisos.this);
                    int res = -1;
                    try {
                        res = jsonVincularDispositivo.run(app.getUsuario().getId(), device_id, device_name);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    final int fRes = res;

                }
            }.start();
            Intent intent = new Intent(ActivityPermisos.this, ActivityPrincipal.class);
            startActivity(intent);
            finish();
        }
    }
}
