package com.atendus.app;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.ImageSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AppUsage;
import com.atendus.app.clases.EstadisticaAplicacion;
import com.atendus.app.remoto.ActivityPrincipalRemoto;
import com.atendus.app.utilidades.TresAndroides;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.MPPointF;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;



public class FragmentDatosDeUso extends Fragment  implements SeekBar.OnSeekBarChangeListener,
        OnChartValueSelectedListener {

    private final int kTOTALDATOS = 3;

    private View view;
    private Context context;

    private PieChart mChart;
    private BarChart mChart2;
    private String[] mParties ;
    int values[];
    private Drawable drawables[];
    int total;
    private int colors[] = new int[kTOTALDATOS+1];

    List<AppUsage> appUsages;


    private long mTotal;

    private LinearLayout lyEstadisticasLista;
    private LinearLayout lyAct;

    private boolean swGrafico = false;

    private Button btnDia;
    private Button btnSemana;
    private Button btnMes;

    private int periodoEstadisticas = 1;

    private float fValues[];

    private boolean firsttime = true;
    //private ImageView imgActualizar;

    private static boolean ususarioRemoto = false;

    public FragmentDatosDeUso(){}

    public static FragmentDatosDeUso newInstance() {
        FragmentDatosDeUso fragmentFirst = new FragmentDatosDeUso();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kDatosUso);
        args.putString("someTitle", "");
        ususarioRemoto = false;
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }
    public static FragmentDatosDeUso newInstance(boolean u) {
        FragmentDatosDeUso fragmentFirst = new FragmentDatosDeUso();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kDatosUso);
        args.putString("someTitle", "");
        ususarioRemoto = u;
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }


    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_estadisticas, container, false);

        if (ususarioRemoto){
            initRemoto();
        }
        else {
            init();
        }


        return view;
    }

    public void init(){

        lyAct = view.findViewById(R.id.lyAct);
        final Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        appUsages = app.getAppUsages();

        mChart =  view.findViewById(R.id.chart1);
        mChart2 =  view.findViewById(R.id.chart2);
        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);


        swGrafico = app.isChartTypeEstadisticas();

        if (appUsages.size() > 0)
            setListaApps();

        if (swGrafico) {
            //actualizarDatosTarta();
            fab.setImageResource(R.drawable.pie_chart);
        }
        else {
            //actualizarDatosLineal();
            fab.setImageResource(R.drawable.analytics);
        }



        /*imgActualizar = view.findViewById(R.id.imgActualizar);
        imgActualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lyAct.setVisibility(View.VISIBLE);
                imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        if (periodoEstadisticas == 1)
                            appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                        else if (periodoEstadisticas == 2)
                            appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                        else if(periodoEstadisticas == 3)
                            appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);


                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                if (swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();
                                lyAct.setVisibility(View.GONE);
                                imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();
            }
        });

        */


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Aplicacion app = new Aplicacion();
                //app.cargarAplicacionDePreferencias(context);
                if (swGrafico) {
                    actualizarDatosLineal();
                    swGrafico = false;
                    fab.setImageResource(R.drawable.pie_chart);
                } else {
                    actualizarDatosTarta();
                    swGrafico = true;
                    fab.setImageResource(R.drawable.analytics);
                }
                app.setChartTypeEstadisticas(swGrafico);
                app.guardarEnPreferencias(context);
            }
        });


        btnDia = view.findViewById(R.id.btnDia);
        btnSemana = view.findViewById(R.id.btnSemana);
        btnMes = view.findViewById(R.id.btnMes);

        btnDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 1;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                /*if (swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();*/

                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();

                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();

            }
        });
        btnSemana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 2;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();
                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();

            }
        });
        btnMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 3;

                lyAct.setVisibility(View.VISIBLE);
                //imgActualizar.setVisibility(View.GONE);
                final Handler handler2 = new Handler();
                final Handler handler3 = new Handler();
                new Thread() {
                    @Override
                    public void run() {
                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);

                        handler2.post(new Runnable() {
                            @Override
                            public void run() {
                                if (appUsages.size() > 0)
                                    setListaApps();
                                new Thread(){
                                    @Override
                                    public void run() {
                                        super.run();
                                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);
                                        handler3.post(new Runnable() {
                                            @Override
                                            public void run() {
                                                if (appUsages.size() > 0)
                                                    setListaApps();
                                                if (swGrafico)
                                                    actualizarDatosTarta();
                                                else
                                                    actualizarDatosLineal();

                                                lyAct.setVisibility(View.GONE);
                                            }
                                        });
                                    }
                                }.start();
                                //imgActualizar.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                }.start();
            }
        });

        view.findViewById(R.id.imgDatos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long millis = 0L;
                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(context);
                int desbloqueos = 0;


                switch (periodoEstadisticas ){
                    case 1:
                        millis = app.getEstadisticaUsoDia().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoDia().getDesbloqueos();break;
                    case 2:
                        millis = app.getEstadisticaUsoSemana().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoSemana().getDesbloqueos();break;
                    case 3:millis = app.getEstadisticaUsoMes().getTiempoUso();
                        desbloqueos = app.getEstadisticaUsoMes().getDesbloqueos();break;
                }

                String str = "Uso del terminal: " + TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.MILLISECONDS.toHours(millis)*60) + "m" + "\nDesbloqueos: "
                        + desbloqueos;

                new SimpleTooltip.Builder(context)
                        .anchorView(view.findViewById(R.id.imgDatos))
                        .text(str)
                        .gravity(Gravity.BOTTOM)
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show();
            }
        });

        btnDia.setBackground(null);
        btnSemana.setBackground(null);
        btnMes.setBackground(null);
        btnDia.setTextColor(context.getColor(R.color.blanco));
        btnSemana.setTextColor(context.getColor(R.color.blanco));
        btnMes.setTextColor(context.getColor(R.color.blanco));
        btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
        btnDia.setTextColor(context.getColor(R.color.backgroundStart));

        periodoEstadisticas = 1;

        lyAct.setVisibility(View.VISIBLE);
        //imgActualizar.setVisibility(View.GONE);
        final Handler handler2 = new Handler();
        final Handler handler3 = new Handler();
        new Thread() {
            @Override
            public void run() {
                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                handler2.post(new Runnable() {
                    @Override
                    public void run() {
                        if (appUsages.size() > 0)
                            setListaApps();
                                /*if (swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();*/

                        new Thread(){
                            @Override
                            public void run() {
                                super.run();
                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                handler3.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        if (swGrafico)
                                            actualizarDatosTarta();
                                        else
                                            actualizarDatosLineal();

                                        lyAct.setVisibility(View.GONE);
                                    }
                                });
                            }
                        }.start();

                        //imgActualizar.setVisibility(View.VISIBLE);
                    }
                });
            }
        }.start();

        final SwipeRefreshLayout swipeRefreshLayout = view.findViewById(R.id.swRefresh);
        swipeRefreshLayout.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayout.setRefreshing(true);

                switch (periodoEstadisticas){
                    case 1:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 1;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                /*if (swGrafico)
                                    actualizarDatosTarta();
                                else
                                    actualizarDatosLineal();*/

                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();

                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                    case 2:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 2;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();
                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                    case 3:{
                        btnDia.setBackground(null);
                        btnSemana.setBackground(null);
                        btnMes.setBackground(null);
                        btnDia.setTextColor(context.getColor(R.color.blanco));
                        btnSemana.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setTextColor(context.getColor(R.color.blanco));
                        btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                        btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                        periodoEstadisticas = 3;

                        lyAct.setVisibility(View.VISIBLE);
                        //imgActualizar.setVisibility(View.GONE);
                        final Handler handler2 = new Handler();
                        final Handler handler3 = new Handler();
                        new Thread() {
                            @Override
                            public void run() {
                                appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);

                                handler2.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (appUsages.size() > 0)
                                            setListaApps();
                                        new Thread(){
                                            @Override
                                            public void run() {
                                                super.run();
                                                appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);
                                                handler3.post(new Runnable() {
                                                    @Override
                                                    public void run() {
                                                        if (appUsages.size() > 0)
                                                            setListaApps();
                                                        if (swGrafico)
                                                            actualizarDatosTarta();
                                                        else
                                                            actualizarDatosLineal();

                                                        lyAct.setVisibility(View.GONE);
                                                        swipeRefreshLayout.setRefreshing(false);
                                                    }
                                                });
                                            }
                                        }.start();
                                        //imgActualizar.setVisibility(View.VISIBLE);
                                    }
                                });
                            }
                        }.start();
                        break;
                    }

                }
            }
        });

    }
    public void initRemoto(){

        mChart =  view.findViewById(R.id.chart1);
        mChart2 =  view.findViewById(R.id.chart2);
        final FloatingActionButton fab = view.findViewById(R.id.floatingActionButton);

        while (ActivityPrincipalRemoto.estadisticasUsuarioRemoto == null){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        final List<EstadisticaAplicacion> estadisticaAplicacionDia =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasDia();
        final List<EstadisticaAplicacion> estadisticaAplicacionSemana =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasSemana();
        final List<EstadisticaAplicacion> estadisticaAplicacionMes =
                ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getEstadisticasMes();
        actualizarDatosTartaRemoto(estadisticaAplicacionDia);
        setListaAppsRemoto(estadisticaAplicacionDia);

        swGrafico = true;
        fab.setImageResource(R.drawable.analytics);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<EstadisticaAplicacion> estadisticas = estadisticaAplicacionDia;

                if (periodoEstadisticas == 2){
                    estadisticas = estadisticaAplicacionSemana;
                }else if (periodoEstadisticas == 3){
                    estadisticas = estadisticaAplicacionMes;
                }
                if (swGrafico) {
                    actualizarDatosLinealRemoto(estadisticas);
                    swGrafico = false;
                    fab.setImageResource(R.drawable.pie_chart);
                } else {
                    actualizarDatosTartaRemoto(estadisticas);
                    swGrafico = true;
                    fab.setImageResource(R.drawable.analytics);
                }
            }
        });

        btnDia = view.findViewById(R.id.btnDia);
        btnSemana = view.findViewById(R.id.btnSemana);
        btnMes = view.findViewById(R.id.btnMes);

        btnDia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnDia.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnDia.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 1;

                if (swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionDia);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionDia);
                }


            }
        });

        btnSemana.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnSemana.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 2;

                if (swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionSemana);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionSemana);
                }



            }
        });

        btnMes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnDia.setBackground(null);
                btnSemana.setBackground(null);
                btnMes.setBackground(null);
                btnDia.setTextColor(context.getColor(R.color.blanco));
                btnSemana.setTextColor(context.getColor(R.color.blanco));
                btnMes.setTextColor(context.getColor(R.color.blanco));
                btnMes.setBackground(context.getDrawable(R.drawable.button_estadisticas));
                btnMes.setTextColor(context.getColor(R.color.backgroundStart));

                periodoEstadisticas = 3;

                if (swGrafico) {
                    actualizarDatosTartaRemoto(estadisticaAplicacionMes);
                } else {
                    actualizarDatosLinealRemoto(estadisticaAplicacionMes);
                }


            }
        });

        view.findViewById(R.id.swRefresh).setEnabled(false);

        view.findViewById(R.id.imgDatos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long millis = 0L;
                Aplicacion app = new Aplicacion();
                app.cargarAplicacionDePreferencias(context);
                int desbloqueos = 0;


                switch (periodoEstadisticas){
                    case 1:
                        millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoDia();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosDia();break;
                    case 2:
                        millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoSemana();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosSemana();break;
                    case 3:millis = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getTiempoUsoMes();
                        desbloqueos = ActivityPrincipalRemoto.estadisticasUsuarioRemoto.getDesbloqueosMes();break;
                }

                String str = "Uso del terminal: " + TimeUnit.MILLISECONDS.toHours(millis) + "h " +
                        (TimeUnit.MILLISECONDS.toMinutes(millis) -
                                TimeUnit.MILLISECONDS.toHours(millis)*60) + "m" + "\nDesbloqueos: "
                        + desbloqueos;

                new SimpleTooltip.Builder(context)
                        .anchorView(view.findViewById(R.id.imgDatos))
                        .text(str)
                        .gravity(Gravity.BOTTOM)
                        .animated(true)
                        .transparentOverlay(false)
                        .build()
                        .show();
            }
        });
    }

    private void actualizarDatosTarta(){
        if (appUsages.size() <= 0)
            return;

        mChart.setVisibility(View.VISIBLE);
        mChart2.setVisibility(View.GONE);

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterText(generateCenterSpannableText("", null, -1));

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(context.getColor(R.color.backgroundEnd));

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(0f);
        mChart.setTransparentCircleRadius(0f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        mChart.getLegend().setEnabled(false);

        setDataPie(mParties.length, 100);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);
    }
    private void actualizarDatosTartaRemoto(List<EstadisticaAplicacion> estadisticas){
        if (estadisticas.size() <= 0)
            return;

        mChart.setVisibility(View.VISIBLE);
        mChart2.setVisibility(View.GONE);

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setExtraOffsets(5, 10, 5, 5);

        mChart.setDragDecelerationFrictionCoef(0.95f);

        mChart.setCenterText(generateCenterSpannableText("", null, -1));

        mChart.setDrawHoleEnabled(true);
        mChart.setHoleColor(context.getColor(R.color.backgroundEnd));

        mChart.setTransparentCircleColor(Color.WHITE);
        mChart.setTransparentCircleAlpha(110);

        mChart.setHoleRadius(0f);
        mChart.setTransparentCircleRadius(0f);

        mChart.setDrawCenterText(true);

        mChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        mChart.setRotationEnabled(false);
        mChart.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart.setOnChartValueSelectedListener(this);

        mChart.getLegend().setEnabled(false);

        setDataPieRemoto(estadisticas);

        mChart.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling
        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);
    }

    private void actualizarDatosLineal(){
        mChart.setVisibility(View.GONE);
        mChart2.setVisibility(View.VISIBLE);

        if (appUsages.size() <= 0)
            return;



        mChart2.getDescription().setEnabled(false);
        mChart2.setExtraOffsets(5, 10, 5, 5);

        mChart2.setDragDecelerationFrictionCoef(0.95f);





        // enable rotation of the chart by touch
        mChart2.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart2.setOnChartValueSelectedListener(this);

        mChart2.getLegend().setEnabled(false);

        setDataLinear(mParties.length, 100);

        mChart2.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling

    }
    private void actualizarDatosLinealRemoto(List<EstadisticaAplicacion> estadisticas){
        mChart.setVisibility(View.GONE);
        mChart2.setVisibility(View.VISIBLE);

        if (estadisticas.size() <= 0)
            return;



        mChart2.getDescription().setEnabled(false);
        mChart2.setExtraOffsets(5, 10, 5, 5);

        mChart2.setDragDecelerationFrictionCoef(0.95f);





        // enable rotation of the chart by touch
        mChart2.setHighlightPerTapEnabled(true);

        // mChart.setUnit(" €");
        // mChart.setDrawUnitsInChart(true);

        // add a selection listener
        mChart2.setOnChartValueSelectedListener(this);

        mChart2.getLegend().setEnabled(false);

        setDataLinearRemoto(estadisticas);

        mChart2.animateY(1400, Easing.EasingOption.EaseInOutQuad);
        // mChart.spin(2000, 0, 360);




        // entry label styling

    }


    private void setDataPie(int count, float range) {

        float mult = range;

        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        for (int i = 0; i < count ; i++) {
            PieEntry  entry =new PieEntry(fValues[i],
                    mParties[i],
                    drawables[i]);
            entries.add(entry);
        }
        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 0));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(appUsages.get(0).getColor());
        clr.add(appUsages.get(1).getColor());
        clr.add(appUsages.get(2).getColor());
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(colors[kTOTALDATOS]);

        dataSet.setColors(clr);
        //dataSet.setValueTextSize(20f);
        //dataSet.setSelectionShift(0f);


        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }
    private void setDataPieRemoto(List<EstadisticaAplicacion> estadisticas) {


        ArrayList<PieEntry> entries = new ArrayList<PieEntry>();



        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        for (int i = 0; i < 3 ; i++) {
            PieEntry  entry =new PieEntry((float) estadisticas.get(i).getPorcentajeUso(),
                    estadisticas.get(i).getNombreApp());
            entries.add(entry);
        }
        float total = 0;
        for(int i = 3; i < estadisticas.size(); ++i){
            total += estadisticas.get(i).getPorcentajeUso();
        }
        PieEntry entry = new PieEntry(total, "Otras");
        entries.add(entry);
        PieDataSet dataSet = new PieDataSet(entries, "Election Results");

        dataSet.setDrawIcons(false);

        dataSet.setSliceSpace(3f);
        dataSet.setIconsOffset(new MPPointF(0, 0));
        dataSet.setSelectionShift(5f);

        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(estadisticas.get(0).getColor());
        clr.add(estadisticas.get(1).getColor());
        clr.add(estadisticas.get(2).getColor());
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(Color.parseColor("#ffc87c"));

        dataSet.setColors(clr);
        //dataSet.setValueTextSize(20f);
        //dataSet.setSelectionShift(0f);


        PieData data = new PieData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart.setData(data);

        // undo all highlights
        mChart.highlightValues(null);

        mChart.invalidate();
    }

    private void setDataLinear(int count, float range) {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();


        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.


        for (int i = 0; i < count; ++i){
            char ellipsis = 133;
            String lbl = mParties[i];
            if (lbl.length() > 9) {
                lbl = lbl.substring(0, 8);
                lbl = lbl.concat("...");
            }
            mParties[i] = lbl;

        }


        for (int i = 0; i < count ; i++) {
            entries.add(new BarEntry(i, fValues[i],
                    mParties[i]));
        }

        BarDataSet dataSet = new BarDataSet(entries, "Hola");

        //dataSet.setDrawIcons(false);

        dataSet.setIconsOffset(new MPPointF(0, 40));



        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(appUsages.get(0).getColor());
        clr.add(appUsages.get(1).getColor());
        clr.add(appUsages.get(2).getColor());
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(colors[kTOTALDATOS]);

        dataSet.setColors(clr);
        dataSet.setStackLabels(mParties);
        //dataSet.setSelectionShift(0f);


        BarData data = new BarData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart2.setData(data);
        mChart2.setDrawValueAboveBar(true);





        mChart2.getXAxis().setValueFormatter(new IndexAxisValueFormatter(mParties));
        mChart2.getXAxis().setTextSize(11f);
        mChart2.getXAxis().setLabelCount(entries.size());
        mChart2.setDoubleTapToZoomEnabled(false);
        mChart2.setPinchZoom(false);
        mChart2.setScaleEnabled(false);



        // undo all highlights
        mChart2.highlightValues(null);

        mChart2.invalidate();
    }
    private void setDataLinearRemoto(List<EstadisticaAplicacion> estadisticas) {

        ArrayList<BarEntry> entries = new ArrayList<BarEntry>();


        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.

        String nombres[] = new String[4];
        float values[] = new float[4];
        for (int i = 0; i < 3; ++i){
            char ellipsis = 133;
            String lbl = estadisticas.get(i).getNombreApp();
            if (lbl.length() > 9) {
                lbl = lbl.substring(0, 8);
                lbl = lbl.concat("...");
            }
            nombres[i] = lbl;

            values[i] = (float)estadisticas.get(i).getPorcentajeUso();
        }
        nombres[3] = "Otras";
        values[3] = 0;
        for (int i = 3; i < estadisticas.size(); ++i){
            values[3]+= estadisticas.get(i).getPorcentajeUso();
        }



        for (int i = 0; i < 4 ; i++) {
            entries.add(new BarEntry(i, values[i],
                    nombres[i]));
        }

        BarDataSet dataSet = new BarDataSet(entries, "Hola");

        //dataSet.setDrawIcons(false);

        dataSet.setIconsOffset(new MPPointF(0, 40));



        // add a lot of colors

        ArrayList<Integer> clr = new ArrayList<Integer>();

        clr.add(estadisticas.get(0).getColor());
        clr.add(estadisticas.get(1).getColor());
        clr.add(estadisticas.get(2).getColor());
        //clr.add(appUsages.get(3).getColor());
        //clr.add(appUsages.get(4).getColor());
        clr.add(Color.parseColor("#ffc87c"));

        dataSet.setColors(clr);
        dataSet.setStackLabels(nombres);
        //dataSet.setSelectionShift(0f);


        BarData data = new BarData(dataSet);
        //data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(14f);
        data.setValueTextColor(Color.WHITE);
        data.setValueFormatter(new PercentFormatter());

        mChart2.setData(data);
        mChart2.setDrawValueAboveBar(true);





        mChart2.getXAxis().setValueFormatter(new IndexAxisValueFormatter(nombres));
        mChart2.getXAxis().setTextSize(11f);
        mChart2.getXAxis().setLabelCount(entries.size());
        mChart2.setDoubleTapToZoomEnabled(false);
        mChart2.setPinchZoom(false);
        mChart2.setScaleEnabled(false);



        // undo all highlights
        mChart2.highlightValues(null);

        mChart2.invalidate();
    }




    private SpannableString generateCenterSpannableText(String app, Drawable d, float p) {
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);

        String strapp = app;
        float auxp = p;
        if (p == -14) {
            view.findViewById(R.id.scroll).scrollTo(0, (int) lyEstadisticasLista.getY());
        }else if (p != -1)
            strapp = strapp.concat("\n" + df.format(auxp) + "%");
        SpannableString ss = new SpannableString(strapp + "\n\n ");
        ss.setSpan(new RelativeSizeSpan(1.3f), 0, strapp.length(), 0);
        ss.setSpan(new StyleSpan(Typeface.BOLD), 0, strapp.length(), 0);
        ss.setSpan(new ForegroundColorSpan(context.getColor(R.color.blanco)), 0, strapp.length(), 0);
        if (p!= -1 && p!= -14) {
            d.setBounds(0, 0, 200, 200);
            ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BASELINE);
            ss.setSpan(span, strapp.length()+2, strapp.length()+3, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
        }

        return ss;
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {

        /*if (e == null)
            return;
        for (int i = 0; i < values.length; ++i){
            if (values[i] == e.getY()){
                if (i == values.length-1){
                    mChart.setCenterText(generateCenterSpannableText("", drawables[i], -14));
                }else{
                    mChart.setCenterText(generateCenterSpannableText(mParties[i], drawables[i], (float)values[i]*100/total));
                }
            }
        }*/
    }

    @Override
    public void onNothingSelected() {

    }

    @Override
    public void onResume() {
        super.onResume();
        if (!ususarioRemoto)
            init();

        /*if (!firsttime){
            lyAct.setVisibility(View.VISIBLE);
            //imgActualizar.setVisibility(View.GONE);
            final Handler handler2 = new Handler();
            new Thread() {
                @Override
                public void run() {
                    if (periodoEstadisticas == 1)
                        appUsages  = TresAndroides.getDailyStatistics(context, drawables, colors);
                    else if (periodoEstadisticas == 2)
                        appUsages  = TresAndroides.getWeeklyStatistics(context, drawables, colors);
                    else if(periodoEstadisticas == 3)
                        appUsages  = TresAndroides.getMonthlyStatistics(context, drawables, colors);


                    handler2.post(new Runnable() {
                        @Override
                        public void run() {
                            if (appUsages.size() > 0)
                                setListaApps();
                            if (swGrafico)
                                actualizarDatosTarta();
                            else
                                actualizarDatosLineal();
                            lyAct.setVisibility(View.GONE);
                            //imgActualizar.setVisibility(View.VISIBLE);
                        }
                    });
                }
            }.start();
        }else
            firsttime = false;*/
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    private void setListaApps(){
        List<AppUsage> appusg = new ArrayList<>(appUsages);
        lyEstadisticasLista = view.findViewById(R.id.lyEstadisticasLista);
        LayoutInflater inflater = LayoutInflater.from(context);

        lyEstadisticasLista.removeAllViews();

        int i = 0;

        drawables = new Drawable[kTOTALDATOS+1];
        mParties = new String[kTOTALDATOS+1];
        colors = new int[kTOTALDATOS+1];
        values = new int[kTOTALDATOS+1];

        float fval[] =  new float[kTOTALDATOS+1];
        fval[kTOTALDATOS] = 0f;

        values[kTOTALDATOS] = 0;
        mParties[kTOTALDATOS] = "Otras";
        colors[kTOTALDATOS] = Color.parseColor("#ffc87c");
        mTotal = 0L;
        for (AppUsage app : appusg){
            mTotal += app.getTotalTimeInForeground();
        }
        for (AppUsage app : appusg){
            View v = inflater.inflate(R.layout.row_estadisticas, null);
            ImageView imgLogo = v.findViewById(R.id.imgLogo);
            TextView txtAppName = v.findViewById(R.id.txtAppName);
            TextView txtUsage = v.findViewById(R.id.txtUsage);


            String applicationName;
            applicationName = app.getAppName();

            txtAppName.setText(applicationName);
            float value = app.getTotalTimeInForeground()*100 / mTotal;
            String usage;

            if (value == 0){
                usage = " <1 %";
            }else{
                usage = String.valueOf(value) + " %";
            }
            txtUsage.setText(usage);
            try {
                Drawable icon = context.getPackageManager().getApplicationIcon(app.getPackage_name());
                imgLogo.setImageDrawable(icon);
                if (i < kTOTALDATOS){
                    drawables[i] = icon;
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (i > kTOTALDATOS-1) {
                lyEstadisticasLista.addView(v);
                values[kTOTALDATOS] += value;
                fval[kTOTALDATOS] += app.getTotalTimeInForeground();
            }else{
                values[i] = (int) value;
                mParties[i] = applicationName;
                fval[i] = (float)app.getTotalTimeInForeground()*100f / mTotal;
            }

            total = values[0] + values[1] + values[2] + values[3];// + values[4];

            i++;
        }

        fval[kTOTALDATOS] = fval[kTOTALDATOS]*100/mTotal;




        fValues = new float[kTOTALDATOS+1];
        System.arraycopy(fval, 0, fValues, 0, kTOTALDATOS + 1);
    }
    private void setListaAppsRemoto(List<EstadisticaAplicacion> estadisticas){
        lyEstadisticasLista = view.findViewById(R.id.lyEstadisticasLista);
        LayoutInflater inflater = LayoutInflater.from(context);

        lyEstadisticasLista.removeAllViews();

        for (int i = 3; i < estadisticas.size(); ++i){
            View v = inflater.inflate(R.layout.row_estadisticas, null);

            TextView txtAppName = v.findViewById(R.id.txtAppName);
            TextView txtUsage = v.findViewById(R.id.txtUsage);

            txtAppName.setText(estadisticas.get(i).getNombreApp());

            float value = (float) estadisticas.get(i).getPorcentajeUso();
            String usage;
            if (value < 1){
                usage = " <1 %";
            }else{
                usage = String.valueOf(value) + " %";
            }
            txtUsage.setText(usage);

            lyEstadisticasLista.addView(v);
        }

        lyEstadisticasLista.invalidate();
    }



}
