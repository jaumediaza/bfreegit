package com.atendus.app.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.utilidades.PhoneStateManager;



/**
 * Created by Alberto on 23/06/2018.
 */

public class AdapterAppsFragment extends BaseAdapter {
    private Context context;



    public AdapterAppsFragment(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return PhoneStateManager.getInstance().getAppsPermitidas().size();

    }

    @Override
    public AplicacionPermitida getItem(int position) {
        return PhoneStateManager.getInstance().getAppsPermitidas().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }



    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.celda_app_fragment, viewGroup, false);

        }

        ImageView imagen = view.findViewById(R.id.imagen);
        TextView label = view.findViewById(R.id.label);
        final ConstraintLayout lyMain = view.findViewById(R.id.ly_main);
        final ImageView imgCircle = view.findViewById(R.id.imageView14);

        final int pos = position;
        Drawable mDrawable = PhoneStateManager.getInstance().getAppsPermitidas().get(pos).getDrawable();
        label.setText(PhoneStateManager.getInstance().getAppsPermitidas().get(pos).getAppLabel());

        //ColorFilter filter = new LightingColorFilter( Color.WHITE, Color.WHITE);
        //mDrawable.setColorFilter(filter);

        imagen.setImageDrawable(mDrawable);

        Switch sw = view.findViewById(R.id.sw);
        sw.setChecked(PhoneStateManager.getInstance().getAppsPermitidas().get(pos).isUsoDuranteDesconecta());
        if (PhoneStateManager.getInstance().getAppsPermitidas().get(pos).isUsoDuranteDesconecta()){
            //lyMain.setBackgroundColor(Color.parseColor("#b47eb5d4"));
            imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle3));
        }else{
           // lyMain.setBackgroundColor(Color.parseColor("#00ffffff"));
            imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle));
        }
        lyMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PhoneStateManager.getInstance().getAppsPermitidas().get(pos).isUsoDuranteDesconecta()) {
                    PhoneStateManager.getInstance().getAppsPermitidas().get(pos).setUsoDuranteDesconecta(false);
                    //lyMain.setBackgroundColor(Color.parseColor("#00ffffff"));
                    imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle));
                }
                else {
                    PhoneStateManager.getInstance().getAppsPermitidas().get(pos).setUsoDuranteDesconecta(true);
                    //lyMain.setBackgroundColor(Color.parseColor("#b47eb5d4"));
                    imgCircle.setImageDrawable(context.getDrawable(R.drawable.circle3));

                }
            }
        });


        return view;
    }



}