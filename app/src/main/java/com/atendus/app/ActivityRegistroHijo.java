package com.bfree.app;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.media.Image;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.bfree.app.clases.Aplicacion;
import com.bfree.app.json.JSONChild;
import com.bfree.app.json.JSONVincularDispositivo;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.Calendar;

import static com.bfree.app.ActivityPermisos.permiso_accesibilidad;
import static com.bfree.app.ActivityPermisos.permiso_admin;
import static com.bfree.app.ActivityPermisos.permiso_contactos;
import static com.bfree.app.ActivityPermisos.permiso_datouso;
import static com.bfree.app.ActivityPermisos.permiso_default;
import static com.bfree.app.ActivityPermisos.permiso_geoloc;
import static com.bfree.app.ActivityPermisos.permiso_llamadas;
import static com.bfree.app.ActivityPermisos.permiso_notif;


public class ActivityRegistroHijo extends AppCompatActivity {

    private Aplicacion app;

    private static Activity activity;
    private Context contexto;


    private Button buttonAge;
    private EditText txtNombre;
    private ImageView manIv;
    private ImageView womanIv;
    int isSexSelected = -1;  //-1 not selected 0 woman 1 man
    static Dialog d ;
    int year = Calendar.getInstance().get(Calendar.YEAR);
    Handler handlerChild;
    int resChild;

    TextView txtSaludo;


    void setPadre(final int isPadre, final String nombre, final int sexo, final int edad){
        handlerChild = new Handler();

        new Thread() {
            @Override
            public void run() {
                super.run();
                JSONChild jsonChild = new JSONChild(ActivityRegistroHijo.this);
                resChild = -1;
                try {
                    resChild = jsonChild.run(app.getUsuario().getId(), isPadre,nombre,sexo,edad);
                    handlerChild.post(new Runnable() {
                        public void run() {
                            if(resChild==1) {
                                if (isPadre == 0) {
                                    app.getUsuario().setIsPadre(isPadre);
                                    app.getUsuario().setNombrehijo(nombre);
                                    app.getUsuario().setEdadhijo(edad);
                                    app.getUsuario().setSexo(sexo);

                                    if(permiso_llamadas && permiso_geoloc && permiso_contactos && permiso_default && permiso_accesibilidad && permiso_admin && permiso_datouso  && permiso_notif){

                                        Intent intent = new Intent(ActivityRegistroHijo.this, ActivityPrincipal.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                    else {
                                        Intent intent = new Intent(ActivityRegistroHijo.this, ActivityPermisos.class);
                                        startActivity(intent);
                                        finish();
                                    }
                                }
                            }
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reg_hijo);
        activity = this;
        contexto = this;


        ImageView buttonOk;
        ImageView buttonOkSmall;
        ImageView buttonBack = (ImageView)findViewById(R.id.arr_back);
        txtSaludo = (TextView)findViewById(R.id.txtSaludo);
        buttonOk = (ImageView)findViewById(R.id.done_big);
        buttonOkSmall = (ImageView)findViewById(R.id.done);
        buttonAge = (Button) findViewById(R.id.nacBut);
        txtNombre = (EditText)findViewById(R.id.edtxt_nombre);
        manIv = (ImageView)findViewById(R.id.mascBut);
        womanIv = (ImageView)findViewById(R.id.femBut);



        app = new Aplicacion();

        app.cargarAplicacionDePreferencias(this);

        String saludo = getString(R.string.mensaje_bienvenida_1) + " ";
        if (app.getUsuario()!=null){
            saludo = saludo.concat(app.getUsuario().getNombre().toUpperCase());
        }else{
            app.setUsuarioSinRegistro(true);
            app.guardarEnPreferencias(this);
        }
        saludo = saludo.concat(" !");
        txtSaludo.setText(saludo);

        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSexSelected!=-1 && !txtNombre.getText().toString().equals("")){
                    //set child
                    SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("selectedSex",isSexSelected);
                    editor.commit();
                    setPadre(0,txtNombre.getText().toString(),isSexSelected,Integer.parseInt(buttonAge.getText().toString()));

                }
            }
        });
        buttonOkSmall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isSexSelected!=-1 && !txtNombre.getText().toString().equals("")){
                    //set child
                    SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = preferences.edit();
                    editor.putInt("selectedSex",isSexSelected);
                    editor.commit();
                    setPadre(0,txtNombre.getText().toString(),isSexSelected,Integer.parseInt(buttonAge.getText().toString()));
                }
            }
        });
        buttonBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = preferences.edit();
                editor.putInt("padre",-1);
                editor.commit();
                Intent intent = new Intent(ActivityRegistroHijo.this, ActivityPadre.class);
                startActivity(intent);
                finish();
            }
        });

        buttonAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showYearDialog();
            }
        });

        manIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bottom = womanIv.getPaddingBottom();
                int top = womanIv.getPaddingTop();
                int right = womanIv.getPaddingRight();
                int left = womanIv.getPaddingLeft();
                womanIv.setBackground(getResources().getDrawable(R.drawable.button1));
                manIv.setBackground(getResources().getDrawable(R.drawable.button11));
                isSexSelected = 1;
                womanIv.setPadding(left, top, right, bottom);
                manIv.setPadding(left, top, right, bottom);
            }
        });

        womanIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int bottom = womanIv.getPaddingBottom();
                int top = womanIv.getPaddingTop();
                int right = womanIv.getPaddingRight();
                int left = womanIv.getPaddingLeft();
                womanIv.setBackground(getResources().getDrawable(R.drawable.button11));
                manIv.setBackground(getResources().getDrawable(R.drawable.button1));
                isSexSelected = 0;
                womanIv.setPadding(left, top, right, bottom);
                manIv.setPadding(left, top, right, bottom);
            }
        });
    }

    public void showYearDialog()
    {

        final Dialog d = new Dialog(ActivityRegistroHijo.this);
        d.setTitle("Year Picker");
        d.setContentView(R.layout.year_dialog);
        Button set = (Button) d.findViewById(R.id.button1);
        Button cancel = (Button) d.findViewById(R.id.button2);
        TextView year_text=(TextView)d.findViewById(R.id.year_text);
        year_text.setText(""+year);
        final NumberPicker nopicker = (NumberPicker) d.findViewById(R.id.numberPicker1);

        nopicker.setMaxValue(year+50);
        nopicker.setMinValue(year-50);
        nopicker.setWrapSelectorWheel(false);
        nopicker.setValue(year);
        nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

        set.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                buttonAge.setText(String.valueOf(nopicker.getValue()));
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();


    }


    @Override
    protected void onPostResume() {
        super.onPostResume();


        app.cargarAplicacionDePreferencias(this);

    }


    @Override
    protected void onResume() {
        super.onResume();


    }
}
