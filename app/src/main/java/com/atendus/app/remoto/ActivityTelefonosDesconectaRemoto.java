package com.atendus.app.remoto;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.adapters.AdapterTfnosPermitidosRecyclerRemoto;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;

import in.myinnos.alphabetsindexfastscrollrecycler.IndexFastScrollRecyclerView;
import spencerstudios.com.bungeelib.Bungee;


public class ActivityTelefonosDesconectaRemoto extends AppCompatActivity {

    //private GridView gridView;
    private AdapterTfnosPermitidosRecyclerRemoto adapter;
    private ProgressBar progressBar;
    private IndexFastScrollRecyclerView fast_recycler_tfnos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telefonos_desconecta);



        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        //gridView = findViewById(R.id.gridApps);

        fast_recycler_tfnos = findViewById(R.id.fast_recycler_tfnos);

        progressBar.setVisibility(View.GONE);
        if(getSharedPreferences("Preferencias",Context.MODE_PRIVATE).getInt("padre",0)==0) {
            PhoneStateManager.getInstance().cargarListinTelefonico(getContentResolver(), this);
        }

        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //adapter = new AdapterTfnosPermitidos(this);
        //gridView.setAdapter(adapter);
        adapter = new AdapterTfnosPermitidosRecyclerRemoto();
        fast_recycler_tfnos.setIndexBarColor("#AAAAAA");
        fast_recycler_tfnos.setIndexBarTransparentValue(0.35f);
        fast_recycler_tfnos.setLayoutManager(
                new LinearLayoutManager(this));
        fast_recycler_tfnos.setAdapter(adapter);


        final TextView txt_titulo = findViewById(R.id.txt_titulo);
        final EditText edtxt_search = findViewById(R.id.edtxt_search);
        ImageView search = findViewById(R.id.search);

        txt_titulo.setVisibility(View.VISIBLE);
        edtxt_search.setVisibility(View.GONE);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (edtxt_search.getVisibility() == View.GONE){
                    txt_titulo.setVisibility(View.INVISIBLE);
                    edtxt_search.setVisibility(View.VISIBLE);
                    edtxt_search.requestFocus();
                    TresAndroides.showKeyboard(ActivityTelefonosDesconectaRemoto.this);
                }else{
                    edtxt_search.requestFocus();
                    TresAndroides.showKeyboard(ActivityTelefonosDesconectaRemoto.this);
                }
            }
        });

        edtxt_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    String search = edtxt_search.getText().toString();
                    adapter.search(search);
                    return true;
                }
                return false;
            }
        });

        edtxt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String search = edtxt_search.getText().toString();
                adapter.search(search);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TresAndroides.hideKeyboard(this);
        Bungee.fade(this);
        finish();
    }

    @Override
    protected void onPause() {
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(this);
        app.setTfnosPermitidos(PhoneStateManager.getInstance().getTfnosPermitidos());
        app.guardarEnPreferencias(this);
        if(getSharedPreferences("Preferencias",Context.MODE_PRIVATE).getInt("padre",0)==0) {
            PhoneStateManager.getInstance().cargarListinTelefonico(this.getContentResolver(), this);
        }
        super.onPause();
    }

}
