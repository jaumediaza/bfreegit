package com.bfree.app;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.ScrollView;

import com.bfree.app.adapters.AdapterAppsFragment;
import com.bfree.app.adapters.AdapterAppsFragmentRemoto;
import com.bfree.app.clases.AplicacionPermitida;
import com.bfree.app.clases.DispositivosControlados;
import com.bfree.app.remoto.ActivityPrincipalRemoto;
import com.bfree.app.utilidades.PhoneStateManager;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 23/06/2018.
 */

public class FragmentAplicaciones extends Fragment{

    private View view;
    private Context context;

    private ScrollView scrollView;
    private Thread tDatosAplicaciones;
    private Handler handlerDatosAplicaciones;
    private List<ApplicationInfo> appInfo;
    private ArrayList<ApplicationInfo> preselectedApps;
    private List<ApplicationInfo> listaAplicacionesDispositivo=new ArrayList<>();
    private GridView gridView;
    private AdapterAppsFragment adaptador;
    private AdapterAppsFragmentRemoto adaptadorRemoto;
    private ProgressBar progressBar;

    private static final String TAG = "AllAppsActivity";

    private static boolean dispositivoRemoto = false;
    private static DispositivosControlados dispositivo;

    public FragmentAplicaciones() {}

    public static FragmentAplicaciones newInstance() {
        FragmentAplicaciones fragmentFirst = new FragmentAplicaciones();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kAplicaciones);
        args.putString("someTitle", "");
        dispositivoRemoto = false;
        fragmentFirst.setArguments(args);
        return fragmentFirst;
    }

    public static FragmentAplicaciones newInstance(DispositivosControlados d) {
        FragmentAplicaciones fragmentFirst = new FragmentAplicaciones();
        Bundle args = new Bundle();
        args.putInt("someInt", ActivityPrincipal.kAplicaciones);
        args.putString("someTitle", "");
        fragmentFirst.setArguments(args);
        dispositivo = d;
        dispositivoRemoto = true;
        return fragmentFirst;
    }

    // Store instance variables based on arguments passed
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity();


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_aplicaciones, container, false);

        if (!dispositivoRemoto)
            init();
        else
            initRemoto();
        return view;
    }



    private void init(){


        scrollView = view.findViewById(R.id.scrollView);

        gridView = view.findViewById(R.id.listaApps);

        progressBar = view.findViewById(R.id.progressBar);


        tDatosAplicaciones = new Thread() {
            @Override
            public void run()
            {
                //listaAplicacionesDispositivo=getAllInstalledApps();
                while (!PhoneStateManager.getInstance().isAppInfoListLoaded()){
                    try {
                        sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                handlerDatosAplicaciones.post(new Runnable() {
                    public void run() {

                    //adaptador = new APPsAdapter(context,listaAplicacionesDispositivo);

                        adaptador = new AdapterAppsFragment(context);
                        gridView.setAdapter(adaptador);
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        };

        handlerDatosAplicaciones = new Handler();
        tDatosAplicaciones.start();

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //adaptador.getItem(i).
            }
        });



    }
    private void initRemoto(){
        while (ActivityPrincipalRemoto.appsPermitidas == null){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        while (ActivityPrincipalRemoto.appsPermitidas.size() == 0){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        if (dispositivo.getUltima_desconexion_lista_apps() !=null) {
            String paquetes[] = dispositivo.getUltima_desconexion_lista_apps().split("/");

            for (String pckg : paquetes) {
                for (AplicacionPermitida app : ActivityPrincipalRemoto.appsPermitidas) {
                    if (app.getPackageName().equals(pckg)) {
                        app.setUsoDuranteDesconecta(true);
                        app.setPermitida(true);
                    }
                }
            }
        }


        scrollView = view.findViewById(R.id.scrollView);

        gridView = view.findViewById(R.id.listaApps);

        progressBar = view.findViewById(R.id.progressBar);


        adaptadorRemoto = new AdapterAppsFragmentRemoto(context);
        gridView.setAdapter(adaptadorRemoto);
        progressBar.setVisibility(View.GONE);

    }



}
