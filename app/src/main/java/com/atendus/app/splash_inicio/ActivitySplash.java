package com.bfree.app.splash_inicio;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.bfree.app.ActivityPadre;
import com.bfree.app.ActivityPermisos;
import com.bfree.app.ActivityPrincipal;
import com.bfree.app.ActivityRegistroHijo;
import com.bfree.app.R;
import com.bfree.app.clases.Aplicacion;
import com.bfree.app.login.ActivityBienvenida;
import com.bfree.app.login.ActivityPassword;
import com.bfree.app.utilidades.PhoneStateManager;
import com.bfree.app.utilidades.TresAndroides;
import com.bfree.app.utilidades.ViewPagerManager;

import spencerstudios.com.bungeelib.Bungee;

import static com.bfree.app.ActivityPermisos.permiso_accesibilidad;
import static com.bfree.app.ActivityPermisos.permiso_admin;
import static com.bfree.app.ActivityPermisos.permiso_contactos;
import static com.bfree.app.ActivityPermisos.permiso_datouso;
import static com.bfree.app.ActivityPermisos.permiso_default;
import static com.bfree.app.ActivityPermisos.permiso_geoloc;
import static com.bfree.app.ActivityPermisos.permiso_llamadas;
import static com.bfree.app.ActivityPermisos.permiso_notif;


public class ActivitySplash extends AppCompatActivity {

    private static final int DURACION_PANTALLA = 2000;
    private Aplicacion app=new Aplicacion();
    private Activity activity;
    private Context contexto;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        activity=this;
        contexto=this;




        Log.d("BFree", "ViewPagerManager.getInstance().isAppRunning()" + ViewPagerManager.getInstance().isAppRunning());
        if (ViewPagerManager.getInstance().isAppRunning()){


            SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
            int isPadre = -1;
            if(app.getUsuario()!=null){
                isPadre = app.getUsuario().getIsPadre();//preferences.getInt("padre",-1);
            }

            if ((permiso_llamadas && permiso_contactos && permiso_geoloc && permiso_default && permiso_accesibilidad && permiso_admin && permiso_datouso  && permiso_notif) || isPadre==1){
                Intent intent = new Intent(ActivitySplash.this, ActivityPrincipal.class);
                startActivity(intent);
                Bungee.fade(activity);
                finish();
            }
            else if((permiso_llamadas && permiso_geoloc && permiso_contactos && permiso_default && permiso_accesibilidad && permiso_admin && permiso_datouso  && permiso_notif) && isPadre==0 && !app.isUsuarioSinRegistro()){
                Intent intent = new Intent(ActivitySplash.this, ActivityPassword.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }else if(isPadre==0){
                Intent i;
                if(preferences.getInt("selectedSex",-1)!=-1) {
                    i = new Intent(ActivitySplash.this, ActivityPermisos.class);
                }
                else{
                    i = new Intent(ActivitySplash.this, ActivityPadre.class);
                }
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }
            else{
                Intent i = new Intent(ActivitySplash.this, ActivityPadre.class);
                startActivity(i);
                Bungee.fade(activity);
                finish();
            }

        }
        else
        {
            setContentView(R.layout.activity_splash);
            PhoneStateManager.getInstance().cargarDatosDeAplicacion(this);
            PhoneStateManager.getInstance().refrescarEstadisticasDeUso(this);
            app.cargarAplicacionDePreferencias(this);


            ViewPagerManager.getInstance().setAppRunning(true);
            new CountDownTimer(DURACION_PANTALLA, DURACION_PANTALLA) {
                public void onTick(long millisUntilFinished) {

                }

                public void onFinish() {
                    if(app.isUsuarioConectado() || app.isUsuarioSinRegistro())
                    {



                        SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
                        int isPadre = -1;
                        if(app.getUsuario()!=null){
                            isPadre = app.getUsuario().getIsPadre();//preferences.getInt("padre",-1);
                        }

                        if ((permiso_llamadas && permiso_geoloc && permiso_contactos && permiso_default && permiso_accesibilidad && permiso_admin && permiso_datouso  && permiso_notif) || isPadre==1){
                            Intent intent = new Intent(ActivitySplash.this, ActivityPrincipal.class);
                            startActivity(intent);
                            Bungee.fade(activity);
                            finish();
                        }
                        else if((permiso_llamadas && permiso_geoloc && permiso_geoloc && permiso_contactos && permiso_default && permiso_accesibilidad && permiso_admin && permiso_datouso  && permiso_notif) && isPadre==0 && !app.isUsuarioSinRegistro()){
                            Intent intent = new Intent(ActivitySplash.this, ActivityPassword.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if(isPadre==0){
                            Intent i;
                            if(preferences.getInt("selectedSex",-1)!=-1) {
                                i = new Intent(ActivitySplash.this, ActivityPermisos.class);
                            }
                            else{
                                i = new Intent(ActivitySplash.this, ActivityPadre.class);
                            }
                            startActivity(i);
                            Bungee.fade(activity);
                            finish();
                        }
                        else{

                            Intent i = new Intent(ActivitySplash.this, ActivityPadre.class);
                            startActivity(i);
                            Bungee.fade(activity);
                            finish();
                        }



                    }
                    else
                    {
                        Intent i = new Intent(ActivitySplash.this, ActivityInicio.class);
                        startActivity(i);
                        Bungee.fade(activity);




                        finish();
                    }

                }
            }.start();


        }

        }


}
