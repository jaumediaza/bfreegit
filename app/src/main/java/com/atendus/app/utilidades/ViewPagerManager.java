package com.atendus.app.utilidades;

import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;

import com.atendus.app.FragmentAjustes;
import com.atendus.app.FragmentDesconecta;
import com.atendus.app.FragmentProgramarDesconexion;


public class ViewPagerManager {

    //Patrón de funcionamiento de singletón
    private static final ViewPagerManager instance = new ViewPagerManager();

    private ViewPagerManager(){

    }

    public static ViewPagerManager getInstance(){
        return instance;
    }

    public FragmentDesconecta fragmentDesconecta;
    public FragmentAjustes fragmentAjustes;
    public FragmentProgramarDesconexion fragmentProgramarDesconexion;
    public ViewPager pager;
    public DrawerLayout drawer;
    private boolean appRunning = false;


    public boolean isAppRunning() {
        return appRunning;
    }

    public void setAppRunning(boolean appRunning) {
        this.appRunning = appRunning;
  