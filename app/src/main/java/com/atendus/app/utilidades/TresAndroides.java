package com.atendus.app.utilidades;

import android.app.Activity;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v7.graphics.Palette;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AppUsage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import spencerstudios.com.bungeelib.Bungee;



/**
 * Created by Pau on 26/7/17.
 */

public class TresAndroides {

    public static final char kA_MINUSCULA_ACENTUADA = '\u00e1'; // á
    public static final char kE_MINUSCULA_ACENTUADA = '\u00e9'; //é
    public static final char kI_MINUSCULA_ACENTUADA = '\u00ed'; //í
    public static final char kO_MINUSCULA_ACENTUADA = '\u00f3'; //ó
    public static final char kU_MINUSCULA_ACENTUADA = '\u00fa'; //ú
    public static final char kA_MAYUSCULA_ACENTUADA = '\u00c1'; //Á
    public static final char kE_MAYUSCULA_ACENTUADA= '\u00c9'; //É
    public static final char kI_MAYUSCULA_ACENTUADA = '\u00cd'; //Í
    public static final char kO_MAYUSCULA_ACENTUADA = '\u00d3'; //Ó
    public static final char kU_MAYUSCULA_ACENTUADA = '\u00da'; //Ú
    public static final char kENYE_MINUSCULA = '\u00f1'; //ñ
    public static final char kENYE_MAYUSCULA= '\u00d1'; //Ñ
    public static final char k_INTERROGACION_INICIO = '\u00bf'; // ¿


    public static final Pattern VALID_EMAIL_ADDRESS_REGEX =
            Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    public static boolean isPermisosAdministrador(Context contexto) {
        boolean resultado=false;
        final Intent deviceAdminIntent = new Intent("android.app.action.DEVICE_ADMIN_ENABLED", null);
        final List<ResolveInfo> pkgAppsList = contexto.getPackageManager().queryBroadcastReceivers(deviceAdminIntent, 0);
        for (ResolveInfo aResolveInfo : pkgAppsList) {
            String pkg = aResolveInfo.activityInfo.applicationInfo.packageName;
            String name = aResolveInfo.activityInfo.applicationInfo.loadLabel(contexto.getPackageManager()).toString();

            if(pkg.equals("com.bfree.app"))
            {
                return true;
            }

            System.out.println("Package :: " + pkg);
            System.out.println("Name :: " + name);

        }

        return false;
    }

    //Función para cambiar de actividad
    //curr_a Activity actual
    //next_a Activity a iniciar
    //finish cierra la activity actual
    public static void changeActivity(Activity curr_a, Class next_a, boolean finish){
        Intent mainIntent = new Intent().setClass(curr_a, next_a);
        curr_a.startActivity(mainIntent);
        Bungee.zoom(curr_a);
        if(finish)
            curr_a.finish();
    }
    //Función para cambiar de actividad sin animación
    //curr_a Activity actual
    //next_a Activity a iniciar
    //finish cierra la activity actual
    public static void changeActivityWithoutAnimation(Activity curr_a, Class next_a, boolean finish){
        Intent mainIntent = new Intent().setClass(curr_a, next_a);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        curr_a.startActivity(mainIntent);
        Bungee.zoom(curr_a);
        if(finish)
            curr_a.finish();

    }

    //Muestra el teclado de Android por pantalla
    public static void showKeyboard(Context c, View v){
        hideKeyboard(c, v);
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    //Oculta el teclado de Android
    public static void hideKeyboard(Context c, View v){
        if (v != null) {
            InputMethodManager imm = (InputMethodManager)c.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    //Muestra el teclado de Android por pantalla
    public static void showKeyboard(Activity a){
        hideKeyboard(a);
        InputMethodManager imm = (InputMethodManager) a.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    //Oculta el teclado de Android
    public static void hideKeyboard(Activity a){
        View v = a.getCurrentFocus();
        if (v != null) {
            InputMethodManager imm = (InputMethodManager)a.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
        }
    }

    public static int dpToPx(Activity a, int dp){
        DisplayMetrics displayMetrics = a.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int pxToDp(Activity a, int px){
        DisplayMetrics displayMetrics = a.getResources().getDisplayMetrics();
        return Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static boolean isOnline(Context c) {
        ConnectivityManager cm =
                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnectedOrConnecting();
    }

    public static boolean validateEmail(String emailStr) {
        Matcher matcher = VALID_EMAIL_ADDRESS_REGEX .matcher(emailStr);
        return matcher.find();
    }

    public static boolean validateImage(String img_path){
        String extension;
        int i  = 1;
        while (img_path.toCharArray()[img_path.length()-i] != 46){
            i++;
        }
        i--;
        i = img_path.length()-i;
        extension = img_path.substring(i);
        extension = extension.toUpperCase();
        return extension.equals("JPG") || extension.equals("JPEG") || extension.equals("GIF") ||
                extension.equals("PNG") || extension.equals("TIF") || extension.equals("TIFF") ||
                extension.equals("BMP");
    }

    //Función para rotar una imagen
    public static Bitmap rotate(Bitmap bitmap, int degree) {
        int w = bitmap.getWidth();
        int h = bitmap.getHeight();

        Matrix mtx = new Matrix();
        mtx.postRotate(degree);

        return Bitmap.createBitmap(bitmap, 0, 0, w, h, mtx, true);
    }

    //Función que devuelte la ruta real pasando un URI
    public static String getRealPathFromURI(Context context, Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    //Función para extraer una imáagen de un archivo
    public static Bitmap extraerImagen(File f, int tamanoMaximo)
    {
        Bitmap b = null;

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        BitmapFactory.decodeStream(fis, null, o);
        try {
            fis.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        int scale = 1;

        if (o.outHeight > tamanoMaximo || o.outWidth > tamanoMaximo)
        {
            scale = (int) Math.pow(2, (int) Math.ceil(Math.log(tamanoMaximo / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
        }
        //Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        try {
            fis = new FileInputStream(f);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        b = BitmapFactory.decodeStream(fis, null, o2);
        try {
            fis.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return b;
    }

    //Función para guardar una imagen pasando un Bitmap y devuelve la ruta
    public static String guardarImagen(Bitmap bitmap, String nombre, Bitmap.CompressFormat formato, int compresion, Context context) {
        File filesDir = context.getFilesDir();
        File imageFile = new File(filesDir, nombre);

        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(formato, compresion, os);
            os.flush();
            os.close();
        } catch (Exception e) {

        }

        return imageFile.getPath();
    }

    //Devuelve la extensión de un archivo
    public static String devolverExtension(File archivo)
    {
        String extension = archivo.getAbsolutePath().substring(archivo.getAbsolutePath().lastIndexOf("."));
        return extension;
    }

    //Devuelve el nombre de un archivo pasando una ruta completa
    public static String devolverNombreArchivo(String ruta)
    {
        String filename=ruta.substring(ruta.lastIndexOf("/")+1);
        return filename;
    }

    //Devuelve la versión de la aplicación
    public static String getAppVersion(Activity a) {
        PackageInfo pInfo = null;
        try {
            pInfo = a.getPackageManager().getPackageInfo(a.getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        String version = pInfo.versionName;
        return version;
    }

    //Devuelve el código de un vídeo de Youtube pasandole la URL
    public static String devolverCodigoVideoYoutube(String youtubeUrl)
    {
        String video_id="";
        if (youtubeUrl != null && youtubeUrl.trim().length() > 0 && youtubeUrl.startsWith("http"))
        {

            String expression = "^.*((youtu.be"+ "\\/)" + "|(v\\/)|(\\/u\\/w\\/)|(embed\\/)|(watch\\?))\\??v?=?([^#\\&\\?]*).*"; // var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
            CharSequence input = youtubeUrl;
            Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(input);
            if (matcher.matches())
            {
                String groupIndex1 = matcher.group(7);
                if(groupIndex1!=null && groupIndex1.length()==11)
                    video_id = groupIndex1;
            }
        }
        return video_id;
    }

    public static List<AppUsage> getDailyStatistics(Context context, Drawable drawables[], int colors[]) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();



        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, 0, time);



        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return appUsages;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return appUsages;
                }
            }catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        if (continuar)
                            if (drawables !=null) {
                                if (i < drawables.length && drawables[i] != null) {
                                    if (appUsages.get(i).getColor() == 0)
                                        colors[i] = setDominantColor(drawables[i]);
                                    else
                                        colors[i] = appUsages.get(i).getColor();
                                    if (colors[i] == -1){
                                        Random rnd = new Random();
                                        switch (i) {
                                            case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                            case 1: colors[i] = Color.parseColor("#1D678F");break;
                                            case 2: colors[i] = Color.parseColor("#36779A");break;
                                            case 3: colors[i] = Color.parseColor("#5595B7");break;
                                            case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                        }
                                        //colors[i] =  Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256));
                                    }
                                    if (i < appUsages.size())
                                        appUsages.get(i).setColor(colors[i]);
                                }
                            }else if (i < 5){
                                Random rnd = new Random();
                                switch (i) {
                                    case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                    case 1: colors[i] = Color.parseColor("#1D678F");break;
                                    case 2: colors[i] = Color.parseColor("#36779A");break;
                                    case 3: colors[i] = Color.parseColor("#5595B7");break;
                                    case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                }                              }

                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }

    public static List<AppUsage> getDailyStatistics(Context context) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();


        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, 0, time);
        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.DAY_OF_MONTH) == cal.get(Calendar.DAY_OF_MONTH) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return null;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return null;
                }
            }catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        int cont = 0;
                        for (AppUsage app : appUsages) {
                            try {
                                    Drawable icon = context.getPackageManager().getApplicationIcon(app.getPackage_name());

                                    if (icon != null){
                                        appUsages.get(cont).setColor(setDominantColor(icon));
                                    }else{
                                        Random rnd = new Random();
                                        appUsages.get(cont).setColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
                                    }
                                cont++;
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }

    public static List<AppUsage> getWeeklyStatistics(Context context, Drawable drawables[], int colors[]) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();


        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_WEEKLY, 0, time);
        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) == cal.get(Calendar.WEEK_OF_YEAR) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return appUsages;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return appUsages;
                }
            }catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        if (continuar)
                            if (drawables !=null) {
                                if (i < drawables.length && drawables[i] != null) {
                                    if (appUsages.get(i).getColor() == 0)
                                        colors[i] = setDominantColor(drawables[i]);
                                    else
                                        colors[i] = appUsages.get(i).getColor();
                                    if (colors[i] == -1){
                                        Random rnd = new Random();
                                        switch (i) {
                                            case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                            case 1: colors[i] = Color.parseColor("#1D678F");break;
                                            case 2: colors[i] = Color.parseColor("#36779A");break;
                                            case 3: colors[i] = Color.parseColor("#5595B7");break;
                                            case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                        }                                    }
                                    if (i < appUsages.size())
                                        appUsages.get(i).setColor(colors[i]);
                                }
                            }else if (i < 5){
                                Random rnd = new Random();
                                switch (i) {
                                    case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                    case 1: colors[i] = Color.parseColor("#1D678F");break;
                                    case 2: colors[i] = Color.parseColor("#36779A");break;
                                    case 3: colors[i] = Color.parseColor("#5595B7");break;
                                    case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                }                              }

                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }

    public static List<AppUsage> getWeeklyStatistics(Context context) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();


        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_WEEKLY, 0, time);
        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.WEEK_OF_YEAR) == cal.get(Calendar.WEEK_OF_YEAR) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return appUsages;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return appUsages;
                }
            }catch (PackageManager.NameNotFoundException e) {
               // e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        int cont = 0;
                        for (AppUsage app : appUsages) {
                            try {
                                Drawable icon = context.getPackageManager().getApplicationIcon(app.getPackage_name());
                                if (icon != null){
                                    appUsages.get(cont).setColor(setDominantColor(icon));
                                }else{
                                    Random rnd = new Random();
                                    appUsages.get(cont).setColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
                                }
                                cont++;
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }


    public static List<AppUsage> getMonthlyStatistics(Context context, Drawable drawables[], int colors[]) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();


        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_MONTHLY, 0, time);
        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.MONTH) == cal.get(Calendar.MONTH) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return appUsages;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return appUsages;
                }
            }catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        if (continuar)
                            if (drawables !=null) {
                                if (i < drawables.length && drawables[i] != null) {
                                    if (appUsages.get(i).getColor() == 0)
                                        colors[i] = setDominantColor(drawables[i]);
                                    else
                                        colors[i] = appUsages.get(i).getColor();
                                    if (colors[i] == -1){
                                        Random rnd = new Random();
                                        switch (i) {
                                            case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                            case 1: colors[i] = Color.parseColor("#1D678F");break;
                                            case 2: colors[i] = Color.parseColor("#36779A");break;
                                            case 3: colors[i] = Color.parseColor("#5595B7");break;
                                            case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                        }                                      }
                                    if (i < appUsages.size())
                                        appUsages.get(i).setColor(colors[i]);
                                }
                            }else if (i < 5){
                                Random rnd = new Random();
                                switch (i) {
                                    case 0: colors[i] = Color.parseColor("#0F4E70");break;
                                    case 1: colors[i] = Color.parseColor("#1D678F");break;
                                    case 2: colors[i] = Color.parseColor("#36779A");break;
                                    case 3: colors[i] = Color.parseColor("#5595B7");break;
                                    case 4: colors[i] = Color.parseColor("#7EB6D4");break;
                                }                              }

                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }

    public static List<AppUsage> getMonthlyStatistics(Context context) {
        //String topPackageName = null;
        List<AppUsage> appUsages;
        appUsages = new ArrayList<>();
        UsageStatsManager usage = (UsageStatsManager) context.getSystemService(Context.USAGE_STATS_SERVICE);
        long time = System.currentTimeMillis();


        List<UsageStats> stats = usage.queryUsageStats(UsageStatsManager.INTERVAL_MONTHLY, 0, time);
        if (stats != null) {
            //SortedMap<Long, UsageStats> runningTask = new TreeMap<Long,UsageStats>();
            for (UsageStats usageStats : stats) {
                //runningTask.put(usageStats.getLastTimeUsed(), usageStats);
                AppUsage appusage = new AppUsage();
                Calendar cal = Calendar.getInstance();
                cal.setTime(new Date(usageStats.getLastTimeUsed()));
                if (Calendar.getInstance().get(Calendar.MONTH) == cal.get(Calendar.MONTH) ) {
                    appusage.setPackage_name(usageStats.getPackageName());
                    appusage.setTotalTimeInForeground(usageStats.getTotalTimeInForeground());
                    boolean esta = false;
                    for (int i = 0; i < appUsages.size(); ++i){
                        if (appUsages.get(i).getPackage_name().equals(appusage.getPackage_name())){
                            appUsages.get(i).setTotalTimeInForeground(
                                    appUsages.get(i).getTotalTimeInForeground() +
                                            appusage.getTotalTimeInForeground());
                            esta = true;
                        }
                    }
                    if (!esta)
                        appUsages.add(appusage);
                }
            }
        }
        Collections.sort(appUsages);
        Collections.reverse(appUsages);


        final PackageManager pm = context.getPackageManager();

        for (int i = 0; i < appUsages.size(); ++i){
            if (i >= appUsages.size()){
                return appUsages;
            }
            try {
                ApplicationInfo app = context.getPackageManager().getApplicationInfo("com.example.name", 0);
                if (isUserApp(app)){
                    appUsages.remove(i);
                    i--;
                    return appUsages;
                }
            }catch (PackageManager.NameNotFoundException e) {
               // e.printStackTrace();
            }
            if (appUsages.size() > i)
                if (appUsages.get(i).getTotalTimeInForeground() == 0 ){
                    appUsages.remove(i);
                    i--;
                }else {
                    if (appUsages.get(i).getPackage_name().contains("systemui") ||
                            appUsages.get(i).getPackage_name().contains("launcher") ||
                            appUsages.get(i).getPackage_name().contains("incallui") ||
                            appUsages.get(i).getPackage_name().contains("MtpApplication")){
                        appUsages.remove(i);
                        i--;
                    }else{
                        ApplicationInfo ai;

                        try {
                            ai = pm.getApplicationInfo( appUsages.get(i).getPackage_name(), 0);
                        } catch (final PackageManager.NameNotFoundException e) {
                            ai = null;
                        }
                        boolean continuar = true;
                        if (i < appUsages.size()) {
                            if (ai == null){
                                appUsages.remove(i);
                                i--;
                                continuar = false;
                            }else {
                                appUsages.get(i).setAppName((String) (ai != null ? pm.getApplicationLabel(ai) : "(unknown)"));
                            }
                        }

                        int cont = 0;
                        for (AppUsage app : appUsages) {
                            try {
                                Drawable icon = context.getPackageManager().getApplicationIcon(app.getPackage_name());
                                if (icon != null){
                                    appUsages.get(cont).setColor(setDominantColor(icon));
                                }else{
                                    Random rnd = new Random();
                                    appUsages.get(cont).setColor(Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256)));
                                }
                                cont++;
                            } catch (PackageManager.NameNotFoundException e) {
                            }
                        }

                    }
                }
        }

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setAppUsages(appUsages);
        app.guardarEnPreferencias(context);

        return appUsages;

    }




    public static boolean isUserApp(ApplicationInfo ai) {
        if (ai.sourceDir.startsWith("/data/app/") ){
            return true;
        } else
            return ai.packageName.contains("dialer") || ai.packageName.contains("call") || ai.packageName.contains("message") || ai.packageName.contains("sms") || ai.packageName.contains("messaging");
    }

    public static int setDominantColor(Drawable drawable) {
        Bitmap bitmap;
        int vibrant;
        try {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;

            bitmap = bitmapDrawable.getBitmap();

            Palette palette = Palette.from(bitmap).generate();

            int defaultColor = 0xFF000000;
            vibrant = palette.getDominantColor(defaultColor);
        }
        catch (ClassCastException e){
           // e.printStackTrace();
            vibrant = -1;
        }

        return vibrant;
    }


    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }


