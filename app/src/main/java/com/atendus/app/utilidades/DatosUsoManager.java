package com.bfree.app.utilidades;

import android.content.Context;

import com.bfree.app.clases.Ajustes;
import com.bfree.app.clases.Aplicacion;


public class DatosUsoManager {

    //Patrón de funcionamiento de singletón
    private static final DatosUsoManager instance = new DatosUsoManager();
    private DatosUsoManager(){}
    public static DatosUsoManager getInstance(){
        return instance;
    }

    private Ajustes ajustes = new Ajustes();


    public void loadFromPrefs(Context c){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        ajustes = app.getAjustes();
        ajustes.checkDay();
    }

    public void saveInPrefs(Context c){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(c);
        app.setAjustes(ajustes);
        app.guardarEnPreferencias(c);
    }


    public Ajustes getAjustes() {
        return ajustes;
    }

    public void setAjustes(Ajustes ajustes) {
        this.ajustes = ajustes;
    }
}
