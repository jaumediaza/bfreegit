package com.atendus.app.services;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.atendus.app.clases.Aplicacion;
import com.atendus.app.utilidades.PhoneStateManager;


public class AdminReceiver extends DeviceAdminReceiver {
    public static final String ACTION_DISABLED = "device_admin_action_disabled";
    public static final String ACTION_ENABLED = "device_admin_action_enabled";

    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        app.setDeviceAdmin(false);
        PhoneStateManager.getInstance().setDeviceAdmin(false);
        app.guardarEnPreferencias(context);
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(ACTION_DISABLED));
    }
    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(context);
        PhoneStateManager.getInstance().setDeviceAdmin(true);
        app.setDeviceAdmin(true);
        app.guardarEnPreferencias(context);
        LocalBroadcastManager.getInstance(context).sendBroadcast(
                new Intent(ACTION_ENABLED));
    