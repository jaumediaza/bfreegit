package com.atendus.app.services;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.atendus.app.ActivityPrincipal;
import com.atendus.app.utilidades.PhoneStateManager;


public class ServicioDesconexionBroadcastReceiver extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        PhoneStateManager.getInstance().cargarDatosDeAplicacion(context);
        if (!PhoneStateManager.getInstance().isDesconectado()
                && !PhoneStateManager.getInstance().isFromProgramarDesconexion()
                /*|| isMyServiceRunning(NotificationMonitorBroadcastReceiver.class, context)
                && !PhoneStateManager.getInstance().isFromProgramarDesconexion()*/)
            return;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N && intent.getAction().equals("android.intent.action.LOCKED_BOOT_COMPLETED")
                || Build.VERSION.SDK_INT < Build.VERSION_CODES.N && intent.getAction().equals("android.intent.action.BOOT_COMPLETED")){
            Intent i = new Intent(context, ActivityPrincipal.class);
            context.startActivity(i);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, ServicioDesconexion.class));
            } else {
                context.startService(new Intent(context, ServicioDesconexion.class));
            }
            return;
        }

        if (intent.getAction().equals("com.tresandroides.com.bfree.services.RestartServicioDesconexion")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                context.startForegroundService(new Intent(context, ServicioDesconexion.class));
            } else {
                context.startService(new Intent(context, ServicioDesconexion.class));
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    