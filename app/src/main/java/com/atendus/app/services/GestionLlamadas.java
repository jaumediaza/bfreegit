package com.atendus.app.services;

import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.telephony.TelephonyManager;
import android.widget.Toast;

import com.android.internal.telephony.ITelephony;
import com.atendus.app.clases.NotificacionInfo;
import com.atendus.app.utilidades.PhoneStateManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;


public class GestionLlamadas extends BroadcastReceiver {

    ITelephony telephonyService;
    String number;
    static Ringtone r = null;



    public GestionLlamadas() {
        super();
        // TODO esto puede producir algún error, no sabemos cómo afecta el guardar las activities fuera de ellas
        try {
            TelephonyManager tm = (TelephonyManager) PhoneStateManager.getInstance().getCurrentActivity().getSystemService(Context.TELEPHONY_SERVICE);
            Method m = tm.getClass().getDeclaredMethod("getITelephony");
            m.setAccessible(true);
            telephonyService = (ITelephony) m.invoke(tm);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onReceive(final Context context, Intent intent) {

        //Si no se bloquean llamadas termina
        try {
            if (!PhoneStateManager.getInstance().isDesconectado()){
                return;
            }else if (!PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()) {
                AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                //TODO MIRAR ESTA MIERDA
                am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
                if (r == null){
                    Uri call = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    r = RingtoneManager.getRingtone(context, call);
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    r.play();
                }else if (!r.isPlaying()){
                    Uri call = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);
                    r = RingtoneManager.getRingtone(context, call);
                    am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                    r.play();
                }else{
                    r.stop();
                }
                return;
            }
        }catch (NullPointerException e){
            e.printStackTrace();
            PhoneStateManager.getInstance().cargarDatosDeAplicacion(context);
            if (!PhoneStateManager.getInstance().isDesconectado()){
                return;
            }else if (!PhoneStateManager.getInstance().getDesconexion().isBloquearLlamadas()) {
                AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                return;
            }
        }


        try
        {
            String state = intent.getStringExtra(TelephonyManager.EXTRA_STATE);


            if(state.equals("RINGING")){
                number = intent.getExtras().getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
                String number2="+34"+number;
                if ((number != null)) {

                    if (PhoneStateManager.getInstance().getTfnosPermitidosNumeros().contains(number)
                            || PhoneStateManager.getInstance().getTfnosPermitidosNumeros().contains(number2)){
                        AudioManager am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
                        am.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
                        return;
                    }

                    telephonyService.endCall();
                    boolean encontrado = false;
                    for (int i =  0; i < PhoneStateManager.getInstance().getNotificacioines()
                            .getList().size(); ++i){
                        if (PhoneStateManager.getInstance().getNotificacioines().getList().get(i).getPackageName().equals("Llamada")){
                            PhoneStateManager.getInstance().getNotificacioines().getList().get(i).addNewNotificaction();
                            encontrado = true;
                        }
                    }
                    if (!encontrado){
                        NotificacionInfo notif =  new NotificacionInfo("Llamada");
                        notif.setAppname("Llamadas");
                        PhoneStateManager.getInstance().getNotificacioines().getList().add(notif);
                    }
                }

                if (PhoneStateManager.getInstance().isLocked())
                    lock(context);
            }

        }
        catch(Exception e)
        {
            e.printStackTrace();
        }





    }


    public static void lock(Context context) {
        DevicePolicyManager policy = (DevicePolicyManager)
                context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        try {
            policy.lockNow();
        } catch (SecurityException ex) {
            Toast.makeText(
                    context,
                    "must enable device administrator",
                    Toast.LENGTH_LONG).show();
            ComponentName admin = new ComponentName(context.getApplicationContext(), AdminReceiver.class);
            Intent intent = new Intent(
                    DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN).putExtra(
                    DevicePolicyManager.EXTRA_DEVICE_ADMIN, admin);
            context.getApplicationContext().startActivity(intent);
        }

    }