package com.atendus.app.services;

import android.Manifest;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.atendus.app.ActivityAppNoPermitida;
import com.atendus.app.ActivityPrincipal;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.json.JSONEnviarEstadoDispositivo;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.ViewPagerManager;

import org.json.JSONException;

import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


public class ServicioDesconexion extends Service {

    private Thread t;
    private ServicioDesconexionBroadcastReceiver receiver;
    private GestionLlamadas receiver2;


    private static CountDownTimer contadorDesconexion;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        CheckRunningActivity runningAct = new CheckRunningActivity(this);
        t = new Thread(runningAct);
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        //PhoneStateManager.getInstance().setDesconectado(true);
        /*if (!PhoneStateManager.getInstance().isDesconectado())
            return START_NOT_STICKY;*/

        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("com.tresandroides.com.bfree.services.RestartServicioDesconexion");
        filter2.addAction("android.intent.action.BOOT_COMPLETED");
        filter2.addAction("android.intent.action.LOCKED_BOOT_COMPLETED");

        receiver = new ServicioDesconexionBroadcastReceiver();
        try {
            unregisterReceiver(receiver);
        }catch (Exception e){
            e.printStackTrace();
        }
        registerReceiver(receiver, filter2);

        receiver2 = new GestionLlamadas();
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.PHONE_STATE");
        registerReceiver(receiver2, filter);

        startTimer();

        CheckRunningActivity runningAct = new CheckRunningActivity(this);
        t = new Thread(runningAct);
        t.start();

        return START_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        String channel = "CHANNEL_DESCONEXIÓN";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel, "BFree sigue funcionando para ofrecerle unas estadísticas precisas", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setSound(null, null);
            notificationChannel.setVibrationPattern(new long[]{0L});
            notificationChannel.setLockscreenVisibility(MODE_PRIVATE);
            notificationChannel.setShowBadge(false);
            notificationChannel.setDescription("BFree sigue funcionando para ofrecerle unas estadísticas precisas");
            notificationManager.createNotificationChannel(notificationChannel);
        }else{
            channel = "";
        }

        Intent showTaskIntent = new Intent(getApplicationContext(), ActivityPrincipal.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channel)
                .build();

        PhoneStateManager.getInstance().setDesconectado(true);
        PhoneStateManager.getInstance().setDesconectado(true);
        startForeground(501, notification);

        Log.d("BFree_pruebas_1", "Servicio desconexión iniciado");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (t!= null)
            t.interrupt();
        if (contadorDesconexion != null)
            contadorDesconexion.cancel();
        unregisterReceiver(receiver);
        Intent broadcastIntent = new Intent("com.tresandroides.com.bfree.services.RestartServicioDesconexion");
        sendBroadcast(broadcastIntent);
        unregisterReceiver(receiver2);
        Log.d("BFree_pruebas_1", "Servicio desconexión destruido");
    }

    private void startTimer(){
        final long tiempodesconexion;
        tiempodesconexion = PhoneStateManager.getInstance().getMillisUntilFinished();
        PhoneStateManager.getInstance().setDesconectado(true);
        Log.d("Desconexion programada", "servicio tiempo " + tiempodesconexion );
        Log.d("BFree_pruebas_1", "Servicio desconexión nueva desconexion");
        contadorDesconexion=new CountDownTimer(
                tiempodesconexion, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                if (!PhoneStateManager.getInstance().isDesconectado()) {
                    Log.d("Desconexion programada", "servicio cancelado" );
                    Log.d("BFree_pruebas_1", "Servicio desconexión desconexión terminada");
                    PhoneStateManager.getInstance().setEditable(true);
                    // APUNTE 3A -> Esto estaba comentado por alg
                    //contadorDesconexion.cancel();
                    contadorDesconexion.cancel();
                    return;
                }
                if (ViewPagerManager.getInstance().fragmentDesconecta != null) {
                    ViewPagerManager.getInstance().fragmentDesconecta
                    .tickDesconectar(millisUntilFinished);
                }
                PhoneStateManager.getInstance().setMillisUntilFinished(millisUntilFinished);
                PhoneStateManager.getInstance().guardarDatosEnAplicacion(getBaseContext());

            }

            @Override
            public void onFinish() {
                PhoneStateManager.getInstance().setEditable(true);
                if (ViewPagerManager.getInstance().fragmentDesconecta !=null)
                    ViewPagerManager.getInstance().fragmentDesconecta
                            .finishFromTimer();

                new Thread(){
                    @Override
                    public void run() {
                        super.run();
                        if (ActivityCompat.checkSelfPermission(getApplication(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }
                        TelephonyManager telephonyManager = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);
                        final String iddisp = telephonyManager.getDeviceId();

                        JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                                new JSONEnviarEstadoDispositivo(getApplication());
                        int estdao = 0;
                        if (PhoneStateManager.getInstance().isDesconectado())
                            estdao = 1;
                        try {
                            Aplicacion app = new Aplicacion();
                            app.cargarAplicacionDePreferencias(getApplication());
                            jsonEnviarEstadoDispositivo.run(app.getUsuario().getId(), iddisp, estdao);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }.start();
                Log.d("BFree pruebas 1", "Servicio desconexión desconexión terminada");
            }
        }.start();
    }

    private class CheckRunningActivity implements Runnable {


        Context context;

        public CheckRunningActivity(Context c){
            context = c;
        }

        @Override
        public void run() {
            while (true) {
                if (PhoneStateManager.getInstance().isDesconectado() &&
                        PhoneStateManager.getInstance().getDesconexion().isBloquearNotificaciones()) {

                    String currentApp = "NULL";
                    if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        UsageStatsManager usm = (UsageStatsManager)context.getSystemService(USAGE_STATS_SERVICE);
                        long time = System.currentTimeMillis();
                        List<UsageStats> appList = usm.queryUsageStats(UsageStatsManager.INTERVAL_DAILY,  time - 1000*1000, time);
                        if (appList != null && appList.size() > 0) {
                            SortedMap<Long, UsageStats> mySortedMap = new TreeMap<Long, UsageStats>();
                            for (UsageStats usageStats : appList) {
                                mySortedMap.put(usageStats.getLastTimeUsed(), usageStats);
                            }
                            if (mySortedMap != null && !mySortedMap.isEmpty()) {
                                currentApp = mySortedMap.get(mySortedMap.lastKey()).getPackageName();
                                //Log.d("WEEEEEEEEEEE", currentApp);
                            }
                        }
                    } else {
                        ActivityManager am = (ActivityManager)context.getSystemService(Context.ACTIVITY_SERVICE);
                        List<ActivityManager.RunningAppProcessInfo> tasks = am.getRunningAppProcesses();
                        currentApp = tasks.get(0).processName;
                    }
                    //Puesto por Alberto porque parece que entraba en bucle infinito...
                    // Actualiz. movido arriba para que directamente si es Bfree no haga nada
                    if (!currentApp.equals("com.bfree.app")) {

                        List<AplicacionPermitida> apps = PhoneStateManager.getInstance().getAppsPermitidas();
                        if (apps != null) {

                            boolean encontrado = false;
                            for (int i = 0; i < apps.size(); ++i) {
                                if (apps.get(i).getPackageName().equals(currentApp)
                                        && !apps.get(i).isUsoDuranteDesconecta()
                                        && !currentApp.equals(NotificationMonitor.getLastNotif())
                                        && !currentApp.contains("incallui")
                                        && !currentApp.contains("messaging")
                                        && !currentApp.contains("launcher") || currentApp.contains("camera")) {
                                    //Si estás en el escritorio y te llaman entra aquí
                                    encontrado = true;
                                }
                            }

                            if (encontrado) {
                                Intent startMain = new Intent(Intent.ACTION_MAIN);
                                startMain.addCategory(Intent.CATEGORY_HOME);
                                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(startMain);


                                ActivityAppNoPermitida.fromService = true;

                                Intent intent = new Intent(context, ActivityAppNoPermitida.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);


                            }
                        }
                    }

                }else{
                    return;
                }
            }
        }