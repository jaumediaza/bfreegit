package com.atendus.app.services;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.IBinder;
import android.os.Looper;
import android.os.PowerManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.atendus.app.ActivityAvisoEstadisticas;
import com.atendus.app.ActivityPrincipal;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.AplicacionPermitida;
import com.atendus.app.clases.AppUsage;
import com.atendus.app.clases.ConfiguracionDesconexion;
import com.atendus.app.clases.Telefono;
import com.atendus.app.json.AESCrypt;
import com.atendus.app.json.JSONAddApp;
import com.atendus.app.json.JSONEnviarEstadisticas;
import com.atendus.app.json.JSONEnviarEstadisticasUso;
import com.atendus.app.json.JSONEnviarEstadoDispositivo;
import com.atendus.app.json.JSONEnviarListinTelefonico;
import com.atendus.app.utilidades.DatosUsoManager;
import com.atendus.app.utilidades.PhoneStateManager;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;



public class ServicioDatos extends Service {

    private ScreenReceiver screenReceiver;
    private ServicioDatosRestartBroadcastReceiver receiver;
    //public long usageTime = 0;
    //private int cont = 0;
    public ServicioDatos(){}
    private boolean entrar = true;
    private boolean entrar2 = true;

    private boolean looperSet = true;

    private int val = 59;
    private int idusu = 0;
    private int contadorEstadisticas = 30;
    private int contadorColaEnvios = 0;
    private int contadorEstado = 0;
    private static int mm = 0;


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        String channel = "CHANNEL_DESCONEXIÓN";
        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel notificationChannel = new NotificationChannel(channel, "BFree sigue funcionando para ofrecerle unas estadísticas precisas", NotificationManager.IMPORTANCE_DEFAULT);
            notificationChannel.setSound(null, null);
            notificationChannel.setVibrationPattern(new long[]{0L});
            notificationChannel.setLockscreenVisibility(MODE_PRIVATE);
            notificationChannel.setShowBadge(false);
            notificationChannel.setDescription("BFree sigue funcionando para ofrecerle unas estadísticas precisas");
            notificationManager.createNotificationChannel(notificationChannel);
        }else{
            channel = "";
        }

        Intent showTaskIntent = new Intent(getApplicationContext(), ActivityPrincipal.class);
        showTaskIntent.setAction(Intent.ACTION_MAIN);
        showTaskIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        showTaskIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent contentIntent = PendingIntent.getActivity(
                getApplicationContext(),
                0,
                showTaskIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification notification = new NotificationCompat.Builder(getApplicationContext(), channel)
                .build();


        startForeground(501, notification);

        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());
        idusu = app.getUsuario().getId();

        //TODO 3A Esto puede consumir mucha batería
        PowerManager mgr = (PowerManager)getSystemService(Context.POWER_SERVICE);
        PowerManager.WakeLock wakeLock = mgr.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
        wakeLock.acquire();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);



        DatosUsoManager.getInstance().loadFromPrefs(getBaseContext());

        //cont = Calendar.getInstance().get(Calendar.SECOND);


        IntentFilter filter = new IntentFilter();
        filter.addAction("android.intent.action.SCREEN_OFF");
        filter.addAction("android.intent.action.SCREEN_ON");
        filter.addAction("android.intent.action.USER_PRESENT");
        screenReceiver = new ScreenReceiver();
        registerReceiver(screenReceiver, filter);

        IntentFilter filter2 = new IntentFilter();
        filter2.addAction("com.tresandroides.com.bfree.services.RestartServicioDatos");
        filter2.addAction("android.intent.action.BOOT_COMPLETED");
        filter2.addAction("android.intent.action.LOCKED_BOOT_COMPLETED");
        receiver = new ServicioDatosRestartBroadcastReceiver();
        try{
            unregisterReceiver(receiver);
        }catch (Exception e){
            e.printStackTrace();
        }
        registerReceiver(receiver, filter2);
        startTimer();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        Log.d("Hola_paso_por_aqui", "ServicioDatosRestartBroadcastReceiver");
        super.onDestroy();
        DatosUsoManager.getInstance().saveInPrefs(getBaseContext());
        try {
            unregisterReceiver(screenReceiver);
            Intent broadcastIntent = new Intent("com.tresandroides.com.bfree.services.RestartServicioDatos");
            sendBroadcast(broadcastIntent);
            stoptimertask();
        }
        catch(Exception e){

        }

        //unregisterReceiver(receiver);
    }

    private Timer timer;
    private TimerTask timerTask;
    long oldTime=0;
    public void startTimer() {
        //set a new Timer
        timer = new Timer();
        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, to wake up every 1 second
        timer.schedule(timerTask, 1000, 1000); //
    }

    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        timerTask = new TimerTask() {

            public void run() {

                if (looperSet) {
                    Looper.prepare();
                    looperSet = false;
                }
                if (DatosUsoManager.getInstance().getAjustes().isUserPresent()) {
                    DatosUsoManager.getInstance().getAjustes().addOneSecondOfUsage();
                    guardarDatosDeUso();
                }

                if (Calendar.getInstance().get(Calendar.SECOND) > 55 ) {
                    entrar = true;
                }

                if (Calendar.getInstance().get(Calendar.SECOND) < 10 && entrar) {
                    entrar = false;
                    datosDeUso();
                }

                desconexionesProgramadas();

                val++;

                if (val >= 60){
                    val = 0;
                    //Enviar datos de apps al servidor

                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    Aplicacion app = new Aplicacion();
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                    app.cargarAplicacionDePreferencias(getBaseContext());
                    final int idusu = app.getUsuario().getId();
                    final String iddisp = telephonyManager.getDeviceId();
                    Log.d("BFreePruebas", "enviando datos...");

                    new Thread(){
                        @Override
                        public void run() {
                            super.run();

                            JSONAddApp jsonAddApp = new JSONAddApp(getBaseContext());
                            String strLbl = "";
                            String strPckg = "";
                            for (AplicacionPermitida nApp : PhoneStateManager.getInstance().getAppsPermitidas()) {
                                try {
                                    if (nApp.getAppLabel().contains("/")){
                                        String nstr = nApp.getAppLabel().replace("/", " ");
                                        strLbl = strLbl.concat(URLEncoder.encode(AESCrypt.encrypt(nstr), "UTF-8") + "/");
                                    }else {
                                        strLbl = strLbl.concat(URLEncoder.encode(AESCrypt.encrypt(nApp.getAppLabel()), "UTF-8") + "/");
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                                try {
                                    strPckg = strPckg.concat(AESCrypt.encrypt(nApp.getPackageName()) + "/");
                                }
                                catch(Exception e){

                                }
                            }
                            try {
                                jsonAddApp.run(idusu, iddisp, strLbl, strPckg);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            Log.d("BFreePruebas", "envio terminado");
                        }
                    }.start();

                    PhoneStateManager.getInstance().cargarAplicacionesInstaladas();

                    enviarListinTelefonico(iddisp);


                }

                actualizarEstadisticasDiarias();

                //Enviar estadisiticas
                contadorEstadisticas++;
                if (contadorEstadisticas >= 30){
                    contadorEstadisticas = 0;
                    if (ActivityCompat.checkSelfPermission(getBaseContext(),
                            Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    Aplicacion app = new Aplicacion();
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

                    app.cargarAplicacionDePreferencias(getBaseContext());
                    final int idusu = app.getUsuario().getId();
                    final String iddisp = telephonyManager.getDeviceId();

                    if (idusu != -1) {
                        //Diarias
                        enviarEstadisticasDiarias(idusu, iddisp);
                        //Semanales
                        enviarEstadisticasSemanales(idusu, iddisp);
                        //mensuales
                        enviarEstadisticasMensuales(idusu, iddisp);
                    }
                }

                //Cola envios
                contadorColaEnvios++;
                if (contadorColaEnvios >= 30){
                    contadorColaEnvios = 0;
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getBaseContext());
                    if (app.getColaEnvios().pendientes && TresAndroides.isOnline(getBaseContext())){
                        boolean done = app.getColaEnvios().enviarTodos(getBaseContext(),
                                app.getUsuario().getId());
                        app.getColaEnvios().pendientes = !done;
                        app.guardarEnPreferencias(getBaseContext());
                    }
                }

                //Envío del estado del terminal (desconectado o no)
                //Envío de estadísticas de uso
                contadorEstado++;
                if (contadorEstado >= 30){
                    contadorEstado = 0;

                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                    final String iddisp = telephonyManager.getDeviceId();

                    JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                            new JSONEnviarEstadoDispositivo(getBaseContext());

                    JSONEnviarEstadisticasUso jsonEnviarEstadisticasUso =
                            new JSONEnviarEstadisticasUso(getBaseContext());
                    Aplicacion app = new Aplicacion();
                    app.cargarAplicacionDePreferencias(getBaseContext());

                    int estdao = 0;
                    if (PhoneStateManager.getInstance().isDesconectado())
                        estdao = 1;
                    try {
                        jsonEnviarEstadoDispositivo.run(idusu, iddisp, estdao);
                        jsonEnviarEstadisticasUso.run(idusu, iddisp,  app.getEstadisticaUsoDia(),
                                app.getEstadisticaUsoSemana(), app.getEstadisticaUsoMes());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        };


    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    private void datosDeUso(){
        //Alerta de tiempo de uso del terminal
        if (DatosUsoManager.getInstance().getAjustes().isAlertaUsoMovil()) {
            int hh1 = DatosUsoManager.getInstance().getAjustes().getUsoMovil().hh;
            int mm1 = DatosUsoManager.getInstance().getAjustes().getUsoMovil().mm;

            long milliseconds = DatosUsoManager.getInstance().getAjustes().getUsageTime();
            int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
            int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);

            if (hours == hh1 && mm1 == minutes) {
                Intent intrtrn = new Intent(getBaseContext(), ActivityAvisoEstadisticas.class);
                intrtrn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityAvisoEstadisticas.isDesbloqueos = 2;
                ActivityAvisoEstadisticas.fromService = true;
                getBaseContext().startActivity(intrtrn);

            }
        }

        //Alerta de uso diario del terminal
        if (DatosUsoManager.getInstance().getAjustes().isNotificarResumenDiario()){
            int hours = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().hh;
            int minutes = DatosUsoManager.getInstance().getAjustes().getHoraResumenDiario().mm;

            int mm = Calendar.getInstance().get(Calendar.MINUTE);
            int hh = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

            if(hours == hh && minutes == mm){

                Intent intrtrn = new Intent(getBaseContext(), ActivityAvisoEstadisticas.class);
                intrtrn.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                ActivityAvisoEstadisticas.isDesbloqueos = 3;
                ActivityAvisoEstadisticas.fromService = true;
                getBaseContext().startActivity(intrtrn);

            }
        }
        DatosUsoManager.getInstance().saveInPrefs(getBaseContext());
    }
    private void desconexionesProgramadas(){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());
        Calendar cal = Calendar.getInstance();
        int hh = cal.get(Calendar.HOUR_OF_DAY);
        int lmin = mm;
        mm = cal.get(Calendar.MINUTE);

        if (lmin == mm){
            return;
        }

        for (int i = 0; i < app.getDesconexionesProgramadas().getList().size(); ++i){
            if (app.getDesconexionesProgramadas().getList().get(i).isEnabled()){
                if (app.getDesconexionesProgramadas().getList().get(i).getHoraInicio() == hh &&
                        app.getDesconexionesProgramadas().getList().get(i).getMinutoInicio() == mm){

                    int day = cal.get(Calendar.DAY_OF_WEEK);
                    boolean dias[] = app.getDesconexionesProgramadas().getList().get(i).getDias();
                    switch (day){
                        case Calendar.MONDAY:{
                            if (!dias[0])
                                return;
                            break;
                        }
                        case Calendar.TUESDAY:{
                            if (!dias[1])
                                return;
                            break;
                        }
                        case Calendar.WEDNESDAY:{
                            if (!dias[2])
                                return;
                            break;
                        }
                        case Calendar.THURSDAY:{
                            if (!dias[3])
                                return;
                            break;
                        }
                        case Calendar.FRIDAY:{
                            if (!dias[4])
                                return;
                            break;
                        }
                        case Calendar.SATURDAY:{
                            if (!dias[5])
                                return;
                            break;
                        }
                        case Calendar.SUNDAY:{
                            if (!dias[6])
                                return;
                            break;
                        }
                    }

                    Log.d("Desconexion programada", "" + day);

                    if (PhoneStateManager.getInstance().isDesconectado()){
                        PhoneStateManager.getInstance().setDesconectado(false);
                        try {
                            Thread.sleep(1500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }


                    Intent startMain = new Intent(Intent.ACTION_MAIN);
                    startMain.addCategory(Intent.CATEGORY_HOME);
                    startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(startMain);

                    int horas = app.getDesconexionesProgramadas().getList().get(i)
                            .getHoraFin() - app.getDesconexionesProgramadas()
                            .getList().get(i).getHoraInicio();

                    int minutos = app.getDesconexionesProgramadas().getList().get(i).
                            getMinutoFin() - app.getDesconexionesProgramadas()
                            .getList().get(i).getMinutoInicio();

                    if (horas < 0) {
                        horas = 24 + horas;
                    }
                    if (minutos < 0) {
                        horas--;
                        minutos = 60 + minutos;
                    }

                    PhoneStateManager.getInstance().setFromProgramarDesconexion(true);

                    Log.d("Desconexion programada", "Horas " + horas);
                    Log.d("Desconexion programada", "minutos " + minutos);

                    ConfiguracionDesconexion desconexion = new ConfiguracionDesconexion();
                    int tiempo  = horas * 60 * 60 *1000 + minutos * 60 *1000;
                    desconexion.setTiempoDesconexion(tiempo, horas, minutos);
                    Log.d("Desconexion programada", "Tiempo " + tiempo);
                    desconexion.setBloquearNotificaciones(app.getDesconexionesProgramadas()
                            .getList().get(i).isBloquearNotificaciones());
                    desconexion.setBloquearLlamadas(app.getDesconexionesProgramadas()
                            .getList().get(i).isBloquearLlamadas());

                    String[] appsPermitidas;
                    String auxApps = app.getDesconexionesProgramadas()
                            .getList().get(i).getAplicacionesPermitidas();
                    appsPermitidas = auxApps.split("/");
                    if (appsPermitidas.length > 0) {
                        for (int p = 0; p < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++p) {
                            PhoneStateManager.getInstance().getAppsPermitidas().get(p).setPermitida(false);
                            PhoneStateManager.getInstance().getAppsPermitidas().get(p).setUsoDuranteDesconecta(false);
                            for (int j = 0; j < appsPermitidas.length; ++j) {
                                if (PhoneStateManager.getInstance().getAppsPermitidas().get(p).getPackageName().equals(appsPermitidas[j])) {
                                    PhoneStateManager.getInstance().getAppsPermitidas().get(p).setPermitida(true);
                                    PhoneStateManager.getInstance().getAppsPermitidas().get(p).setUsoDuranteDesconecta(true);
                                }
                            }
                        }
                    }else{
                        for (int p = 0; p < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++p) {
                            PhoneStateManager.getInstance().getAppsPermitidas().get(p).setPermitida(false);
                            PhoneStateManager.getInstance().getAppsPermitidas().get(p).setUsoDuranteDesconecta(false);
                        }
                    }

                    PhoneStateManager.getInstance().setDesconectado(true);
                    PhoneStateManager.getInstance().getDesconexion().setTiempoDesconexion(
                            desconexion.getTiempoDesconexion(), desconexion.getHorasDesconexion(),
                            desconexion.getMinutosDesconexion());
                    PhoneStateManager.getInstance().getDesconexion().setBloquearLlamadas(desconexion.isBloquearLlamadas());
                    PhoneStateManager.getInstance().getDesconexion().setBloquearNotificaciones(desconexion.isBloquearNotificaciones());
                    PhoneStateManager.getInstance().getNotificacioines().getList().clear();

                    PhoneStateManager.getInstance().getDesconexion().setlDateStart();
                    PhoneStateManager.getInstance().getDesconexion().setAudioStatus(getBaseContext());

                    PhoneStateManager.getInstance().setMillisUntilFinished(tiempo);
                    PhoneStateManager.getInstance().guardarDatosEnAplicacion(getBaseContext());

                    if(!app.getDesconexionesProgramadas().getList().get(i).isEditable())
                        PhoneStateManager.getInstance().setEditable(false);

                    PhoneStateManager.getInstance().setDesconectado(true);
                    PhoneStateManager.getInstance().setDesconectado(true);
                    Intent servicioDesconexion = new Intent(getBaseContext(), ServicioDesconexion.class);
                    Intent notificationMonitor = new Intent(getBaseContext(), NotificationMonitor.class);

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        getBaseContext().startForegroundService(servicioDesconexion);
                        getBaseContext().startForegroundService(notificationMonitor);
                        Log.d("Desconexion programada", "ServicioIniciado");
                    } else {
                        getBaseContext().startService(servicioDesconexion);
                        getBaseContext().startService(notificationMonitor);
                        Log.d("Desconexion programada", "ServicioIniciado");
                    }


                    Intent intnt = new Intent(getBaseContext(), ActivityPrincipal.class);
                    startActivity(intnt);

                    Log.d("Desconexion programada", "Activity Principal");
                    //Comprobar si hay que desactivar la desconexión

                    if (app.getDesconexionesProgramadas().getList().get(i).getRepetir() == 2){
                        return;
                    }

                    int start = 0;
                    switch (day) {
                        case Calendar.MONDAY:{
                            start = 1;
                            break;
                        }
                        case Calendar.TUESDAY:{
                            start = 2;
                            break;
                        }
                        case Calendar.WEDNESDAY:{
                            start = 3;
                            break;
                        }
                        case Calendar.THURSDAY:{
                            start = 4;
                            break;
                        }
                        case Calendar.FRIDAY:{
                            start = 5;
                            break;
                        }
                        case Calendar.SATURDAY:{
                            start = 6;
                            break;
                        }
                        case Calendar.SUNDAY:{
                            start = -1;
                            break;
                        }

                    }

                    boolean enabled = false;
                    if (start != -1)
                        for (int k = start; k < dias.length; ++k ){
                            if (dias[k]){
                                enabled = true;
                            }
                        }

                    Log.d("Desconexion programada", "Sigue activado " + enabled);
                    app.getDesconexionesProgramadas().getList().get(i).setEnabled(enabled);
                    app.guardarEnPreferencias(getBaseContext());

                    if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                        return;
                    }
                    TelephonyManager telephonyManager = (TelephonyManager) getApplication().getSystemService(Context.TELEPHONY_SERVICE);
                    final String iddisp = telephonyManager.getDeviceId();

                    JSONEnviarEstadoDispositivo jsonEnviarEstadoDispositivo =
                            new JSONEnviarEstadoDispositivo(getBaseContext());
                    int estdao = 0;
                    if (PhoneStateManager.getInstance().isDesconectado())
                        estdao = 1;
                    try {
                        jsonEnviarEstadoDispositivo.run(app.getUsuario().getId(), iddisp, estdao);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
    private void actualizarEstadisticasDiarias() {
        if (Calendar.getInstance().get(Calendar.HOUR_OF_DAY) == 23
                && Calendar.getInstance().get(Calendar.MINUTE) == 59) {

            if (Calendar.getInstance().get(Calendar.SECOND) > 5 ) {
                entrar2 = true;
            }
            if (Calendar.getInstance().get(Calendar.SECOND) > 55 && entrar2) {
                entrar2 = false;


            }
        }
    }

    private void enviarEstadisticasDiarias(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getDailyStatistics(getBaseContext());
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg,1);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasSemanales(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getWeeklyStatistics(getBaseContext());
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, 2);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstadisticasMensuales(final int idusu, final String iddisp){
        List<AppUsage> daily = TresAndroides.getMonthlyStatistics(getBaseContext());
        String pckg ="";
        String timeInForeground="";
        String name = "";
        String clr = "";
        long total = 0;
        String pctg = "";
        assert daily != null;
        for (AppUsage apusu : daily) {
            total += apusu.getTotalTimeInForeground();
        }
        for (AppUsage apusu : daily) {
            try {
                if (apusu.getAppName().contains("/")) {
                    String aux = apusu.getAppName().replace("/", " ");
                    name = name.concat(URLEncoder.encode(aux, "UTF-8") + "/");
                } else {
                    name = name.concat(URLEncoder.encode(apusu.getAppName(), "UTF-8") + "/");
                }

                pckg = pckg.concat(apusu.getPackage_name() + "/");
                timeInForeground = timeInForeground.concat(apusu.getTotalTimeInForeground() + "/");
                clr = clr.concat(apusu.getColor() + "/");
                double val = (((double)apusu.getTotalTimeInForeground())*100/((double)total));
                pctg = pctg.concat(val + "/");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        JSONEnviarEstadisticas jsonEnviarEstadisticas =
                new JSONEnviarEstadisticas(getBaseContext());

        try {
            jsonEnviarEstadisticas.run(idusu, iddisp, name, pckg, timeInForeground, clr, pctg, 3);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void enviarEstado(){

    }

    private void enviarListinTelefonico(final String iddisp){

        if (PhoneStateManager.getInstance().getTfnosPermitidos().size() == 0){
            try{
                if(getSharedPreferences("Preferencias",Context.MODE_PRIVATE).getInt("padre",0)==0) {
                    PhoneStateManager.getInstance().cargarListinTelefonicoThread(getContentResolver(), this);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
            return;
        }

        new Thread(){
            @Override
            public void run() {
                super.run();

                JSONEnviarListinTelefonico jsonTfnos = new JSONEnviarListinTelefonico(getBaseContext());
                String strNombre = "";
                String strNumero = "";
                //Aplicacion app = new Aplicacion();
                //app.cargarAplicacionDePreferencias(getApplication());
                int i = 0;
                for (Telefono tfno : PhoneStateManager.getInstance().getTfnosPermitidos()) {

                    try {
                        if (tfno.getNombre().contains("/")) {
                            String nstr = AESCrypt.encrypt(tfno.getNombre()).replace("/", " ");
                            //strNombre = strNombre.concat(URLEncoder.encode(nstr, "UTF-8") + "/");
                            strNombre = strNombre.concat(nstr + "/");
                        } else {
                            strNombre = strNombre.concat(AESCrypt.encrypt(tfno.getNombre()) + "/");
                        }
                        strNumero = strNumero.concat(AESCrypt.encrypt(tfno.getNumero())+"/");
                    }
                    catch(Exception e){

                        }

                    ++i;
                    /*if (i >= 100){
                        try {
                            jsonTfnos.run(idusu, iddisp, strNombre, strNumero);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        strNombre = "";
                        strNumero = "";
                    }*/
                }
                try {
                    jsonTfnos.run(idusu, iddisp, strNombre, strNumero);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("BFreePruebas", "envio terminado");
            }
        }.start();
    }

    private void guardarDatosDeUso(){
        Aplicacion app = new Aplicacion();
        app.cargarAplicacionDePreferencias(getBaseContext());

        boolean cambioFecha = false;

        //Uso diarios
        if (app.getEstadisticaUsoDia().getDatoFecha() == -1){
            app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(0);
        }else if(app.getEstadisticaUsoDia().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.DAY_OF_YEAR)){
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(app.getEstadisticaUsoDia().getTiempoUso()+1000);
        }else{
            app.getEstadisticaUsoDia().setDatoFecha(Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
            app.getEstadisticaUsoDia().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoDia().setTiempoUso(0);
            app.getEstadisticaUsoDia().setDesbloqueos(0);
            cambioFecha = true;
        }


        //Uso semanales
        if (app.getEstadisticaUsoSemana().getDatoFecha() == -1){
            app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(0);
        }else if(app.getEstadisticaUsoSemana().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.WEEK_OF_YEAR)){
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(app.getEstadisticaUsoSemana().getTiempoUso()+1000);
        }else{
            app.getEstadisticaUsoSemana().setDatoFecha(Calendar.getInstance().get(Calendar.WEEK_OF_YEAR));
            app.getEstadisticaUsoSemana().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoSemana().setTiempoUso(0);
            app.getEstadisticaUsoSemana().setDesbloqueos(0);
            cambioFecha = true;
        }


        //Uso mensuales
        if (app.getEstadisticaUsoMes().getDatoFecha() == -1){
            app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(0);
        }else if(app.getEstadisticaUsoMes().getDatoFecha() ==
                Calendar.getInstance().get(Calendar.MONTH)){
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(app.getEstadisticaUsoMes().getTiempoUso()+1000);
        }else{
            app.getEstadisticaUsoMes().setDatoFecha(Calendar.getInstance().get(Calendar.MONTH));
            app.getEstadisticaUsoMes().setlFecha(Calendar.getInstance().getTimeInMillis());
            app.getEstadisticaUsoMes().setTiempoUso(0);
            app.getEstadisticaUsoMes().setDesbloqueos(0);
            cambioFecha = true;
        }

        app.guardarEnPreferencias(getBaseContext());

        if (cambioFecha){
            JSONEnviarEstadisticasUso jsonEnviarEstadisticasUso =
                    new JSONEnviarEstadisticasUso(getBaseContext());
            if (ActivityCompat.checkSelfPermission(getBaseContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            final String iddisp = telephonyManager.getDeviceId();
            try {
                jsonEnviarEstadisticasUso.run(idusu, iddisp,  app.getEstadisticaUsoDia(),
                        app.getEstadisticaUsoSemana(), app.getEstadisticaUsoMes());
            } catch (JSONException e) {
                e.printStackTrace();
            }