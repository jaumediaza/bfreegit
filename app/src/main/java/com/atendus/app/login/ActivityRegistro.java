package com.atendus.app.login;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.Usuario;
import com.atendus.app.json.JSONRegistro;
import com.atendus.app.splash_inicio.ActivityInicio;
import com.atendus.app.utilidades.TresAndroides;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;

public class ActivityRegistro extends AppCompatActivity {

    private Thread tRegistro;
    private int resultadoRegistro=0;
    private Handler handlerRegistro;
    private Activity activity=null;
    private Aplicacion app=new Aplicacion();
    private ProgressBar progressBar;
    private LinearLayout lyDisabled;

    Typeface Roboto_Medium=null;
    Typeface Roboto_Light=null;

    private EditText edtxt_nombre,edtxt_pass,edtxt_pass2,edtxt_email;
    private TextView textView;


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TresAndroides.hideKeyboard(ActivityRegistro.this);
        Intent i = new Intent(ActivityRegistro.this, ActivityInicio.class);
        startActivity(i);
        Bungee.fade(activity);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        activity=this;
        app.cargarAplicacionDePreferencias(this);

        Roboto_Medium=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Medium.ttf");
        Roboto_Light=Typeface.createFromAsset(getAssets(),"fonts/Roboto-Light.ttf");

        edtxt_nombre= this.findViewById(R.id.edtxt_nombre);
        edtxt_pass= this.findViewById(R.id.edtxt_pass);
        edtxt_pass2= this.findViewById(R.id.edtxt_pass2);
        edtxt_email= this.findViewById(R.id.edtxt_email);
        textView= this.findViewById(R.id.txt_titulo);

        edtxt_nombre.setTypeface(Roboto_Light);
        edtxt_email.setTypeface(Roboto_Light);
        edtxt_pass.setTypeface(Roboto_Light);
        edtxt_pass2.setTypeface(Roboto_Light);
        textView.setTypeface(Roboto_Medium);


        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarRegistro();
            }
        });

        findViewById(R.id.done_big).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarRegistro();
            }
        });

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        lyDisabled = findViewById(R.id.ly_disabled);
        lyDisabled.setVisibility(View.GONE);



    }

    private void comprobarRegistro(){
        final EditText edtxt_name = findViewById(R.id.edtxt_nombre);
        final EditText edtxt_email = findViewById(R.id.edtxt_email);
        final EditText edtxt_pass = findViewById(R.id.edtxt_pass);
        final EditText edtxt_pass2 = findViewById(R.id.edtxt_pass2);

        String name = edtxt_name.getText().toString();
        String email = edtxt_email.getText().toString();
        String pass = edtxt_pass.getText().toString();

        if (name.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(getString(R.string.error_no_nombre_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_name.requestFocus();
                    TresAndroides.showKeyboard(ActivityRegistro.this);
                }
            });
            dlg.show();

        }else if (email.length() <= 0){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(getString(R.string.error_no_email_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(ActivityRegistro.this);
                }
            });
            dlg.show();

        }else if (!TresAndroides.validateEmail(email)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(getString(R.string.error_no_email_valido));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_email.requestFocus();
                    TresAndroides.showKeyboard(ActivityRegistro.this);
                }
            });
            dlg.show();
        }else if (pass.length() < 4){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(  getString(R.string.error_no_pass_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_pass.requestFocus();
                    TresAndroides.showKeyboard(ActivityRegistro.this);
                }
            });
            dlg.show();
        }else if (!edtxt_pass2.getText().toString().equals(pass)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(  getString(R.string.error_no_pass_match));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_pass.requestFocus();
                    TresAndroides.showKeyboard(ActivityRegistro.this);
                }
            });
            dlg.show();
        }else if (!TresAndroides.isOnline(ActivityRegistro.this)){
            AlertDialog.Builder dlg  = new AlertDialog.Builder(ActivityRegistro.this);
            dlg.setMessage(getString(R.string.error_no_internet));
            dlg.setPositiveButton(getString(R.string.aceptar), null).show();
        }else{
            webServiceRegistro();
        }
    }

    private void webServiceRegistro(){
        final EditText edtxt_name = findViewById(R.id.edtxt_nombre);
        final EditText edtxt_email = findViewById(R.id.edtxt_email);
        final EditText edtxt_pass = findViewById(R.id.edtxt_pass);

        final JSONRegistro jsonRegistro=new JSONRegistro(this);
        final Usuario usuario=new Usuario();

        //Campos obligatorios
        usuario.setPassword(edtxt_pass.getText().toString());
        usuario.setEmail(edtxt_email.getText().toString());
        usuario.setNombre(edtxt_name.getText().toString());

        progressBar.setVisibility(View.VISIBLE);
        lyDisabled.setVisibility(View.VISIBLE);

        tRegistro = new Thread() {
            @Override
            public void run()
            {
               try {
                    resultadoRegistro =jsonRegistro.run(usuario);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerRegistro.post(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        lyDisabled.setVisibility(View.GONE);
                        switch(resultadoRegistro)
                        {
                            case -1://El usuario ya existe
                            {
                                AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                                dlg.setMessage("No ha sido posible crear tu cuenta de usuario.\n\nEl email ya esta en uso.\n\nPor favor, revisa los datos y vuelve a intentarlo.");
                                dlg.setPositiveButton("Aceptar", null);
                                dlg.show();

                                break;
                            }
                            case 1:// OK
                            {
                                app.setUsuario(usuario);
                                app.guardarEnPreferencias(activity);

                                Intent i = new Intent(ActivityRegistro.this, ActivityLogin.class);
                                startActivity(i);
                                Bungee.fade(activity);
                                finish();
                                break;
                            }
                            default://Cualquier otro error
                            {
                                AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                                dlg.setTitle("Error en el registro");
                                dlg.setMessage("No ha sido posible crear su cuenta de usuario.\n\nSe ha producido un error desconocido, por favor vuelta a intentarlo.");
                                dlg.setPositiveButton("Aceptar", null);
                                dlg.show();

                                break;
                            }
                        }




                    }
                });
            }
        };

        handlerRegistro = new Handler();
        tRegistro.start(