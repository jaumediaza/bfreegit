package com.atendus.app.login;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.atendus.app.R;
import com.atendus.app.clases.Aplicacion;
import com.atendus.app.clases.Login;
import com.atendus.app.clases.ResultadoLogin;
import com.atendus.app.json.JSONEnviarTokenPush;
import com.atendus.app.json.JSONLogin;
import com.atendus.app.json.JSONRecuperarPassword;
import com.atendus.app.json.JSONVincularDispositivo;
import com.atendus.app.splash_inicio.ActivityInicio;
import com.atendus.app.utilidades.TresAndroides;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONException;

import spencerstudios.com.bungeelib.Bungee;


public class ActivityLogin extends AppCompatActivity {

    private Thread tLogin;
    private Handler handlerLogin;
    ResultadoLogin resultadoLogin = new ResultadoLogin();
    private Activity activity;
    private Aplicacion app = new Aplicacion();
    private ProgressBar progressBar;
    private LinearLayout lyDisabled;

    private ImageView cmdOlvidePassword;
    private Thread tRecuperarPassword;
    private Handler handlerRecuperarPassword;

    private EditText edtxt_usuario, edtxt_pass;
    private TextView textView, textView2, textView3;
    private Button btn_entrar;

    Typeface Roboto_Medium = null;
    Typeface Roboto_Light = null;


    int resultadoRecuperarPassowrd = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        activity = this;
        app.cargarAplicacionDePreferencias(this);

        Roboto_Medium = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Medium.ttf");
        Roboto_Light = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");

        cmdOlvidePassword = this.findViewById(R.id.cmdOlvidePassword);
        cmdOlvidePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                recuperarPassword();
            }
        });

        edtxt_usuario = this.findViewById(R.id.edtxt_usuario);
        edtxt_pass = this.findViewById(R.id.edtxt_pass);
        textView = this.findViewById(R.id.txt_titulo);
        textView2 = this.findViewById(R.id.textView2);
        textView3 = this.findViewById(R.id.textView3);
        btn_entrar = this.findViewById(R.id.btn_entrar);

        /*edtxt_usuario.setTypeface(Roboto_Light);
        edtxt_pass.setTypeface(Roboto_Light);
        textView.setTypeface(Roboto_Medium);
        textView2.setTypeface(Roboto_Light);
        textView3.setTypeface(Roboto_Medium);
        btn_entrar.setTypeface(Roboto_Medium);*/


        findViewById(R.id.arr_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityLogin.this.onBackPressed();
            }
        });

        findViewById(R.id.done).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarLogIn();
            }
        });

        findViewById(R.id.btn_entrar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                comprobarLogIn();
            }
        });

        progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        lyDisabled = findViewById(R.id.ly_disabled);
        lyDisabled.setVisibility(View.GONE);

        findViewById(R.id.ly_registro).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ActivityLogin.this, ActivityRegistro.class);
                startActivity(intent);
                Bungee.fade(ActivityLogin.this);
                finish();
            }
        });

    }

    private void recuperarPassword() {
        final EditText edtxt_usuario = findViewById(R.id.edtxt_usuario);
        final EditText edtxt_pass = findViewById(R.id.edtxt_pass);
        String usuario = edtxt_usuario.getText().toString();
        String pass = edtxt_pass.getText().toString();

        if (usuario.length() <= 0) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.error_no_email_recuperar_password));
            dlg.setPositiveButton(getString(R.string.aceptar), null);
            dlg.show();
        } else if (!TresAndroides.validateEmail(usuario)) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.error_no_email_recuperar_password2));
            dlg.setPositiveButton(getString(R.string.aceptar), null);
            dlg.show();
        } else {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.pregunta_recuperar_password));
            dlg.setPositiveButton("Si", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    webserviceRecuperarPassword();
                }
            });
            dlg.setNegativeButton("No", null);
            dlg.show();

        }
    }

    private void webserviceRecuperarPassword() {
        final EditText edtxt_usuario = findViewById(R.id.edtxt_usuario);

        final JSONRecuperarPassword jsonRecuperarPassword = new JSONRecuperarPassword(this);

        progressBar.setVisibility(View.VISIBLE);
        lyDisabled.setVisibility(View.VISIBLE);


        tRecuperarPassword = new Thread() {
            @Override
            public void run() {
                try {
                    resultadoRecuperarPassowrd = jsonRecuperarPassword.run(edtxt_usuario.getText().toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerRecuperarPassword.post(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        lyDisabled.setVisibility(View.GONE);

                        if (resultadoRecuperarPassowrd == 0) {
                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage(getString(R.string.no_usuario_email));
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();
                        }
                        if (resultadoRecuperarPassowrd == 1) {
                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage(getString(R.string.aviso_emaill_cambio_password));
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();

                        }


                    }
                });
            }
        };

        handlerRecuperarPassword = new Handler();
        tRecuperarPassword.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        TresAndroides.hideKeyboard(ActivityLogin.this);
        Intent i = new Intent(ActivityLogin.this, ActivityInicio.class);
        startActivity(i);
        Bungee.fade(activity);
        finish();
    }

    private void comprobarLogIn() {
        final EditText edtxt_usuario = findViewById(R.id.edtxt_usuario);
        final EditText edtxt_pass = findViewById(R.id.edtxt_pass);
        String usuario = edtxt_usuario.getText().toString();
        String pass = edtxt_pass.getText().toString();

        if (usuario.length() <= 0) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.error_no_email_intro));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_usuario.requestFocus();
                    TresAndroides.showKeyboard(ActivityLogin.this);
                }
            });
            dlg.show();
        } else if (pass.length() <= 0) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.error_no_pass_intro2));
            dlg.setPositiveButton(getString(R.string.aceptar), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    edtxt_pass.requestFocus();
                    TresAndroides.showKeyboard(ActivityLogin.this);
                }
            });
            dlg.show();
        } else if (!TresAndroides.isOnline(ActivityLogin.this)) {
            AlertDialog.Builder dlg = new AlertDialog.Builder(ActivityLogin.this);
            dlg.setMessage(getString(R.string.error_no_internet));
            dlg.setPositiveButton(getString(R.string.aceptar), null).show();
        } else {
            webserviceLogin();
        }
    }

    private void webserviceLogin() {

        final EditText edtxt_usuario = findViewById(R.id.edtxt_usuario);
        final EditText edtxt_pass = findViewById(R.id.edtxt_pass);


        final JSONLogin jsonLogin = new JSONLogin(this);
        final Login login = new Login();

        //Campos obligatorios
        login.setPassword(edtxt_pass.getText().toString());
        login.setEmail(edtxt_usuario.getText().toString());

        progressBar.setVisibility(View.VISIBLE);
        lyDisabled.setVisibility(View.VISIBLE);

        tLogin = new Thread() {
            @Override
            public void run() {
                try {
                    resultadoLogin = jsonLogin.run(login);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                handlerLogin.post(new Runnable() {
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        lyDisabled.setVisibility(View.GONE);

                        if (resultadoLogin.getResultado() == 0) {
                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage("Error usuario y contrase\u00f1a no v\u00e1lidos.");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();
                        }else if (resultadoLogin.getResultado() == 1) {
                            app.setUsuario(resultadoLogin.getUsuario());
                            app.getUsuario().setPassword(login.getPassword());
                            app.setUsuarioConectado(true);
                            app.guardarEnPreferencias(activity);


                            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                            final String device_name = TresAndroides.getDeviceName();
                            if (ActivityCompat.checkSelfPermission(ActivityLogin.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                Intent i = new Intent(ActivityLogin.this, ActivityBienvenida.class);
                                i.putExtra("Registro", 1);
                                startActivity(i);
                                Bungee.fade(activity);
                                finish();
                                return;
                            }
                            final String device_id = telephonyManager.getDeviceId();



                            final Handler handler = new Handler();

                            new Thread(){
                                @Override
                                public void run() {
                                    super.run();
                                    JSONVincularDispositivo jsonVincularDispositivo = new JSONVincularDispositivo(ActivityLogin.this);
                                    int res = -1;
                                    try {
                                        res = jsonVincularDispositivo.run(app.getUsuario().getId(), device_id, device_name);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                    final int fRes = res;

                                    JSONEnviarTokenPush jsonEnviarToken=new JSONEnviarTokenPush(ActivityLogin.this);
                                    try {
                                        if (ActivityCompat.checkSelfPermission(ActivityLogin.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                                            return;
                                        }
                                        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                                        final String iddisp = telephonyManager.getDeviceId();
                                        String token = FirebaseInstanceId.getInstance().getToken();
                                        jsonEnviarToken.run(app.getUsuario().getId(),token, "", iddisp);
                                        app.getUsuario().setToken(token);
                                        app.guardarEnPreferencias(ActivityLogin.this);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }.start();


                            Intent i = new Intent(ActivityLogin.this, ActivityBienvenida.class);
                            i.putExtra("Registro", 1);
                            startActivity(i);
                            Bungee.fade(activity);
                            finish();

                        }else if (resultadoLogin.getResultado() == -1){
                            AlertDialog.Builder dlg = new AlertDialog.Builder(activity);
                            dlg.setMessage("Error, el usuario no ha sido activado, revise su bandeja de correo y spam");
                            dlg.setPositiveButton("Aceptar", null);
                            dlg.show();
                        }




                    }
                });
            }
        };

        handlerLogin = new Handler();
        tLogin.st