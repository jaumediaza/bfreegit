package com.atendus.app.json;

import android.app.Activity;

import com.atendus.app.R;

import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by Usuario on 28/09/2017.
 */

public class JSONRecuperarPassword {
    private Activity activity;
    private JSONObject jObject;

    int resultado;//0. El usuario ya existe.  / //1. Ok

    public JSONRecuperarPassword(Activity a){
        activity = a;
    }

    public int run(final String email) throws JSONException {

        return loadJSON(email);
    }


    private int loadJSON(String email) throws JSONException{


        String ruta="authentication/recover_pasword.php?mail="+email;

        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }
    }


    private int parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode");


    }


