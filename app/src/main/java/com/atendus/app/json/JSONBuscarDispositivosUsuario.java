package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.DispositivoBasic;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONBuscarDispositivosUsuario {
    private Context activity;
    private JSONObject jObject;



    public JSONBuscarDispositivosUsuario(Context a){
        activity = a;
    }

    public List<DispositivoBasic> run(String email, int idUsuControlador) throws JSONException {

        return loadJSON(email, idUsuControlador);
    }


    private List<DispositivoBasic> loadJSON(String email, int idUsuControlador) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"link/query_user_devices.php?email="+email+
                "&id_controller="+idUsuControlador);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private List<DispositivoBasic> parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("devices");

        List <DispositivoBasic> dispositivosControlados = new ArrayList<>();
        int i = 0;
        while (jArray.length() > i) {
            DispositivoBasic dispositivo = new DispositivoBasic();


            if (!jArray.getJSONObject(i).isNull("id"))
                dispositivo.setId(jArray.getJSONObject(i).getInt("id"));

            if (!jArray.getJSONObject(i).isNull("device_id"))
                dispositivo.setIdDispositivo(jArray.getJSONObject(i).getString("device_id"));

            if (!jArray.getJSONObject(i).isNull("device_label"))
                dispositivo.setNombre(jArray.getJSONObject(i).getString("device_label"));

            dispositivosControlados.add(dispositivo);
            i++;
        }

        return dispositivosControlados;

    }