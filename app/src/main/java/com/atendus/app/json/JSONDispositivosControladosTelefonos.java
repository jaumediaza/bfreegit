package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.Telefono;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDispositivosControladosTelefonos {
    private Context activity;
    private JSONObject jObject;



    public JSONDispositivosControladosTelefonos(Context a){
        activity = a;
    }

    public List<Telefono> run(final int id_usuario, int id_dispositivo) throws JSONException {

        return loadJSON(id_usuario, id_dispositivo);
    }


    private List<Telefono> loadJSON(int id_usuario, int id_dispositivo) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"userdata/query_phone_list.php?id="+id_usuario+"&device_id="+
                id_dispositivo);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private List<Telefono> parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("phoneList");

        List <Telefono> tfnos = new ArrayList<>();
        int i = 0;
        while (jArray.length() > i) {
            Telefono tfno = new Telefono();


            if (!jArray.getJSONObject(i).isNull("id"))
                tfno.setId(jArray.getJSONObject(i).getInt("id"));

            try {
                if (!jArray.getJSONObject(i).isNull("name"))
                    tfno.setNombre(AESCrypt.decrypt(jArray.getJSONObject(i).getString("name")));

                if (!jArray.getJSONObject(i).isNull("phone"))
                    tfno.setNumero(AESCrypt.decrypt(jArray.getJSONObject(i).getString("phone")));

                tfno.setLetra(tfno.getNombre().charAt(0));
            }
            catch(Exception e){

            }
            tfno.setPermitido(false);
            tfno.setLetra(tfno.getNombre().charAt(0));

            tfnos.add(tfno);
            i++;
        }
        Collections.reverse(tfnos);
        return tfnos;

    }

}
