package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONEditarUsuario {
    private Context activity;
    private JSONObject jObject;

    int resultado;//0. El usuario ya existe.  / //1. Ok

    public JSONEditarUsuario(Context a){
        activity = a;
    }

    public int run(final Usuario usuario) throws JSONException {

        return loadJSON(usuario);
    }


    private int loadJSON(Usuario usuario) throws JSONException{
        String nombre="",codigo_pin="",password="";

        try {
            nombre= URLEncoder.encode(usuario.getNombre(), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if(usuario.getPassword().length()>0)
        {
            password=usuario.getPassword();
        }
        if(usuario.getCodigo_pin().length()>0)
        {
            codigo_pin=usuario.getCodigo_pin();
        }
        else
        {
            codigo_pin="3ANDROIDES-BORRAR";
        }

        String ruta="userdata/edit_user.php?id="+usuario.getId()+"mail="+usuario.getEmail()+"&name="+nombre+"&password"+password+"pinCode"+codigo_pin;

        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }
    }


    private int parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode");


    