package com.bfree.app.json;


import android.app.Activity;

import com.bfree.app.R;
import com.bfree.app.clases.Login;
import com.bfree.app.clases.ResultadoLogin;
import com.bfree.app.clases.Usuario;
import com.bfree.app.json.JSONManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class JSONLogin {
	private Activity activity;
	private JSONObject jObject;
	private ResultadoLogin resultadoLogin = new ResultadoLogin();

	private Usuario usr = new Usuario();


	public JSONLogin(Activity a) {
		activity = a;
	}

	public ResultadoLogin run(final Login login) throws JSONException {

		return loadJSON(login);
	}


	private ResultadoLogin loadJSON(Login login) throws JSONException {
		jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
				+ "/authentication/login.php?email=" + login.getEmail() + "&password=" + login.getPassword());
		if (jObject != null) {
			return parseJSON(jObject);
		} else {
			return resultadoLogin;
		}

	}


	private ResultadoLogin parseJSON(JSONObject resultadoJSON) throws JSONException {
		resultadoLogin.setResultado(resultadoJSON.getInt("resultCode"));

		if (resultadoJSON.getInt("resultCode") == 0) {
			return resultadoLogin;
		} else {
			JSONObject jsonUser = resultadoJSON.getJSONObject("user");

			if (!jsonUser.isNull("id"))
				resultadoLogin.getUsuario().setId(jsonUser.getInt("id"));

			if (!jsonUser.isNull("name"))
				resultadoLogin.getUsuario().setNombre(jsonUser.getString("name"));

			if (!jsonUser.isNull("mail"))
				resultadoLogin.getUsuario().setEmail(jsonUser.getString("mail"));

			if (!jsonUser.isNull("pinCode"))
				resultadoLogin.getUsuario().setCodigo_pin(jsonUser.getString("pinCode"));

			if (!jsonUser.isNull("sexohijo"))
				resultadoLogin.getUsuario().setSexo(jsonUser.getInt("sexohijo"));

			if (!jsonUser.isNull("nombrehijo"))
				resultadoLogin.getUsuario().setNombrehijo(jsonUser.getString("nombrehijo"));

			if (!jsonUser.isNull("edadhijo"))
				resultadoLogin.getUsuario().setEdadhijo(jsonUser.getInt("edadhijo"));
			if (!jsonUser.isNull("isPadre"))
				resultadoLogin.getUsuario().setIsPadre(jsonUser.getInt("isPadre"));
			if (!jsonUser.isNull("plan"))
				resultadoLogin.getUsuario().setPlan(jsonUser.getInt("plan"));
		}

		return resultadoLogin;

	}


}
