package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONModificarVinculacion {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONModificarVinculacion(Context c){
        context = c;
    }

    public boolean run(int id,int estado) throws JSONException {

        return loadJSON(id, estado);
    }


    private boolean loadJSON(int id,int estado) throws JSONException{

        String ruta="link/update_link.php?link_id="+id+"&status="+estado;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
