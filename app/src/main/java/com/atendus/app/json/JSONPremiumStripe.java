package com.bfree.app.json;


import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class JSONPremiumStripe {
	private Context activity;
	private JSONObject jObject;
	int resultado;

	private Usuario usr=new Usuario();


	public JSONPremiumStripe(Context a){
		activity = a;
	}

	public String run(int plan) throws JSONException{

		return loadJSON(plan);
	}


	private String loadJSON(int plan) throws JSONException{


		String ruta = activity.getResources().getString(R.string.url_webservices)+"stripe/stripetest.php?premium="+plan;

		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return "";
		}

	}


	private String parseJSON(JSONObject resultadoJSON) throws JSONException{
		String res = "";
		if(!resultadoJSON.isNull("secretKey")){
			res = resultadoJSON.getString("secretKey");
		}


		return res;

	}



}
