package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.DesconexionProgramada;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;



/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDesconexionesProgramadas {
    private Context activity;
    private JSONObject jObject;



    public JSONDesconexionesProgramadas(Context a){
        activity = a;
    }

    public DesconexionProgramada.DesconexionProgramadaList run(final int id_usuario, String imei) throws JSONException {

        return loadJSON(id_usuario, imei);
    }


    private DesconexionProgramada.DesconexionProgramadaList loadJSON(int id_usuario, String imei) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"disconnections/scheduled/query_list.php?id="+id_usuario+"&imei="+imei);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private DesconexionProgramada.DesconexionProgramadaList parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("disconnection");

        DesconexionProgramada.DesconexionProgramadaList desc = new DesconexionProgramada.DesconexionProgramadaList();
        int i = 0;
        while (jArray.length() > i) {
            DesconexionProgramada resultado = new DesconexionProgramada();


            if (!jArray.getJSONObject(i).isNull("id"))
                resultado.setId(jArray.getJSONObject(i).getInt("id"));

            resultado.setBloquearNotificaciones(false);
            if (!jArray.getJSONObject(i).isNull("notif_allowed"))
                if(jArray.getJSONObject(i).getInt("notif_allowed") == 1)
                    resultado.setBloquearNotificaciones(true);

            resultado.setBloquearLlamadas(false);
            if (!jArray.getJSONObject(i).isNull("calls_allowed"))
                if(jArray.getJSONObject(i).getInt("calls_allowed") == 1)
                    resultado.setBloquearLlamadas(true);

            if (!jArray.getJSONObject(i).isNull("hh_init"))
                resultado.setHoraInicio(jArray.getJSONObject(i).getInt("hh_init"));

            if (!jArray.getJSONObject(i).isNull("mm_init"))
                resultado.setMinutoInicio(jArray.getJSONObject(i).getInt("mm_init"));

            if (!jArray.getJSONObject(i).isNull("hh_end"))
                resultado.setHoraFin(jArray.getJSONObject(i).getInt("hh_end"));

            if (!jArray.getJSONObject(i).isNull("mm_end"))
                resultado.setMinutoFin(jArray.getJSONObject(i).getInt("mm_end"));

            resultado.setEnabled(false);
            if (!jArray.getJSONObject(i).isNull("enabled"))
                if(jArray.getJSONObject(i).getInt("enabled") == 1)
                    resultado.setEnabled(true);

            if (!jArray.getJSONObject(i).isNull("title"))
                resultado.setStrTitulo(jArray.getJSONObject(i).getString("title"));

            if (!jArray.getJSONObject(i).isNull("repeat"))
                resultado.setRepetir(jArray.getJSONObject(i).getInt("repeat")+1);


            boolean dias[] = {false, false, false, false, false, false, false};

            if (!jArray.getJSONObject(i).isNull("monday"))
                if(jArray.getJSONObject(i).getInt("monday") == 1)
                    dias[0] = true;

            if (!jArray.getJSONObject(i).isNull("tuesday"))
                if(jArray.getJSONObject(i).getInt("tuesday") == 1)
                    dias[1] = true;

            if (!jArray.getJSONObject(i).isNull("wednesday"))
                if(jArray.getJSONObject(i).getInt("wednesday") == 1)
                    dias[2] = true;

            if (!jArray.getJSONObject(i).isNull("thursday"))
                if(jArray.getJSONObject(i).getInt("thursday") == 1)
                    dias[3] = true;

            if (!jArray.getJSONObject(i).isNull("friday"))
                if(jArray.getJSONObject(i).getInt("friday") == 1)
                    dias[4] = true;

            if (!jArray.getJSONObject(i).isNull("saturday"))
                if(jArray.getJSONObject(i).getInt("saturday") == 1)
                    dias[5] = true;

            if (!jArray.getJSONObject(i).isNull("sunday"))
                if(jArray.getJSONObject(i).getInt("sunday") == 1)
                    dias[6] = true;

            if (!jArray.getJSONObject(i).isNull("editable"))
                if(jArray.getJSONObject(i).getInt("editable") == 0)
                    resultado.setEditable(false);

                resultado.setDias(dias);


            if (!jArray.getJSONObject(i).isNull("pckg_allowed"))
                resultado.setAplicacionesPermitidas(jArray.getJSONObject(i).getString("pckg_allowed"));

            if (!jArray.getJSONObject(i).isNull("phones_allowed"))
                resultado.setTfnosPermitidos(jArray.getJSONObject(i).getString("phones_allowed"));

            desc.getList().add(resultado);
            i++;
        }

        return desc;

    }

}
