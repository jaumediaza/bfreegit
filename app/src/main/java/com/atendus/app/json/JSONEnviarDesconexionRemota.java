package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.AplicacionPermitida;
import com.bfree.app.clases.DispositivosControlados;
import com.bfree.app.clases.Telefono;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.List;


public class JSONEnviarDesconexionRemota {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONEnviarDesconexionRemota(Context c){
        context = c;
    }

    public boolean run(DispositivosControlados dispositivo, int idControlador,
                       List<AplicacionPermitida> apps, List<Telefono> tfnos) throws JSONException {

        return loadJSON(dispositivo, idControlador, apps, tfnos);
    }


    private boolean loadJSON(DispositivosControlados dispositivo, int idControlador,
                             List<AplicacionPermitida> apps, List<Telefono> tfnos) throws JSONException{

        int notif = 1;
        if (dispositivo.isUltima_desconexion_bloquear_apps())
            notif = 0;

        int llamad = 1;
        if (dispositivo.isUltima_desconexion_bloquear_llamadas())
            llamad = 0;
        dispositivo.setUltima_desconexion_lista_apps("");
        for (AplicacionPermitida app : apps){
            if(app.isPermitida()){
                try {
                    dispositivo.setUltima_desconexion_lista_apps(
                            dispositivo.getUltima_desconexion_lista_apps().concat(
                                    URLEncoder.encode(app.getPackageName(), "UTF-8")  + "/")
                    );
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }
        dispositivo.setUltima_desconexion_lista_contactos("");
        for (Telefono tfno : tfnos){
            if (tfno.isPermitido()){
                try {
                    dispositivo.setUltima_desconexion_lista_contactos(
                            dispositivo.getUltima_desconexion_lista_contactos().concat(
                                    URLEncoder.encode(tfno.getNumero(), "UTF-8") + "/"
                            ));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
            }
        }

        int notif2 = 0;
        int llamad2 = 0;
        if (notif == 0)
            notif2 = 1;
        if (llamad == 0)
            llamad2 = 1;


        String ruta2="disconnections/send_status.php?" +
                "id="+dispositivo.getId_usuario()+
                "&device_id="+dispositivo.getId_dispositivo()+
                "&hh="+dispositivo.getUltima_desconexion_hh()+
                "&mm="+dispositivo.getUltima_desconexion_mm()+
                "&block_apps="+notif2+
                "&block_calls="+llamad2+
                "&phone_list="+dispositivo.getUltima_desconexion_lista_contactos()+
                "&app_list="+dispositivo.getUltima_desconexion_lista_apps()+
                "&controller_id="+idControlador;

        JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta2);

        String ruta="programar_desconexion.php?" +
                "id_usuario="+dispositivo.getId_usuario()+
                "&id_dispositivo="+dispositivo.getId_dispositivo()+
                "&horas="+dispositivo.getUltima_desconexion_hh()+
                "&minutos="+dispositivo.getUltima_desconexion_mm()+
                "&notif_permitidas="+notif+
                "&llamadas_permitidas="+llamad+
                "&lista_fnos="+dispositivo.getUltima_desconexion_lista_contactos()+
                "&lista_apps_permitidas="+dispositivo.getUltima_desconexion_lista_apps()+
                "&id_usuario_controlador="+idControlador;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
