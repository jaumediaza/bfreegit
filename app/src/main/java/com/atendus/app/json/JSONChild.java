package com.bfree.app.json;


import android.app.Activity;
import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.Login;
import com.bfree.app.clases.ResultadoLogin;
import com.bfree.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


public class JSONChild {
	private Context activity;
	private JSONObject jObject;
	int resultado;

	private Usuario usr=new Usuario();


	public JSONChild(Context a){
		activity = a;
	}

	public int run(int id_usuario, int isPadre, String nombrehijo, int sexohijo, int edadhijo) throws JSONException{

		return loadJSON(id_usuario, isPadre, nombrehijo, sexohijo, edadhijo);
	}


	private int loadJSON(int id_usuario, int isPadre, String nombrehijo, int sexohijo, int edadhijo) throws JSONException{

		try {
			if(nombrehijo!=null)
			nombrehijo = URLEncoder.encode(nombrehijo, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}

		String ruta = activity.getResources().getString(R.string.url_webservices)
				+"userdata/edit_child.php?id="+id_usuario+"&isPadre="+isPadre+"&nombrehijo="+nombrehijo+"&sexohijo="+sexohijo+"&edadhijo="+edadhijo;

		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return -1;
		}

	}


	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
