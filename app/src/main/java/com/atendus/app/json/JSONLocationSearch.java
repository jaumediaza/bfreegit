package com.bfree.app.json;


import android.content.Context;
import android.location.Location;

import com.bfree.app.R;
import com.bfree.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONLocationSearch {
	private Context activity;
	private JSONObject jObject;
	int resultado;


	public JSONLocationSearch(Context a){
		activity = a;
	}

	public Location run(int id_usuario) throws JSONException{

		return loadJSON(id_usuario);
	}


	private Location loadJSON(int id_usuario) throws JSONException{



		String ruta = activity.getResources().getString(R.string.url_webservices)
				+"userdata/query_location.php?id="+id_usuario;

		jObject = JSONManager.getJSONfromURL(ruta);
		if(jObject != null){
			return parseJSON(jObject);
		}
		else
		{
			return null;
		}

	}


	private Location parseJSON(JSONObject resultadoJSON) throws JSONException{
		Location loc = null;
		if(!resultadoJSON.isNull("latitud")){
			loc = new Location("");
			loc.setLatitude(resultadoJSON.getLong("latitud"));
			loc.setLongitude(resultadoJSON.getLong("longitud"));
		}


		return loc;

	}



}
