package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONComprobarVinculacionesDisponibles {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONComprobarVinculacionesDisponibles(Context c){
        context = c;
    }

    public boolean run(final int id_usuario) throws JSONException {

        return loadJSON(id_usuario);
    }


    private boolean loadJSON(int id_usuario) throws JSONException{

        String ruta="link/query_vinculation_limit.php?id="+id_usuario;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") > 0;

    }



}
