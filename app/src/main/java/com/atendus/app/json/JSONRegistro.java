package com.bfree.app.json;

import android.app.Activity;

import com.bfree.app.R;
import com.bfree.app.clases.Usuario;
import com.bfree.app.json.JSONManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;


/**
 * Created by Usuario on 28/09/2017.
 */

public class JSONRegistro {
    private Activity activity;
    private JSONObject jObject;

    int resultado;//-1 -> Correo en uso 0 -> Error 1-> Registro satisfactorio

    public JSONRegistro(Activity a){
        activity = a;
    }

    public int run(final Usuario usuario) throws JSONException {

        return loadJSON(usuario);
    }


    private int loadJSON(Usuario usuario) throws JSONException{

        String ruta="authentication/register.php?name="+usuario.getNombre()+"&password="+usuario.getPassword()+"&mail="+usuario.getEmail();

        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }
    }


    private int parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode");
    }



}
