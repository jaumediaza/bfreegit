package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONBorrarDesconexionProgramadaRemota {
    private Context context;
    private JSONObject jObject;

    boolean resultado = false;

    public JSONBorrarDesconexionProgramadaRemota(Context c){
        context = c;
    }

    public boolean run(final int id_usuario, int id_dispositivo, int id_desconexion) throws JSONException {

        return loadJSON(id_usuario, id_dispositivo, id_desconexion);
    }


    private boolean loadJSON(int id_usuario, int id_dispositivo, int id_desconexion) throws JSONException{

        String ruta="disconnections/scheduled/delete_device_disconnection.php?id="+id_usuario+"&device_id="
                +id_dispositivo+"&id_desconexion="+id_desconexion;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
