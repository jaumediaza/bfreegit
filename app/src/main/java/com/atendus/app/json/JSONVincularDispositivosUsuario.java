package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONVincularDispositivosUsuario {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONVincularDispositivosUsuario(Context c){
        context = c;
    }

    public boolean run(int idControlador, int idUsuario, String idDispositivos) throws JSONException {

        return loadJSON(idControlador, idUsuario, idDispositivos);
    }


    private boolean loadJSON(int idControlador, int idUsuario, String idDispositivos) throws JSONException{

        String ruta="link/vincular_dispositivos_usuario.php?id_usuario_controlador="+idControlador+"&id_usuario="
                +idUsuario+"&id_dispositivos="+idDispositivos;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }



    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultado") != 0;

    }



}
