package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.EstadisticaAplicacion;
import com.bfree.app.clases.EstadisticasUsuarioRemoto;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDispositivosControladosEstadisticas {
    private Context activity;
    private JSONObject jObject;



    public JSONDispositivosControladosEstadisticas(Context a){
        activity = a;
    }

    public EstadisticasUsuarioRemoto run(final int id_usuario, int id_dispositivo) throws JSONException {

        return loadJSON(id_usuario, id_dispositivo);
    }


    private EstadisticasUsuarioRemoto loadJSON(int id_usuario, int id_dispositivo) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"userstats/query_usage_statistics.php?id="+id_usuario+"&device_id="+
                id_dispositivo);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private EstadisticasUsuarioRemoto parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        EstadisticasUsuarioRemoto estadisticasUsuarioRemoto = new EstadisticasUsuarioRemoto();

        JSONArray jArrayDia = null;
        if(!resultadoJSON.isNull("statistics_day")){
            jArrayDia = resultadoJSON.getJSONArray("statistics_day");
        }
        JSONArray jArraySemana = null;
        if(!resultadoJSON.isNull("statistics_week")){
            jArraySemana = resultadoJSON.getJSONArray("statistics_week");
        }
        JSONArray jArrayMes = null;
        if(!resultadoJSON.isNull("statistics_month")){
            jArrayMes = resultadoJSON.getJSONArray("statistics_month");
        }

        if(!resultadoJSON.isNull("unlocks_day")) {
            estadisticasUsuarioRemoto.setDesbloqueosDia(resultadoJSON.getInt("unlocks_day"));
        }
        if(!resultadoJSON.isNull("unlocks_week")) {
            estadisticasUsuarioRemoto.setDesbloqueosSemana(resultadoJSON.getInt("unlocks_week"));
        }
        if(!resultadoJSON.isNull("unlocks_month")) {
            estadisticasUsuarioRemoto.setDesbloqueosMes(resultadoJSON.getInt("unlocks_month"));
        }
        if(!resultadoJSON.isNull("usage_day")) {
            estadisticasUsuarioRemoto.setTiempoUsoDia(resultadoJSON.getLong("usage_day"));
        }
        if(!resultadoJSON.isNull("usage_week")) {
            estadisticasUsuarioRemoto.setTiempoUsoSemana(resultadoJSON.getLong("usage_week"));
        }
        if(!resultadoJSON.isNull("usage_month")) {
            estadisticasUsuarioRemoto.setTiempoUsoMes(resultadoJSON.getLong("usage_month"));
        }

        estadisticasUsuarioRemoto.setEstadisticasDia(new ArrayList<EstadisticaAplicacion>());

        int i = 0;
        if(jArrayDia!=null) {
            while (jArrayDia.length() > i) {
                EstadisticaAplicacion est = new EstadisticaAplicacion();

                try {
                    if (!jArrayDia.getJSONObject(i).isNull("label"))
                        est.setNombreApp(AESCrypt.decrypt(jArrayDia.getJSONObject(i).getString("label")));

                    if (!jArrayDia.getJSONObject(i).isNull("usage_percent"))
                        est.setPorcentajeUso(jArrayDia.getJSONObject(i).getDouble("usage_percent"));

                    if (!jArrayDia.getJSONObject(i).isNull("color"))
                        est.setColor(jArrayDia.getJSONObject(i).getInt("color"));
                } catch (Exception e) {

                }

                estadisticasUsuarioRemoto.getEstadisticasDia().add(est);
                i++;
            }
        }

        estadisticasUsuarioRemoto.setEstadisticasSemana(new ArrayList<EstadisticaAplicacion>());

        i = 0;
        if(jArraySemana!=null) {
            while (jArraySemana.length() > i) {
                EstadisticaAplicacion est = new EstadisticaAplicacion();


                if (!jArraySemana.getJSONObject(i).isNull("label"))
                    est.setNombreApp(jArraySemana.getJSONObject(i).getString("label"));

                if (!jArraySemana.getJSONObject(i).isNull("usage_percent"))
                    est.setPorcentajeUso(jArraySemana.getJSONObject(i).getDouble("usage_percent"));

                if (!jArraySemana.getJSONObject(i).isNull("color"))
                    est.setColor(jArraySemana.getJSONObject(i).getInt("color"));


                estadisticasUsuarioRemoto.getEstadisticasSemana().add(est);
                i++;
            }
        }

        estadisticasUsuarioRemoto.setEstadisticasMes(new ArrayList<EstadisticaAplicacion>());

        i = 0;
        if(jArrayMes!=null) {
            while (jArrayMes.length() > i) {
                EstadisticaAplicacion est = new EstadisticaAplicacion();


                if (!jArrayMes.getJSONObject(i).isNull("label"))
                    est.setNombreApp(jArrayMes.getJSONObject(i).getString("label"));

                if (!jArrayMes.getJSONObject(i).isNull("usage_percent"))
                    est.setPorcentajeUso(jArrayMes.getJSONObject(i).getDouble("usage_percent"));

                if (!jArrayMes.getJSONObject(i).isNull("color"))
                    est.setColor(jArrayMes.getJSONObject(i).getInt("color"));


                estadisticasUsuarioRemoto.getEstadisticasMes().add(est);
                i++;
            }
        }

        return estadisticasUsuarioRemoto;

    }

}
