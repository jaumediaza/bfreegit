package com.bfree.app.json;



import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;



public class JSONAddApp {
	private Context activity;
	private JSONObject jObject;

	private Usuario usr=new Usuario();


	public JSONAddApp(Context a){
		activity = a;
	}
	
	public int run(int id_usuario, String id_dispositivo, String app_label, String app_package) throws JSONException{

		return loadJSON(id_usuario, id_dispositivo, app_label, app_package);
	}
		
		
	private int loadJSON(int id_usuario, String id_dispositivo, String app_label, String app_package) throws JSONException{

        try {
            id_dispositivo = URLEncoder.encode(id_dispositivo, "UTF-8");
			String ruta = activity.getResources().getString(R.string.url_webservices)
					+"userdata/update_applist.php?id="+id_usuario+"&device_id="+id_dispositivo+"&app_label="+app_label+"&app_package="+app_package;
			jObject = JSONManager.getJSONfromURL(ruta);
			if(jObject != null){
				return parseJSON(jObject);
			}
			else
			{
				return -1;
			}
        } catch (Exception e) {
            e.printStackTrace();
			return -1;
        }





	}
		

	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
