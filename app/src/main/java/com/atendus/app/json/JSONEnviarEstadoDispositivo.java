package com.bfree.app.json;

import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.json.JSONManager;

import org.json.JSONException;
import org.json.JSONObject;


public class JSONEnviarEstadoDispositivo {
    private Context context;
    private JSONObject jObject;

    boolean resultado;

    public JSONEnviarEstadoDispositivo(Context c){
        context = c;
    }

    public boolean run(final int id_usuario, String imei, int estado) throws JSONException {

        return loadJSON(id_usuario, imei, estado);
    }


    private boolean loadJSON(int id_usuario, String imei, int estado) throws JSONException{

        String ruta="disconnections/send_status.php?id="+id_usuario+"&imei="+imei+"&status="+estado;

        jObject = JSONManager.getJSONfromURL(context.getResources().getString(R.string.url_webservices)
                +ruta);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return resultado;
        }

    }


    private boolean parseJSON(JSONObject resultadoJSON) throws JSONException{
        return resultadoJSON.getInt("resultCode") != 0;

    }



}
