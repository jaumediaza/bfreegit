package com.atendus.app.json;

import android.content.Context;

import com.atendus.app.R;
import com.atendus.app.clases.AplicacionPermitida;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Alberto on 13/06/2018.
 */

public class JSONDispositivosControladosApps {
    private Context activity;
    private JSONObject jObject;



    public JSONDispositivosControladosApps(Context a){
        activity = a;
    }

    public List<AplicacionPermitida> run(final int id_usuario, int id_dispositivo) throws JSONException {

        return loadJSON(id_usuario, id_dispositivo);
    }


    private List<AplicacionPermitida> loadJSON(int id_usuario, int id_dispositivo) throws JSONException{
        jObject = JSONManager.getJSONfromURL(activity.getResources().getString(R.string.url_webservices)
                +"userdata/query_applist.php?id="+id_usuario+"&device_id="+
                id_dispositivo);
        if(jObject != null){
            return parseJSON(jObject);
        }
        else
        {
            return null;
        }

    }


    private List<AplicacionPermitida> parseJSON(JSONObject resultadoJSON) throws JSONException{
        if(resultadoJSON.getInt("resultCode")==0)
        {
            return null;
        }

        JSONArray jArray = resultadoJSON.getJSONArray("apps");

        List <AplicacionPermitida> apps = new ArrayList<>();
        int i = 0;
        while (jArray.length() > i) {
            AplicacionPermitida app = new AplicacionPermitida();


            if (!jArray.getJSONObject(i).isNull("id"))
                app.setId(jArray.getJSONObject(i).getInt("id"));

            try {
                if (!jArray.getJSONObject(i).isNull("label"))
                    app.setAppLabel(AESCrypt.decrypt(jArray.getJSONObject(i).getString("label")));

                if (!jArray.getJSONObject(i).isNull("package"))
                    app.setPackageName(AESCrypt.decrypt(jArray.getJSONObject(i).getString("package")));
            }
            catch(Exception e){

            }
            app.setPermitida(false);
            app.setUsoDuranteDesconecta(false);

            apps.add(app);
            i++;
        }

        return apps;

    }