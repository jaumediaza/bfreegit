package com.bfree.app.json;


import android.content.Context;

import com.bfree.app.R;
import com.bfree.app.clases.Usuario;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;




public class JSONEnviarEstadisticas {
	private Context activity;
	private JSONObject jObject;

	private Usuario usr=new Usuario();


	public JSONEnviarEstadisticas(Context a){
		activity = a;
	}
	
	public int run(int id_usuario, String id_dispositivo, String app_label, String app_package,
				   String timeInFor, String clr, String pctg, int periodo) throws JSONException{

		return loadJSON(id_usuario, id_dispositivo, app_label, app_package, timeInFor, clr, pctg, periodo);
	}
		
		
	private int loadJSON(int id_usuario, String id_dispositivo, String app_label, String app_package,
                         String timeInFor, String clr, String pctg, int periodo) throws JSONException{

        try {
            id_dispositivo = URLEncoder.encode(id_dispositivo, "UTF-8");
		//	app_label = AESCrypt.encrypt(app_label);
		//	app_package = AESCrypt.encrypt(app_package);
			pctg = AESCrypt.encrypt(pctg);

			String ruta = activity.getResources().getString(R.string.url_webservices)
					+"userstats/update_statistics.php?id="+id_usuario+"&device_id="+id_dispositivo+"&label_list="
					+app_label+"&pckg_list="+app_package+"&usage_list="+timeInFor+"&color_list="+clr+"&period="+periodo+"&pctg_list="+pctg;
			jObject = JSONManager.getJSONfromURL(ruta);

		} catch (Exception e) {
            e.printStackTrace();
        }



	   if(jObject != null){
		   return parseJSON(jObject);
	   }
	   else
	   {
		   return -1;
	   }

	}
		

	private int parseJSON(JSONObject resultadoJSON) throws JSONException{
		int res = -1;
		if(!resultadoJSON.isNull("resultCode")){
			res = resultadoJSON.getInt("resultCode");
		}


		return res;

	}



}
