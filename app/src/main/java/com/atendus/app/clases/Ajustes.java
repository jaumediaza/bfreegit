package com.bfree.app.clases;


import java.util.Calendar;
import java.util.Date;

public class Ajustes {
    private boolean alertaUsoMovil;
    private boolean alertaDesbloqueoPantalla;
    private boolean seguimientoUso;
    private boolean notificarResumenDiario;

    private Hora horaResumenDiario;
    private Hora usoMovil;
    private int avisarDesbloqueos;
    private int desbloqueos = 0;
    private long lastDay = 0;

    private long usageTime = 0;
    private boolean isUserPresent;




    public class Hora{
        public Hora(){hh = 0; mm = 0;}
        public int hh;
        public int mm;
    }

    public Ajustes(){
        horaResumenDiario = new Hora();
        horaResumenDiario.hh = 22;
        usoMovil = new Hora();
        usoMovil.hh = 2;
        usoMovil.mm = 30;
        avisarDesbloqueos = 50;
        alertaUsoMovil = false;
        alertaDesbloqueoPantalla = false;
        seguimientoUso = false;
        notificarResumenDiario = false;
    }

    public void unlocked(){
        isUserPresent = true;
    }

    public void locked(){
        isUserPresent = false;
    }

    public void addOneSecondOfUsage(){
        usageTime += 1000;
    }

    public long getUsageTime() {
        return usageTime;
    }

    public boolean isAlertaUsoMovil() {
        return alertaUsoMovil;
    }

    public void setAlertaUsoMovil(boolean alertaUsoMovil) {
        this.alertaUsoMovil = alertaUsoMovil;
    }

    public boolean isAlertaDesbloqueoPantalla() {
        return alertaDesbloqueoPantalla;
    }

    public void setAlertaDesbloqueoPantalla(boolean alertaDesbloqueoPantalla) {
        this.alertaDesbloqueoPantalla = alertaDesbloqueoPantalla;
    }

    public boolean isSeguimientoUso() {
        return seguimientoUso;
    }

    public void setSeguimientoUso(boolean seguimientoUso) {
        this.seguimientoUso = seguimientoUso;
    }

    public boolean isNotificarResumenDiario() {
        return notificarResumenDiario;
    }

    public void setNotificarResumenDiario(boolean notificarResumenDiario) {
        this.notificarResumenDiario = notificarResumenDiario;
    }

    public Hora getHoraResumenDiario() {
        return horaResumenDiario;
    }

    public void setHoraResumenDiario(Hora horaResumenDiario) {
        this.horaResumenDiario = horaResumenDiario;
    }

    public Hora getUsoMovil() {
        return usoMovil;
    }

    public void setUsoMovil(Hora usoMovil) {
        this.usoMovil = usoMovil;
    }

    public int getAvisarDesbloqueos() {
        return avisarDesbloqueos;
    }

    public void setAvisarDesbloqueos(int avisarDesbloqueos) {
        this.avisarDesbloqueos = avisarDesbloqueos;
    }

    public int getDesbloqueos() {
        return desbloqueos;
    }

    public void setDesbloqueos(int desbloqueos) {
        this.desbloqueos = desbloqueos;
    }

    public boolean isAvisarDesbloqueos() {
        return alertaDesbloqueoPantalla && desbloqueos == avisarDesbloqueos;
    }

    public boolean isUserPresent() {
        return isUserPresent;
    }

    public void setUserPresent(boolean userPresent) {
        isUserPresent = userPresent;
    }

    public void checkDay(){
        if (lastDay == 0){
            lastDay = Calendar.getInstance().getTime().getTime();
        }else{
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            cal1.setTime(new Date(lastDay));
            cal2.setTime(Calendar.getInstance().getTime());
            boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
            if (!sameDay) {
                setDesbloqueos(0);
                usageTime = 0;
            }
        }
    }
}
