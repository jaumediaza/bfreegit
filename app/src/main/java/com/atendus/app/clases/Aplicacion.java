package com.bfree.app.clases;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;

import com.bfree.app.ActivityPadre;
import com.bfree.app.ActivityPrincipal;
import com.bfree.app.json.JSONChild;
import com.bfree.app.json.JSONLocation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Usuario on 05/12/2016.
 */

public class Aplicacion {
    private Usuario usuario=new Usuario();//Representa al usuario en la aplicaci�n
    private boolean usuarioConectado=false;
    private boolean usuarioSinRegistro = false;
    private ConfiguracionDesconexion desconexionActual=new ConfiguracionDesconexion();
    private boolean modoDesconectaActivado=false;
    private boolean isDeviceAdmin = false;
    private boolean permisoAccesoDatosUsoAceptado =false;
    private boolean permisoNotificacionesAceptado=false;
    private List<AplicacionPermitida> appsPermitidas = new ArrayList<>();
    private List<Telefono> tfnosPermitidos = new ArrayList<>();
    private Ajustes ajustes = new Ajustes();
    private NotificacionInfo.NotificacionInfoList notificacioines =
            new NotificacionInfo.NotificacionInfoList();
    private long millisUntilFinished;
    private List<Desconexion> desconexiones = new ArrayList<>();
    private DesconexionProgramada.DesconexionProgramadaList desconexionesProgramadas =
            new DesconexionProgramada.DesconexionProgramadaList();
    private List<AppUsage> appUsages = new ArrayList<>();

    private boolean chartTypeEstadisticas = true;
    private ColaEnvios colaEnvios = new ColaEnvios();
    //private boolean desconexionEditable;

    private EstadisticaUso estadisticaUsoDia = new EstadisticaUso();
    private EstadisticaUso estadisticaUsoSemana = new EstadisticaUso();
    private EstadisticaUso estadisticaUsoMes = new EstadisticaUso();
    private List<DispositivosControlados> dispositivosControlados = new ArrayList<>();
    LocationManager mLocationManager;

    Context actualContext;
    long actualLongitud;
    long actualLatitud;


    public boolean isPermisoNotificacionesAceptado() {
        return permisoNotificacionesAceptado;
    }

    public void setPermisoNotificacionesAceptado(boolean permisoNotificacionesAceptado) {
        this.permisoNotificacionesAceptado = permisoNotificacionesAceptado;
    }

    public boolean isPermisoAccesoDatosUsoAceptado() {
        return permisoAccesoDatosUsoAceptado;
    }

    public void setPermisoAccesoDatosUsoAceptado(boolean permisoAccesoDatosUsoAceptado) {
        this.permisoAccesoDatosUsoAceptado = permisoAccesoDatosUsoAceptado;
    }

    public ConfiguracionDesconexion getDesconexionActual() {
        return desconexionActual;
    }

    public void setDesconexionActual(ConfiguracionDesconexion desconexionActual) {
        this.desconexionActual = desconexionActual;
    }

    public boolean isModoDesconectaActivado() {
        return modoDesconectaActivado;
    }

    public List<DispositivosControlados> getDispositivosControlados() {
        return dispositivosControlados;
    }

    public void setDispositivosControlados(List<DispositivosControlados> dispositivosControlados) {
        this.dispositivosControlados = dispositivosControlados;
    }

    /*public boolean isDesconexionEditable() {
        return desconexionEditable;
    }

    public void setDesconexionEditable(boolean desconexionEditable) {
        this.desconexionEditable = desconexionEditable;
    }*/

    public void setModoDesconectaActivado(boolean modoDesconectaActivado) {
        this.modoDesconectaActivado = modoDesconectaActivado;
    }

    public boolean isUsuarioConectado() {
        return usuarioConectado;
    }

    public void setUsuarioConectado(boolean usuarioConectado) {
        this.usuarioConectado = usuarioConectado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }


    public boolean isUsuarioSinRegistro() {
        return usuarioSinRegistro;
    }

    public void setUsuarioSinRegistro(boolean usuarioSinRegistro) {
        this.usuarioSinRegistro = usuarioSinRegistro;
    }

    public boolean isDeviceAdmin() {
        return isDeviceAdmin;
    }

    public void setDeviceAdmin(boolean deviceAdmin) {
        isDeviceAdmin = deviceAdmin;
    }

    public List<AplicacionPermitida> getAppsPermitidas() {
        return appsPermitidas;
    }

    public void setAppsPermitidas(List<AplicacionPermitida> appsPermitidas) {
        this.appsPermitidas = appsPermitidas;
    }

    public List<Telefono> getTfnosPermitidos() {
        return tfnosPermitidos;
    }

    public void setTfnosPermitidos(List<Telefono> tfnosPermitidos) {
        this.tfnosPermitidos = tfnosPermitidos;
    }

    public Ajustes getAjustes() {
        return ajustes;
    }

    public void setAjustes(Ajustes ajustes) {
        this.ajustes = ajustes;
    }

    public NotificacionInfo.NotificacionInfoList getNotificacioines() {
        return notificacioines;
    }

    public void setNotificacioines(NotificacionInfo.NotificacionInfoList notificacioines) {
        this.notificacioines = notificacioines;
    }

    public long getMillisUntilFinished() {
        return millisUntilFinished;
    }

    public void setMillisUntilFinished(long millisUntilFinished) {
        this.millisUntilFinished = millisUntilFinished;
    }

    public List<Desconexion> getDesconexiones() {
        return desconexiones;
    }

    public DesconexionProgramada.DesconexionProgramadaList getDesconexionesProgramadas() {
        return desconexionesProgramadas;
    }

    public List<AppUsage> getAppUsages() {
        return appUsages;
    }

    public void setAppUsages(List<AppUsage> appUsages) {
        this.appUsages = appUsages;
    }

    public boolean isChartTypeEstadisticas() {
        return chartTypeEstadisticas;
    }

    public void setChartTypeEstadisticas(boolean chartTypeEstadisticas) {
        this.chartTypeEstadisticas = chartTypeEstadisticas;
    }

    public EstadisticaUso getEstadisticaUsoDia() {
        return estadisticaUsoDia;
    }

    public void setEstadisticaUsoDia(EstadisticaUso estadisticaUsoDia) {
        this.estadisticaUsoDia = estadisticaUsoDia;
    }

    public EstadisticaUso getEstadisticaUsoSemana() {
        return estadisticaUsoSemana;
    }

    public void setEstadisticaUsoSemana(EstadisticaUso estadisticaUsoSemana) {
        this.estadisticaUsoSemana = estadisticaUsoSemana;
    }

    public EstadisticaUso getEstadisticaUsoMes() {
        return estadisticaUsoMes;
    }

    public void setEstadisticaUsoMes(EstadisticaUso estadisticaUsoMes) {
        this.estadisticaUsoMes = estadisticaUsoMes;
    }

    public ColaEnvios getColaEnvios() {
        return colaEnvios;
    }

    public void setColaEnvios(ColaEnvios colaEnvios) {
        this.colaEnvios = colaEnvios;
    }

    public void cerrarSesion(Activity activity)
    {
        usuario=new Usuario();
        usuarioConectado=false;
        usuarioSinRegistro = false;

        SharedPreferences prefs = activity.getSharedPreferences("PUSH", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("usuarioConectado", false);
        editor.commit();
    }

    public void guardarEnPreferencias(Activity a)
    {
        SharedPreferences preferences;
        Gson gson = new Gson();
        String json = gson.toJson(this);

        preferences=  a.getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("aplicacion", json);
        editor.commit();

    }

    public void guardarEnPreferencias(Context a)
    {
        SharedPreferences preferences;
        Gson gson = new Gson();
        String json = gson.toJson(this);

        preferences=  a.getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("aplicacion", json);
        editor.commit();

    }

    /*public void cargarAplicacionDePreferencias(Activity a)
    {
        SharedPreferences preferences;
        preferences=  a.getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        String carga=preferences.getString("aplicacion", null);
        Aplicacion aplicacion = null;

        Gson gson = new Gson();
        Type type = new TypeToken<Aplicacion>() {}.getType();
        Aplicacion obj = gson.fromJson(carga, type);
        if(obj!=null) {
            aplicacion=obj;

            this.usuario=obj.getUsuario();
            this.usuarioConectado=obj.isUsuarioConectado();
            usuarioSinRegistro = obj.isUsuarioSinRegistro();
            this.desconexionActual=obj.getDesconexionActual();
            this.modoDesconectaActivado=obj.modoDesconectaActivado;
            this.isDeviceAdmin = obj.isDeviceAdmin;
            this.permisoAccesoDatosUsoAceptado =obj.permisoAccesoDatosUsoAceptado;
            this.permisoNotificacionesAceptado=obj.permisoNotificacionesAceptado;
            notificacioines = obj.notificacioines;
            appsPermitidas = obj.appsPermitidas;
            tfnosPermitidos = obj.tfnosPermitidos;
            millisUntilFinished = obj.millisUntilFinished;
            desconexiones = obj.desconexiones;
            ajustes = obj.ajustes;
            ajustes.checkDay();
            notificacioines.updateItems();
            desconexionesProgramadas = obj.desconexionesProgramadas;
            appUsages = obj.getAppUsages();
            chartTypeEstadisticas = obj.chartTypeEstadisticas;


        }
    }*/

    public void cargarAplicacionDePreferencias(Context c)
    {
        SharedPreferences preferences;

        preferences=  c.getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        String carga=preferences.getString("aplicacion", null);
        Aplicacion aplicacion = null;

        Gson gson = new Gson();
        Type type = new TypeToken<Aplicacion>() {}.getType();
        Aplicacion obj = gson.fromJson(carga, type);
        if(obj!=null) {
            aplicacion=obj;

            this.usuario=obj.getUsuario();
            this.usuarioConectado=obj.isUsuarioConectado();
            usuarioSinRegistro = obj.isUsuarioSinRegistro();
            this.desconexionActual=obj.getDesconexionActual();
            this.modoDesconectaActivado=obj.modoDesconectaActivado;
            this.isDeviceAdmin = obj.isDeviceAdmin;
            this.permisoAccesoDatosUsoAceptado =obj.permisoAccesoDatosUsoAceptado;
            this.permisoNotificacionesAceptado=obj.permisoNotificacionesAceptado;
            appsPermitidas = obj.appsPermitidas;
            tfnosPermitidos = obj.tfnosPermitidos;
            notificacioines = obj.notificacioines;
            millisUntilFinished = obj.millisUntilFinished;
            desconexiones = obj.desconexiones;
            ajustes = obj.ajustes;
            ajustes.checkDay();
            notificacioines.updateItems();
            desconexionesProgramadas = obj.desconexionesProgramadas;
            appUsages = obj.getAppUsages();
            chartTypeEstadisticas = obj.chartTypeEstadisticas;
            colaEnvios = obj.colaEnvios;
            estadisticaUsoDia = obj.estadisticaUsoDia ;
            estadisticaUsoSemana= obj.estadisticaUsoSemana;
            estadisticaUsoMes = obj.estadisticaUsoMes ;

        }
    }

    public void cargarUsuarioDePreferencias(Context c){
        SharedPreferences preferences;
        preferences=  c.getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
        String carga=preferences.getString("aplicacion", null);

        Gson gson = new Gson();
        Type type = new TypeToken<Aplicacion>() {}.getType();
        Aplicacion obj = gson.fromJson(carga, type);
        if(obj!=null) {

            usuario = obj.getUsuario();
            usuarioConectado = obj.isUsuarioConectado();
            usuarioSinRegistro = obj.isUsuarioSinRegistro();
        }
    }

    public void startCheckingForLocation(Context context){
        actualContext = context;
        if (ContextCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);

            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 30000,
                    20, mLocationListener);
        }
    }

    void setLocation(){

        new Thread() {
            @Override
            public void run() {
                super.run();
                JSONLocation jsonChild = new JSONLocation(actualContext);
                int resLoc = -1;
                try {
                    resLoc = jsonChild.run(getUsuario().getId(), actualLatitud, actualLongitud);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }.start();
    }


    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            if(location!=null) {
                actualLongitud = (long)location.getLongitude();
                actualLatitud = (long)location.getLatitude();
                setLocation();
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };


}
