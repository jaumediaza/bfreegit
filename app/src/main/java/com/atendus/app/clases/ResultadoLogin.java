package com.atendus.app.clases;

/**
 * Created by Usuario on 27/09/2017.
 */

public class ResultadoLogin {
    private int resultado;//1. Login Ok, 2. El usuario tiene que activar la cuenta
    Usuario usuario=new Usuario();

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}