package com.atendus.app.clases;


public class Desconexion {
    private long fechaInicio;
    private long fechaFin;
    private NotificacionInfo.NotificacionInfoList notificaciones;
    private boolean appsPermitidas;
    private boolean llamadasPermitidas;

    public Desconexion(long fechaInicio, long fechaFin, NotificacionInfo.NotificacionInfoList notificaciones,
                       boolean appsPermitidas, boolean llamadasPermitidas) {
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.notificaciones = notificaciones;
        this.appsPermitidas = appsPermitidas;
        this.llamadasPermitidas = llamadasPermitidas;
    }

    public long getFechaInicio() {
        return fechaInicio;
    }

    public long getFechaFin() {
        return fechaFin;
    }

    public NotificacionInfo.NotificacionInfoList getNotificaciones() {
        return notificaciones;
    }

    public boolean isAppsPermitidas() {
        return appsPermitidas;
    }

    public boolean isLlamadasPermitidas() {
        return llamadasPermitidas;
    }
}