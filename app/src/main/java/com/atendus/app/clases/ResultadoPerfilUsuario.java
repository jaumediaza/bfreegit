package com.bfree.app.clases;

/**
 * Created by Alberto on 13/06/2018.
 */

public class ResultadoPerfilUsuario {
    private int resultado=0;
    Usuario usuario=new Usuario();

    public int getResultado() {
        return resultado;
    }

    public void setResultado(int resultado) {
        this.resultado = resultado;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
