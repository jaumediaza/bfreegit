package com.bfree.app.clases;


import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;
import android.arch.lifecycle.ProcessLifecycleOwner;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.bfree.app.ActivityPermisos;
import com.bfree.app.ActivityPrincipal;
import com.bfree.app.login.ActivityPassword;

import org.solovyev.android.checkout.Billing;

import javax.annotation.Nonnull;

import static com.bfree.app.ActivityPermisos.permiso_accesibilidad;
import static com.bfree.app.ActivityPermisos.permiso_admin;
import static com.bfree.app.ActivityPermisos.permiso_contactos;
import static com.bfree.app.ActivityPermisos.permiso_datouso;
import static com.bfree.app.ActivityPermisos.permiso_default;
import static com.bfree.app.ActivityPermisos.permiso_geoloc;
import static com.bfree.app.ActivityPermisos.permiso_llamadas;
import static com.bfree.app.ActivityPermisos.permiso_notif;

public class AppController extends Application{

    Context ctx;
    boolean justCreated;

    @Nonnull
    private final Billing mBilling = new Billing(this, new Billing.DefaultConfiguration() {
        @Nonnull
        @Override
        public String getPublicKey() {
            // encrypted public key of the app. Plain version can be found in Google Play's Developer
            // Console in Service & APIs section under "YOUR LICENSE KEY FOR THIS APPLICATION" title.
            String a = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAhAAo2O3aF9SKpNtoOBlaOYP0BdWUlOFIr26+FcZ9JizukD9dJ";
            String b = "PyAeO/b6Y6OvmKtH4Gl/N50iC6YhPktJW1x+MkJlQeFfjYJp5yackERHN4ftFmG2hz7yHIKTK29cuXxGA4eFfZ+6gG+rlcn54cO26Bt/yo";
            String c = "ESNswIM1ouBL/CU8NJjDUkzDEyA/6NNrb4GUeauMNSrrzy/k8tLQEWn4Yv57/E1YPzyfaIoc916EzrKx6uT+/QByIZFR4BXdxNMl2AggFh";
            String d = "CV5cgavMAOPyMxG1EUIRVM23EoL6kFUgTw73m8LHuQtEWEFSHOmwD6tRA7s7Jh4fqgp5p3N72O+FI4s3QIDAQAB";
            final String s = a + b + c + d;

            return s;
        }
    });
    @Nonnull
    public Billing getBilling() {
        return mBilling;
    }

    public static AppController get(Activity activity) {
        return (AppController) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        justCreated = false;
        ProcessLifecycleOwner.get().getLifecycle().addObserver(new AppLifecycleListener());
        ctx = this;
    }

    public class AppLifecycleListener implements LifecycleObserver {


        @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
        public void onMoveToForeground() {
            Aplicacion app = new Aplicacion();
            app.cargarAplicacionDePreferencias(ctx);

            SharedPreferences preferences = getSharedPreferences("Preferencias",Context.MODE_PRIVATE);
            if(app.isUsuarioConectado() && preferences.getInt("padre",-1)==0){
                if((permiso_llamadas && permiso_contactos && permiso_default && permiso_geoloc && permiso_accesibilidad && permiso_admin==true && permiso_datouso  && permiso_notif) && justCreated && !app.isUsuarioSinRegistro()) {
                    if(System.currentTimeMillis()>(preferences.getLong("timeloaded",0)+60000)) {
                        SharedPreferences.Editor editor = preferences.edit();
                        editor.putLong("timeloaded", System.currentTimeMillis());
                        editor.commit();
                        Intent intent = new Intent(ctx, ActivityPassword.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                }
            }
            justCreated=true;
        }

        @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
        public void onMoveToBackground() {
            // app moved to background
        }


    }



}