package com.atendus.app.clases;

import java.util.List;

public class EstadisticasUsuarioRemoto {

    private int desbloqueosDia, desbloqueosSemana, desbloqueosMes;
    private long tiempoUsoDia,tiempoUsoSemana, tiempoUsoMes;

    private List<EstadisticaAplicacion> estadisticasDia;
    private List<EstadisticaAplicacion> estadisticasSemana;
    private List<EstadisticaAplicacion> estadisticasMes;


    public int getDesbloqueosDia() {
        return desbloqueosDia;
    }

    public void setDesbloqueosDia(int desbloqueosDia) {
        this.desbloqueosDia = desbloqueosDia;
    }

    public int getDesbloqueosSemana() {
        return desbloqueosSemana;
    }

    public void setDesbloqueosSemana(int desbloqueosSemana) {
        this.desbloqueosSemana = desbloqueosSemana;
    }

    public int getDesbloqueosMes() {
        return desbloqueosMes;
    }

    public void setDesbloqueosMes(int desbloqueosMes) {
        this.desbloqueosMes = desbloqueosMes;
    }

    public long getTiempoUsoDia() {
        return tiempoUsoDia;
    }

    public void setTiempoUsoDia(long tiempoUsoDia) {
        this.tiempoUsoDia = tiempoUsoDia;
    }

    public long getTiempoUsoSemana() {
        return tiempoUsoSemana;
    }

    public void setTiempoUsoSemana(long tiempoUsoSemana) {
        this.tiempoUsoSemana = tiempoUsoSemana;
    }

    public long getTiempoUsoMes() {
        return tiempoUsoMes;
    }

    public void setTiempoUsoMes(long tiempoUsoMes) {
        this.tiempoUsoMes = tiempoUsoMes;
    }

    public List<EstadisticaAplicacion> getEstadisticasDia() {
        return estadisticasDia;
    }

    public void setEstadisticasDia(List<EstadisticaAplicacion> estadisticasDia) {
        this.estadisticasDia = estadisticasDia;
    }

    public List<EstadisticaAplicacion> getEstadisticasSemana() {
        return estadisticasSemana;
    }

    public void setEstadisticasSemana(List<EstadisticaAplicacion> estadisticasSemana) {
        this.estadisticasSemana = estadisticasSemana;
    }

    public List<EstadisticaAplicacion> getEstadisticasMes() {
        return estadisticasMes;
    }

    public void setEstadisticasMes(List<EstadisticaAplicacion> estadisticasMes) {
        this.estadisticasMes = estadisticasMes;
    }
}