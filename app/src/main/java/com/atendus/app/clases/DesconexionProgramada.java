package com.atendus.app.clases;

import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.List;

public class DesconexionProgramada implements Comparable<DesconexionProgramada> {
    private boolean bloquearNotificaciones;
    private boolean bloquearLlamadas;
    private int horaInicio;
    private int horaFin;
    private int minutoInicio;
    private int minutoFin;
    private String strTitulo;
    private boolean enabled = true;
    private boolean dias[] = new boolean[7];
    private int repetir;
    private int id;
    private int user_id;
    private int device_id;

    private boolean editable = true;

    private String aplicacionesPermitidas;
    private String tfnosPermitidos;

    public DesconexionProgramada(){id = -1;aplicacionesPermitidas = "";}


    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getDevice_id() {
        return device_id;
    }

    public void setDevice_id(int device_id) {
        this.device_id = device_id;
    }

    public boolean isBloquearNotificaciones() {
        return bloquearNotificaciones;
    }

    public void setBloquearNotificaciones(boolean bloquearNotificaciones) {
        this.bloquearNotificaciones = bloquearNotificaciones;
    }

    public boolean isBloquearLlamadas() {
        return bloquearLlamadas;
    }

    public void setBloquearLlamadas(boolean bloquearLlamadas) {
        this.bloquearLlamadas = bloquearLlamadas;
    }

    public int getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(int horaInicio) {
        this.horaInicio = horaInicio;
    }

    public int getHoraFin() {
        return horaFin;
    }

    public void setHoraFin(int horaFin) {
        this.horaFin = horaFin;
    }

    public int getMinutoInicio() {
        return minutoInicio;
    }

    public void setMinutoInicio(int minutoInicio) {
        this.minutoInicio = minutoInicio;
    }

    public int getMinutoFin() {
        return minutoFin;
    }

    public void setMinutoFin(int minutoFin) {
        this.minutoFin = minutoFin;
    }

    public String getStrTitulo() {
        return strTitulo;
    }

    public void setStrTitulo(String strTitulo) {
        this.strTitulo = strTitulo;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean[] getDias() {
        return dias;
    }

    public void setDias(boolean[] dias) {
        this.dias = dias;
    }

    public int getRepetir() {
        return repetir;
    }

    public void setRepetir(int repetir) {
        this.repetir = repetir;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public int compareTo(@NonNull DesconexionProgramada desconexionProgramada) {
        return Integer.compare(id,desconexionProgramada.getId());
    }

    public static class DesconexionProgramadaList{
        List<DesconexionProgramada> list;

        public DesconexionProgramadaList(){
            list = new ArrayList<>();
        }

        public List<DesconexionProgramada> getList() {
            return list;
        }

        public void setList(List<DesconexionProgramada> list) {
            this.list = list;
        }

    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String getAplicacionesPermitidas() {
        return aplicacionesPermitidas;
    }

    public void setAplicacionesPermitidas(String aplicacionesPermitidas) {
        this.aplicacionesPermitidas = aplicacionesPermitidas;
    }

    public String getTfnosPermitidos() {
        return tfnosPermitidos;
    }

    public void setTfnosPermitidos(String tfnosPermitidos) {
        this.tfnosPermitidos = tfnosPermitidos;
    }
}