package com.bfree.app.clases;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;

import com.bfree.app.json.JSONBorrarDesconexionProgramada;
import com.bfree.app.json.JSONEnviarDesconexionProgramada;
import com.bfree.app.json.JSONEnviarDesconexionProgramadaModificar;
import com.bfree.app.utilidades.TresAndroides;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;


public class ColaEnvios {

    public boolean pendientes = false;
    private List<DesconexionProgramada> desconexionesNuevas;
    private List<DesconexionProgramada> desconexionesModificar;
    private List<DesconexionProgramada> desconexionesBorrar;


    public ColaEnvios() {
        desconexionesNuevas = new ArrayList<>();
        desconexionesModificar = new ArrayList<>();
        desconexionesBorrar = new ArrayList<>();
    }

   public void addNuevaDesconexion(DesconexionProgramada desc){
        desconexionesNuevas.add(desc);
        pendientes = true;
   }

   public void addModificarDesconexion(DesconexionProgramada desc){
       desconexionesModificar.add(desc);
       pendientes = true;
   }

    public void addBorrarDesconexion(DesconexionProgramada desc){
        desconexionesBorrar.add(desc);
        pendientes = true;
    }

   public boolean enviarTodos(Context c, int idusu){
        boolean done = true;

       if (ActivityCompat.checkSelfPermission(c, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
           return false;
       }
       TelephonyManager telephonyManager = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
       final String iddisp = telephonyManager.getDeviceId();


        for (int i = 0; i < desconexionesNuevas.size(); ++i){
            if (TresAndroides.isOnline(c)){
                JSONEnviarDesconexionProgramada jsonEnviarDesconexionProgramada =
                        new JSONEnviarDesconexionProgramada(c);
                try {
                    jsonEnviarDesconexionProgramada.run(idusu, iddisp, desconexionesNuevas.get(i));
                    desconexionesNuevas.remove(i);
                    i--;
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
                done = false;
            }
        }

       for (int i = 0; i < desconexionesModificar.size(); ++i){
           if (TresAndroides.isOnline(c)){
               JSONEnviarDesconexionProgramadaModificar jsonEnviarDesconexionProgramada =
                       new JSONEnviarDesconexionProgramadaModificar(c);
               try {
                   int res = jsonEnviarDesconexionProgramada.run(idusu, iddisp, desconexionesModificar.get(i));
                   desconexionesModificar.remove(i);
                   i--;
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }else{
               done = false;
           }
       }


       for (int i = 0; i < desconexionesBorrar.size(); ++i){
           if (TresAndroides.isOnline(c)){
               JSONBorrarDesconexionProgramada jsonEnviarDesconexionProgramada =
                       new JSONBorrarDesconexionProgramada(c);
               try {
               jsonEnviarDesconexionProgramada.run(idusu, iddisp, desconexionesBorrar.get(i).getId());
                   desconexionesBorrar.remove(i);
                   i--;
               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }else{
               done = false;
           }
       }



        return  done;
   }

}
