package com.atendus.app.clases;

import android.graphics.drawable.Drawable;

import com.atendus.app.utilidades.PhoneStateManager;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class NotificacionInfo implements Comparable<NotificacionInfo>{

    private transient Drawable icon = null;
    private String appname;
    private String packageName;
    //private transient Date lastdate;
    private long lDate;
    private int totalNotificaciones;

    public NotificacionInfo(String packageName) {
        this.appname = "NULL";
        try{
            for (int i = 0; i < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++i){
                if (PhoneStateManager.getInstance().getAppsPermitidas().get(i).getPackageName().equals(packageName)){
                    icon = PhoneStateManager.getInstance().getAppsPermitidas().get(i).getDrawable();
                    appname = PhoneStateManager.getInstance().getAppsPermitidas().get(i).getAppLabel();
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }


        this.packageName = packageName;
        lDate = Calendar.getInstance().getTime().getTime();
        //lDate = lastdate.getTime();
        totalNotificaciones = 1;
    }

    public Drawable getIcon() {
        return icon;
    }

    public String getAppname() {
        return appname;
    }

    public Date getLastdate() {
        return new Date(lDate);
    }


    public int getTotalNotificaciones() {
        return totalNotificaciones;
    }

    public void addNewNotificaction(){
        lDate = Calendar.getInstance().getTime().getTime();
        // = lastdate.getTime();
        totalNotificaciones++;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public void setIcon(Drawable icon) {
        this.icon = icon;
    }

    public void setAppname(String appname) {
        this.appname = appname;
    }



    public static class NotificacionInfoList{
        private List<NotificacionInfo> list;

        public NotificacionInfoList(){
            list = new ArrayList<>();
        }
        public NotificacionInfoList(List<NotificacionInfo> list){
            this.list = list;
        }

        public List<NotificacionInfo> getList() {
            return list;
        }

        public void setList(List<NotificacionInfo> list) {
            this.list = list;
        }

        public void updateItems(){
            for (int j = 0; j < list.size(); ++j) {
                for (int i = 0; i < PhoneStateManager.getInstance().getAppsPermitidas().size(); ++i) {
                    if (PhoneStateManager.getInstance().getAppsPermitidas().get(i).getPackageName().equals(list.get(j).getPackageName())) {
                        list.get(j).setIcon( PhoneStateManager.getInstance().getAppsPermitidas().get(i).getDrawable());
                    }
                }
            }
        }
    }

    @Override
    public int compareTo(NotificacionInfo o) {
        return getLastdate().compareTo(o.getLastdate());
    }
