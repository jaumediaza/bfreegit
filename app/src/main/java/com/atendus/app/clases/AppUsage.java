package com.bfree.app.clases;

import android.support.annotation.NonNull;

public class AppUsage implements Comparable<AppUsage> {

    private String package_name;
    private long totalTimeInForeground;
    private String appName = "";
    private int color = 0;


    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }

    public long getTotalTimeInForeground() {
        return totalTimeInForeground;
    }

    public void setTotalTimeInForeground(long totalTimeInForeground) {
        this.totalTimeInForeground = totalTimeInForeground;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public int compareTo(@NonNull AppUsage appUsage) {
        return Long.compare(this.totalTimeInForeground, appUsage.getTotalTimeInForeground());
    }
}
